package kz.PvP.MedievalWars;

import java.util.List;
import java.util.logging.Logger;

import kz.PvP.MedievalWars.Commands.GameCommands;
import kz.PvP.MedievalWars.Commands.PlayerCommands;
import kz.PvP.MedievalWars.Enums.GM;
import kz.PvP.MedievalWars.Enums.Team;
import kz.PvP.MedievalWars.Enums.Game;
import kz.PvP.MedievalWars.Events.BuildListener;
import kz.PvP.MedievalWars.Events.ChatListener;
import kz.PvP.MedievalWars.Events.DamageListener;
import kz.PvP.MedievalWars.Events.DeathListener;
import kz.PvP.MedievalWars.Events.InteractListener;
import kz.PvP.MedievalWars.Events.JoinListener;
import kz.PvP.MedievalWars.Events.Spectator;
import kz.PvP.MedievalWars.Events.bandaidListener;
import kz.PvP.MedievalWars.Gamemodes.CTF;
import kz.PvP.MedievalWars.Gamemodes.Elimination;
import kz.PvP.MedievalWars.Gamemodes.Global;
import kz.PvP.MedievalWars.Gamemodes.TDM;
import kz.PvP.MedievalWars.Methods.Cannons;
import kz.PvP.MedievalWars.Methods.ChangeMaps;
import kz.PvP.MedievalWars.Methods.ConvertTimings;
import kz.PvP.MedievalWars.Methods.CoreM;
import kz.PvP.MedievalWars.Methods.GameManagement;
import kz.PvP.MedievalWars.Methods.LocationsMW;
import kz.PvP.MedievalWars.Methods.PowerUps;
import kz.PvP.MedievalWars.Methods.ProtocolLib;
import kz.PvP.MedievalWars.Methods.Scoreboards;
import kz.PvP.MedievalWars.Methods.Stats;
import kz.PvP.MedievalWars.Methods.TimedCommand;
import kz.PvP.MedievalWars.Methods.teamC;
import kz.PvP.MedievalWars.Methods.teamM;
import kz.PvP.MedievalWars.Timers.EndGameTimer;
import kz.PvP.MedievalWars.Timers.PreGameTimer;
import kz.PvP.MedievalWars.utilities.Files;
import kz.PvP.MedievalWars.utilities.Kits;
import kz.PvP.MedievalWars.utilities.MessageMW;




import kz.PvP.PkzAPI.utilities.Message;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;



public class Main extends JavaPlugin{
		static Logger log = Logger.getLogger("Minecraft");
		public static Game GameStatus = Game.PREGAME;
		public static GM Gamemode = GM.Voting;
		public static String WarDisplayname = null;// The world which can be anything
		public static String WarWorldname = null;// The world which will be used for all war maps
		public static World World = null;
		public static boolean UseMySQL = false;
		public static String DBPass = null;
		public static String DBUser = null;
		public static String DBHost = null;
		public static String DBName = null;
		public static List<String> Tips = null;
		private static boolean ShowTips = false;
		public static GM MainGamemode = null;
		public static boolean ProtocolLibEnable = false;
		public static boolean TabAPIEnable = false;
		
		
		
		
		
		
		public void onLoad()
	    {
			new Files(this);
			new ChangeMaps(this);
		    new ConvertTimings(this);
		    
		    reloadConfigValues();
			
			List<String> worldsinfo = Files.worlds.getCustomConfig().getStringList("Worlds");
	    	WarDisplayname = worldsinfo.get(ConvertTimings.randomInt(0, worldsinfo.size() - 1));
	    	ChangeMaps.LoadMapSave(WarDisplayname);
	    }

		
		
		private void reloadConfigValues() {
			FileConfiguration conf = Files.config.getCustomConfig();
			
			// We set variables depending on the config file.
				String Option = "";
				
				Option = "CaptureFlag.CapturesNeeded";
				if (conf.contains(Option))
				GameManagement.CTFCapturesNeeded = conf.getInt(Option);// Total Caps needed in CTF
				
				Option = "CaptureFlag.Description";
				if (conf.contains(Option))
				GameManagement.CTFDescription = conf.getString(Option);// Total Caps needed in CTF
				
				Option = "CaptureFlag.PointsPerCap";
				if (conf.contains(Option))
				GameManagement.CTFPointsPerCap = conf.getInt(Option);// Total Caps needed in CTF
				
				Option = "CaptureFlag.PointsPerKill";
				if (conf.contains(Option))
				GameManagement.CTFPointsPerKill = conf.getInt(Option);// Total Caps needed in CTF
				
				Option = "CaptureFlag.MaxRoundLength";
				if (conf.contains(Option))
				GameManagement.CTFRoundLength = conf.getInt(Option);// Total Caps needed in CTF
				
				Option = "Elimination.PlayersNeeded";
				if (conf.contains(Option))
				GameManagement.EliminationPlayersNeeded = conf.getInt(Option);// Total Caps needed in CTF
				
				Option = "Elimination.Description";
				if (conf.contains(Option))
				GameManagement.EliminationDescription = conf.getString(Option);// Total Caps needed in CTF
				
				Option = "Elimination.MaxRoundLength";
				if (conf.contains(Option))
				GameManagement.EliminationRoundLength = conf.getInt(Option);// Total Caps needed in CTF

				Option = "Flags.DespawnCountdown";
				if (conf.contains(Option))
				GameManagement.FlagDespawnTime = conf.getInt(Option);// Total Caps needed in CTF
				
				Option = "General.IdleMaxTime";
				if (conf.contains(Option))
				GameManagement.IdleMaxTime = conf.getInt(Option);// Total Caps needed in CTF
				
				Option = "General.PlayersNeeded";
				if (conf.contains(Option))
				GameManagement.MinPlayersNeeded = conf.getInt(Option);// Total Caps needed in CTF
				
				Option = "Mixed.Description";
				if (conf.contains(Option))
				GameManagement.MixedDescription = conf.getString(Option);// Total Caps needed in CTF
				
				Option = "Mixed.PointsPerCap";
				if (conf.contains(Option))
				GameManagement.MixedPointsPerCap = conf.getInt(Option);// Total Caps needed in CTF
				
				Option = "Mixed.PointsPerKill";
				if (conf.contains(Option))
				GameManagement.MixedPointsPerKill = conf.getInt(Option);// Total Caps needed in CTF
				
				Option = "Mixed.MaxRoundLength";
				if (conf.contains(Option))
				GameManagement.MixedRoundLength = conf.getInt(Option);// Total Caps needed in CTF
				
				Option = "Mixed.ScorePerCap";
				if (conf.contains(Option))
				GameManagement.MixedScorePerCap = conf.getInt(Option);// Total Caps needed in CTF
				
				Option = "Mixed.ScorePerKill";
				if (conf.contains(Option))
				GameManagement.MixedScorePerKill = conf.getInt(Option);// Total Caps needed in CTF
				
				Option = "Mixed.ScoreLimit";
				if (conf.contains(Option))
				GameManagement.MixedScoreReq = conf.getInt(Option);// Total Caps needed in CTF
				
				Option = "General.MaxStack";
				if (conf.contains(Option))
				GameManagement.PlayersStackFix = conf.getInt(Option);// Total Caps needed in CTF
				
				Option = "TeamDM.Description";
				if (conf.contains(Option))
				GameManagement.TDMDescription = conf.getString(Option);// Total Caps needed in CTF
				
				Option = "TeamDM.KillLimit";
				if (conf.contains(Option))
				GameManagement.TDMKillsReq = conf.getInt(Option);// Total Caps needed in CTF
				
				Option = "TeamDM.PointsPerKill";
				if (conf.contains(Option))
				GameManagement.TDMPointsPerKill = conf.getInt(Option);// Total Caps needed in CTF
				
				Option = "TeamDM.MaxRoundLength";
				if (conf.contains(Option))
				GameManagement.TDMRoundLength = conf.getInt(Option);// Total Caps needed in CTF
				
				Option = "General.WarWorldName";
				if (conf.contains(Option))
				WarWorldname = conf.getString(Option);// Total Caps needed in CTF
				
				
				Global.HorseNames = Files.config.getCustomConfig().getStringList("HorseNames");

				
			if (ShowTips == true && conf.contains("General.Tips"))
				Tips = conf.getStringList("General.Tips");
		}



		public void onEnable()
	    {
		log.info("Enabling Medieval War");
		
		
	    new Games(this);
	    new InfoSQL(this);
	    new Vote(this);
	    new GameCommands(this);
	    new PlayerCommands(this);
	    new BuildListener(this);
	    new DamageListener(this);
	    new DeathListener(this);
	    new InteractListener(this);
	    new JoinListener(this);
	    new Spectator(this);
	    new CTF(this);
	    new Elimination(this);
	    new Global(this);
	    new TDM(this);
	    new Cannons(this);
	    new PowerUps(this);
	    new Scoreboards(this);
	    new Stats(this);
	    new teamC(this);
	    new teamM(this);
	    new TimedCommand(this);
	    new EndGameTimer(this);
	    new PreGameTimer(this);
	    new Kits(this);
	    new MessageMW(this);
	    new bandaidListener(this);
	    new ChatListener(this);
	    new CoreM(this);
	    new LocationsMW(this);
	    new GameManagement(this);

	    
        PreGameTimer.BeginTimer();
    	RegisterCommands();
    	
    	Scoreboard();
		
    	Message.PREFIX = MessageMW.PREFIX;
    	
    	
    	
		Plugin protocolLib = this.getServer().getPluginManager().getPlugin("ProtocolLib");
		Plugin TabAPI = this.getServer().getPluginManager().getPlugin("TabAPI");
		
		if (protocolLib == null)
			Main.ProtocolLibEnable = false;
		else {
			Main.ProtocolLibEnable = true;
			new ProtocolLib(this);
		}
		
		if (TabAPI == null || protocolLib == null)
			Main.TabAPIEnable = false;
		else 
			Main.TabAPIEnable = true;
		
		if (Main.TabAPIEnable)
    	for (Player pl : this.getServer().getOnlinePlayers())
    		ProtocolLib.TabsetPriority(pl,2);
		}
		private void Scoreboard() {
	    	this.getServer().getScheduler().runTaskTimerAsynchronously(this, new Runnable() {
	    	    @Override  
	    	    public void run() {
	    	        for (Player pl : getServer().getOnlinePlayers()){
	    	        	Scoreboards.ScoreBoard(pl);
	    	        	pl.setFoodLevel(20);
	    	        }
	    	    }
	    	}, 10L, 10L);
	    	
	    	
	    	
	    	this.getServer().getScheduler().runTaskTimerAsynchronously(this, new Runnable() {
	    	    @Override  
	    	    public void run() {
	    	        for (Player pl : getServer().getOnlinePlayers()){
	    	        	if (!GameManagement.IdleUsers.containsKey(pl.getName()))
	    	        		GameManagement.IdleUsers.put(pl.getName(), pl.getLocation());
	    	        	else{
	    	        		if (GameManagement.IdleUsers.get(pl.getName()) == pl.getLocation()){
	    	        			teamM.assignTeam(pl.getName(), Team.Spectator);
								Message.P(pl, Message.Replacer(MessageMW.Idle, ConvertTimings.convertTime(GameManagement.IdleMaxTime), "%time"), true);
	    	        		}
	    	        	}
	    	        }
	    	    }
	    	}, 20L * GameManagement.IdleMaxTime, 20L  * GameManagement.IdleMaxTime);
		}
		private void RegisterCommands() {
	    	getCommand("mw").setExecutor(new PlayerCommands(this));
	    	getCommand("mwa").setExecutor(new GameCommands(this));
	    	getCommand("mwcontrol").setExecutor(new GameCommands(this));
	    	getCommand("class").setExecutor(new PlayerCommands(this));
	    	getCommand("upgrade").setExecutor(new PlayerCommands(this));
	    	getCommand("team").setExecutor(new PlayerCommands(this));
	    	getCommand("activate").setExecutor(new PlayerCommands(this));
	    	getCommand("leave").setExecutor(new PlayerCommands(this));
	    	getCommand("chat").setExecutor(new PlayerCommands(this));
	    	getCommand("notify").setExecutor(new PlayerCommands(this));
	    	getCommand("points").setExecutor(new PlayerCommands(this));
	    	getCommand("help").setExecutor(new PlayerCommands(this));
	    	getCommand("vote").setExecutor(new PlayerCommands(this));
	    	getCommand("tell").setExecutor(new PlayerCommands(this));
	    	getCommand("reply").setExecutor(new PlayerCommands(this));
	    	getCommand("tp").setExecutor(new PlayerCommands(this));
	    	getCommand("tpall").setExecutor(new PlayerCommands(this));
	    	getCommand("gm").setExecutor(new PlayerCommands(this));
	    	getCommand("feedback").setExecutor(new PlayerCommands(this));
	    	getCommand("stats").setExecutor(new PlayerCommands(this));
		}
		public void onDisable(){
		    log.info("Disabling MedievalWar");
		    
			Bukkit.getServer().getScheduler().cancelAllTasks();
	    }
		
		
		
		
}
