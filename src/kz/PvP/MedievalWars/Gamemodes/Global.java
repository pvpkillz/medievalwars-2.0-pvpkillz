package kz.PvP.MedievalWars.Gamemodes;

import java.util.ArrayList;
import java.util.List;

import kz.PvP.MedievalWars.Main;
import kz.PvP.MedievalWars.Enums.Config;
import kz.PvP.MedievalWars.Enums.GM;
import kz.PvP.MedievalWars.Enums.Team;
import kz.PvP.MedievalWars.Events.DamageListener;
import kz.PvP.MedievalWars.Events.InteractListener;
import kz.PvP.MedievalWars.Methods.ConvertTimings;
import kz.PvP.MedievalWars.Methods.GameManagement;
import kz.PvP.MedievalWars.Methods.ProtocolLib;
import kz.PvP.MedievalWars.Methods.Scoreboards;
import kz.PvP.MedievalWars.Methods.Stats;
import kz.PvP.MedievalWars.Methods.teamC;
import kz.PvP.MedievalWars.Methods.teamM;
import kz.PvP.MedievalWars.utilities.Files;
import kz.PvP.MedievalWars.utilities.MessageMW;
import kz.PvP.PkzAPI.utilities.Message;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Horse.Color;
import org.bukkit.entity.Player;
import org.bukkit.inventory.HorseInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;


public class Global {
	public static Main plugin;
	public Global(Main mainclass) {
		plugin = mainclass;
		}
	
	public static List<String> HorseNames = new ArrayList<String>();










public static Location GetLocOfString(String string, Config configFile) {
	Location loc = null;
	if (configFile.getCustomConfig().contains(string)){
		int x = (int) configFile.getCustomConfig().get(string + ".X");
		int y = (int) configFile.getCustomConfig().get(string + ".Y");
		int z = (int) configFile.getCustomConfig().get(string + ".Z");
		String world = configFile.getCustomConfig().getString(string + ".World");
		
		
		loc = new Location(plugin.getServer().getWorld(world),x,y,z);
		
		if (configFile.getCustomConfig().contains(string + ".Yaw")){
			loc.setYaw((float)configFile.getCustomConfig().getDouble(string + ".Yaw"));
		}
		if (configFile.getCustomConfig().contains(string + ".Pitch")){
			loc.setPitch((float)configFile.getCustomConfig().getDouble(string + ".Pitch"));
		}
	}
	else
		loc = plugin.getServer().getWorlds().get(0).getSpawnLocation();
	return loc;
}

public static void SpawnRedHorse(Location loc) {
	Horse h = (Horse) loc.getWorld().spawnEntity(loc, EntityType.HORSE);
	HorseInventory hi = h.getInventory();
	hi.setSaddle(new ItemStack(Material.SADDLE));
	h.setColor(Color.WHITE);
	h.setTamed(true);
	h.setMaxHealth(24.0);
	h.setCustomName(generateCustomName());
	h.setCustomNameVisible(true);
	teamC.getRed().addHorse(h.getUniqueId(), loc);
}

public static void SpawnBlueHorse(Location loc) {
	Horse h = (Horse) loc.getWorld().spawnEntity(loc, EntityType.HORSE);
	HorseInventory hi = h.getInventory();
	hi.setSaddle(new ItemStack(Material.SADDLE));
	h.setColor(Color.BLACK);
	h.setTamed(true);
	h.setMaxHealth(24.0);
	h.setCustomName(generateCustomName());
	h.setCustomNameVisible(true);
	teamC.getBlue().addHorse(h.getUniqueId(), loc);
}



private static String generateCustomName() {
	return HorseNames.get(ConvertTimings.randomInt(0, HorseNames.size() - 1));
}









public static void DeleteFromArrays(Player p, char c) {
	CTF.Compass.remove(p.getName());
	DamageListener.ShieldBlocks.remove(p.getName());
	if (c == 'F'){
		teamM.getTeam(Team.Red).remove(p);
		teamM.getTeam(Team.Blue).remove(p);
		teamM.getTeam(Team.Spectator).remove(p);
		GameManagement.IdleUsers.remove(p.getName());
		Stats.TotalKills.remove(p.getName());
		Scoreboards.Objectives.remove(p);
		Scoreboards.Boards.remove(p);
	}
}

public static void GiveItem(Player p, String string, Material nameTag, int slot, String loretext) {
	ItemStack selector = new ItemStack(nameTag);

	
	
	ItemMeta im = selector.getItemMeta();
	im.setDisplayName(ChatColor.GRAY + Message.Colorize(string));
	List<String> lore = new ArrayList<String>();
	if (loretext.contains("\n")){
	String[] loreitems = loretext.split("\n");
	for (String loretxt: loreitems){
		lore.add(loretxt);
	}
	}
	else
	lore.add(loretext);
	
	
	im.setLore(lore);
	selector.setItemMeta(im);
	
	
	if (Main.ProtocolLibEnable)
		selector = ProtocolLib.setGlowing(selector);
	if (slot != 1000)
		p.getInventory().setItem(slot, selector);
	else
		p.getInventory().addItem(selector);
}


public static void clearInventory(Player p, boolean clearArmor){
	
	p.getInventory().clear();
	if (clearArmor){
		p.getInventory().setHelmet(null);
		p.getInventory().setChestplate(null);
		p.getInventory().setLeggings(null);
		p.getInventory().setBoots(null);
	}
}


@SuppressWarnings("static-access")
public static void GiveTools(Player p, Team Team) {
	if (Team == Team.Spectator){
		GiveItem(p, "Class Selector", Material.EMERALD, 6, MessageMW.ClassDesc);
		GiveItem(p, "Upgrade Selector", Material.CARROT_STICK, 7, MessageMW.UpgradeDesc);
		GiveItem(p, "Activator", Material.FEATHER, 8, MessageMW.ActivatorDesc);
		GiveItem(p, "Spectate Compass", Material.COMPASS, 5, MessageMW.SpectateCompassDesc);
	}
	else{
		if (Main.Gamemode == GM.Mixed || Main.Gamemode == GM.CTF)
			GiveItem(p, MessageMW.ToolCompassName, Material.COMPASS, 1000, MessageMW.CompassDesc);
	}
	
}

public static void SpawnAllHorses() {
	System.out.println(Main.WarDisplayname);
	for (String HorseID : Files.worldteleports.getCustomConfig().getConfigurationSection(Main.WarDisplayname + ".Red.Horses").getKeys(false)){
		Global.SpawnBlueHorse(Global.GetLocOfString(Main.WarDisplayname + ".Red.Horses." + HorseID, Files.worldteleports));
	}
	for (String HorseID : Files.worldteleports.getCustomConfig().getConfigurationSection(Main.WarDisplayname + ".Blue.Horses").getKeys(false)){
		Global.SpawnBlueHorse(Global.GetLocOfString(Main.WarDisplayname + ".Blue.Horses." + HorseID, Files.worldteleports));
	}
}



public static String ChatRank(String pname, boolean Brackets) {
	int Kills = Stats.getKills(pname);
	
	String P = "";
	if (Kills <= 100)
		P = "Peasant";
	else if (Kills <= 250)
		P = "Lionheart";
	else if (Kills <= 650)
		P = "Squire";
	else if (Kills <= 875)
		P = "Crusader";
	else if (Kills <= 1200)
		P = "Baron";
	else if (Kills <= 1600)
		P = "Count";
	else if (Kills <= 2200)
		P = "Desperado";
	else if (Kills <= 3200)
		P = "War-Chief";
	else if (Kills <= 4000)
		P = "Overlord";
	else if (Kills <= 6000)
		P = "Bandito";
	else if (Kills <= 8000)
		P = "Justiciar";
	else if (Kills <= 10000)
		P = "Archon";
	else if (Kills <= 13000)
		P = "Emperor";
	else if (Kills <= 17000)
		P = "Wunderkind";
	else if (Kills <= 20000)
		P = "The Amazing";
	
	
	String Name = "";
	
	if (Brackets)
	Name = ChatColor.GRAY + "(" + ChatColor.GOLD  + P + ChatColor.GRAY + ") ";
	else
	Name =  ChatColor.GOLD  + P + ChatColor.GRAY;
	
	return Name;
}

}
