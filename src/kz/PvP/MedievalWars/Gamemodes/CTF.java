package kz.PvP.MedievalWars.Gamemodes;

import java.util.HashMap;

import kz.PvP.MedievalWars.Main;
import kz.PvP.MedievalWars.Enums.Team;
import kz.PvP.MedievalWars.Enums.TeamInfo;
import kz.PvP.MedievalWars.Methods.GameManagement;
import kz.PvP.MedievalWars.Methods.LocationsMW;
import kz.PvP.MedievalWars.Methods.teamC;
import kz.PvP.MedievalWars.Methods.teamM;
import kz.PvP.MedievalWars.utilities.MessageMW;
import kz.PvP.PkzAPI.utilities.Message;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;



public class CTF {
	public static Main plugin;
		public CTF(Main mainclass) {
			plugin = mainclass;
		}
		public static HashMap<String, Team> Compass = new HashMap<String, Team>();

	
	
	
	
		@SuppressWarnings({ "deprecation", "static-access" })
		public static void FlagDespawnTimer(final Location loc, final Team Team) {
			
			final byte b = Main.World.getBlockAt(loc).getData();
			if (Team == Team.Red){
				teamC.getRed().changeCapturer(null);
				teamC.getRed().changeFlagLoc(loc);
				Message.G(Message.Replacer(Message.Replacer(MessageMW.FlagDropped, Team.Red.name(), "%team"), "" + GameManagement.FlagDespawnTime, "%droptime"), false);
			}
			else if (Team == Team.Blue){
				teamC.getBlue().changeCapturer(null);
				teamC.getBlue().changeFlagLoc(loc);
				Message.G(Message.Replacer(Message.Replacer(MessageMW.FlagDropped, Team.Blue.name(), "%team"), "" + GameManagement.FlagDespawnTime, "%droptime"), false);
			}
			
			plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
					if (loc.getBlock().getTypeId() == 159 && loc.getBlock().getData() == b){
						loc.getBlock().setTypeId(0);
						if (Team == Team.Red){
							teamC.getRed().changeFlagLoc(LocationsMW.redFlag);
							SpawnFlag(LocationsMW.redFlag, Team.Red);
							Message.G(Message.Replacer(MessageMW.FlagReturned, Team.Red.name(), "%team"), true);
						}
						else if (Team == Team.Blue){
							teamC.getBlue().changeFlagLoc(LocationsMW.blueFlag);
							SpawnFlag(LocationsMW.redFlag, Team.Blue);
							Message.G(Message.Replacer(MessageMW.FlagReturned, Team.Blue.name(), "%team"), true);
						}
					}
				}
			}, 20L * GameManagement.FlagDespawnTime);
		}
	
		// Spawn Flag for CTF and Mixed gamemode
		@SuppressWarnings({ "deprecation", "static-access" })
		public static void SpawnFlag(Location location, Team Team) {
			Block flagblock = Main.World.getBlockAt(location);
			if (flagblock.getTypeId() != 0){
				if (!(flagblock.getTypeId() == 159 && (flagblock.getData() == 14 || flagblock.getData() == 11) && (Team == Team.Blue || Team == Team.Red))){
					flagblock.setTypeId(159);
					if (Team == Team.Blue){
						flagblock.setData((byte)11);// We spawn a blue colored stained clay
						teamC.getBlue().changeCapturer(null);
						teamC.getBlue().changeFlagLoc(location);
					}
					else if (Team == Team.Red){
						flagblock.setData((byte)14);// We spawn a red colored stained clay
						teamC.getRed().changeCapturer(null);
						teamC.getRed().changeFlagLoc(location);
					}
				}
				else{
					SpawnFlag(location.add(0, 1, 0), Team);
				}
			}
		}





		public static void changeFlagTarget(Player p) {
			if (Compass.get(p.getName()) == null){
				if (teamM.isInBlue(p.getName()))
					Compass.put(p.getName(), Team.Red);
				else if (teamM.isInRed(p.getName()))
					Compass.put(p.getName(), Team.Blue);
			}
			else if (Compass.get(p.getName()) == Team.Red){
				Compass.put(p.getName(), Team.Blue);
				Message.P(p, Message.Replacer(MessageMW.NowTrackingFlag, "&bBlue&7", "%team"), true);
			}
			else if (Compass.get(p.getName()) == Team.Blue){
				Compass.put(p.getName(), Team.Red);
				Message.P(p, Message.Replacer(MessageMW.NowTrackingFlag, "&cRed&7", "%team"), true);
			}
		}





		public static void getFlagDistance(Player p) {
			// TODO Auto-generated method stub
			if (Compass.get(p.getName()) == null){
				if (teamM.isInBlue(p.getName()))
					Compass.put(p.getName(), Team.Red);
				else if (teamM.isInRed(p.getName()))
					Compass.put(p.getName(), Team.Blue);
			}
			else if (Compass.get(p.getName()) == Team.Red){
			TeamInfo redTeamInfo = GameManagement.teamInfo.get(Team.Red);
			
			p.setCompassTarget(redTeamInfo.getFlagLoc());
			int blocks = (int) p.getLocation().distance(redTeamInfo.getFlagLoc());
			if (redTeamInfo.getCapturer() != null)
				Message.P(p, Message.Replacer(Message.Replacer(Message.Replacer(MessageMW.TrackingFlagHolder, redTeamInfo.getCapturer().getName(), "%holder"), "&cRed&7", "%team"), blocks + "", "%dist"), false);
			else
				Message.P(p, Message.Replacer(Message.Replacer(MessageMW.TrackingFlag, "&cRed&7", "%team"), blocks + "", "%dist"), false);
			}
			else if (Compass.get(p.getName()) == Team.Blue){
			TeamInfo blueTeamInfo = GameManagement.teamInfo.get(Team.Blue);
			p.setCompassTarget(blueTeamInfo.getFlagLoc());
			int blocks = (int) p.getLocation().distance(blueTeamInfo.getFlagLoc());
			
			if (blueTeamInfo.getCapturer() != null)
				Message.P(p, Message.Replacer(Message.Replacer(Message.Replacer(MessageMW.TrackingFlagHolder, blueTeamInfo.getCapturer().getName(), "%holder"), "&cBlue&7", "%team"), blocks + "", "%dist"), false);
			else
				Message.P(p, Message.Replacer(Message.Replacer(MessageMW.TrackingFlag, "&bBlue&7", "%team"), blocks + "", "%dist"), false);
			}
		}

		public static void updateCompass(Player p) {
			if (Compass.get(p.getName()) == null){
				if (teamM.isInBlue(p.getName()))
					Compass.put(p.getName(), Team.Red);
				else if (teamM.isInRed(p.getName()))
					Compass.put(p.getName(), Team.Blue);
				else 
					Compass.put(p.getName(), Team.Blue);
			}
			else if (Compass.get(p.getName()) == Team.Red){
			p.setCompassTarget(teamC.getRed().getFlagLoc());
			}
			else if (Compass.get(p.getName()) == Team.Blue){
			p.setCompassTarget(teamC.getBlue().getFlagLoc());
			}
		}
	
	}
