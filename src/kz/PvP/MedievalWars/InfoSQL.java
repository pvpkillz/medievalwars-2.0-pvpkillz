package kz.PvP.MedievalWars;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import kz.PvP.MedievalWars.Enums.RewardType;
import kz.PvP.MedievalWars.Methods.teamM;
import kz.PvP.MedievalWars.utilities.Kits;
import kz.PvP.MedievalWars.utilities.MessageMW;
import kz.PvP.PkzAPI.utilities.Message;
import kz.PvP.PkzAPI.utilities.Mysql;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;


public class InfoSQL {
	
	static Main plugin;
	
	public InfoSQL(Main mainclass) {
		plugin = mainclass;
	}
	
	
	
	public static int getPlayerPts(String name) throws SQLException {// We grab the user points
		// We create a query to grab the points from the database
		ResultSet res = Mysql.PS.getSecureQuery("SELECT * FROM Points WHERE ID = ?", ""+Mysql.GetUserID(name));
		
		
		int userpts;// Initialize the variable for points
		if (res.next())// If there is an entry in the database for point, we will just grab the column called 'Points' and return the value
			userpts = Integer.parseInt(res.getString("Points"));// We set the variable to the result
		else{// There is no one with that ID in the points Table..
			Mysql.PS.getSecureQuery("INSERT INTO Points (ID, Points) VALUES (?,?)", ""+Mysql.GetUserID(name), "0");
			// We insert a new entry in the database, so we can begin keeping track of their points.
			userpts = 0;// We set the current points to 0, since it is a new record.
		}
		return userpts;// We reutn the record.
	}
	
	//                                   User's Name, The change in points, RewardType -- rewardtype is an Enum, If you don't know what that is, it is basically a new variable type, with pre-defined variable values.
	public static boolean modifyUserPts (String p, int Change, boolean PrintMessage) throws SQLException{
		// We will be editing the user's points...
		
		int Player = Mysql.GetUserID (p);// We grab the ID | We use ID's instead of names.. Why? It saves a lot of space to save Integers than Strings.
		
		int curPts = getPlayerPts(p);// We grab their current points from the method above.
		Player user = Bukkit.getPlayerExact(p);// We get the actual player, so we can send them messages later

		if (Change < 0){// We want to take away coins
			if(curPts < Math.abs(Change)){// Not enough coins
				if (PrintMessage)
				Message.P(user, Message.Replacer(Message.Replacer(MessageMW.CannotAfford, "" + curPts, "<points>"), "" + Math.abs(Change), "<cost>"), true);
				return false;// User does not have enough money, so we don't continue and return false.
			}
			else if (curPts >= Math.abs(Change)){// Enough coins met
				int newPts = curPts - Math.abs(Change);
				if (PrintMessage)
				Message.P(user, Message.Replacer(Message.Replacer(MessageMW.PurchaseSuccess, "" + newPts, "<got>"), "" + Math.abs(Change), "<paid>"), true);
				// We message them   >> I created my own Message Class I use to process all Messages. Why? It allows me to format them easily and makes it easier for me to localize my plugin into other languages
				Mysql.PS.getSecureQuery("UPDATE Points SET Points = ? WHERE ID = ?", ""+newPts, ""+Player);// We update the points
				
				return true;// We return that the user was able to complete a transaction without issues.
			}
			
		}
		else if (Change >= 0){// We want to give coins
			int newPts = curPts + Math.abs(Change);// We get the current amount of points and add the new value. ABS is useless, but ehh keep it.
			
			// Same as Above...
			Mysql.PS.getSecureQuery("UPDATE Points SET Points = ? WHERE ID = ?", ""+newPts, ""+Player);
			
			
			// We message the user of them earning points.
			if (PrintMessage)
			Message.P(user, Message.Replacer(Message.Replacer(MessageMW.EarnedPoints, "" + Math.abs(Change), "$"), "" + getPlayerPts(p), "<curpoints>"), true);
			return true;
		}
		return false;
	}
	
	
	// IGNORE THIS -- It is too complicated to explain at this time.
	public static String GetUserUps(String name) throws SQLException {
		String userups;
		String User = ""+Mysql.GetUserID(name);
		
		
		ResultSet res = Mysql.PS.getSecureQuery("SELECT * FROM Upgrades WHERE User = ?", User);
		
		
		if (res.next())
			userups = res.getString("Upgrades");
		else{
			Mysql.PS.getSecureQuery("INSERT INTO Upgrades (User, Upgrades) VALUES (?,?)", User);
			userups = " ";
		}
		return userups;
	}
	// IGNORE THIS -- It is too complicated to explain at this time.
	public static String GetUserActiveUps(String name) throws SQLException {
		String User = ""+Mysql.GetUserID(name);
		
		ResultSet res = Mysql.PS.getSecureQuery("SELECT * FROM ActiveUpgrade WHERE User = ?", User);
		String useractiveups;
		if (res.next())
			useractiveups = res.getString("ActiveUpgrade");
		else{
			Mysql.PS.getSecureQuery("INSERT INTO ActiveUpgrade (User, ActiveUpgrade) VALUES (?,?)", User, "");
			useractiveups = " ";
		}
		return useractiveups;
	}
	// IGNORE THIS -- It is too complicated to explain at this time.
	public static String BuyUpgrade(Player p, String upgrade, int cost, String Class) throws SQLException {
		ResultSet res = Mysql.PS.getSecureQuery("SELECT * FROM Upgrades WHERE User = ?", ""+Mysql.GetUserID(p.getName()));
		
		String userups = null;

		if (res.next()){
			userups = res.getString("Upgrades");
			if (userups.contains(" " + Class + ":" + upgrade.toLowerCase()))
			Message.P(p, MessageMW.YouAlreadyOwnUpgrade, true);
			else{
				if (modifyUserPts(p.getName(), -cost, false)){
					Message.P(p, MessageMW.SuccessPurchase, true);
					//ServerCon.createStatement().executeUpdate("UPDATE Upgrades SET Upgrades = '" + userups + " " + Class + ":" + upgrade.toLowerCase() + "|" + "' WHERE User = '" + Mysql.GetUserID(p.getName()) + "'");
					Mysql.PS.getSecureQuery("UPDATE Upgrades SET Upgrades = ? WHERE User = ?",  userups + " " + Class + ":" + upgrade.toLowerCase() + "|", ""+Mysql.GetUserID(p.getName()));
					ActivateUpgrade(p, upgrade, Class);
				}
				else
				Message.P(p, Message.Replacer(Message.Replacer(MessageMW.CannotAfford, "" + cost, "<cost>"), "" + getPlayerPts(p.getName()), "<points>"), true);
			}
		}
		return userups;
	}
	
	public static void ActivateUpgrade(Player p, String Upgrade, String Class) throws SQLException {
		int User = Mysql.GetUserID(p.getName());
		
		ResultSet res = Mysql.PS.getSecureQuery("SELECT * FROM ActiveUpgrade WHERE User = ?", ""+User);
		String userups = null;

		if (res.next()){
		
		userups = res.getString("ActiveUpgrade");
		
		String[] split = Upgrade.split(" ");
		if (userups.contains(" " + Class + ":" + Upgrade.toLowerCase())){
		Message.P(p, MessageMW.DeactivatedUpgrade, true);
		userups = userups.replace(" " + Class + ":" + Upgrade.toLowerCase() + "=", "");
		//ServerCon.createStatement().executeUpdate("UPDATE ActiveUpgrade SET ActiveUpgrade = '" + userups + "' WHERE User = '" + User + "'");
		Mysql.PS.getSecureQuery("UPDATE ActiveUpgrade SET ActiveUpgrade = ? WHERE User = ?",  userups, ""+User);

		}
		else{
			if (GetUserUps(p.getName()).contains(" " + Class + ":" + Upgrade.toLowerCase())){
				
			if (userups.toLowerCase().contains(" " + Class.toLowerCase() + ":" + split[0].toLowerCase())){
				String[] allups = userups.split("=");
				for (String up : allups){
					if (up.toLowerCase().contains(" " + Class.toLowerCase() + ":" + split[0].toLowerCase()))
						userups = userups.replace(up.toLowerCase() + "=", "");
				}
			}
			Message.P(p, Message.Replacer(MessageMW.ActivatedUpgrade, Class, "<class>"), true);
			
			if (teamM.NotAttacked.contains(p))
				Kits.GiveKit(p);
			
			
			//ServerCon.createStatement().executeUpdate("UPDATE ActiveUpgrade SET ActiveUpgrade = '" + userups + " " + Class + ":" + Upgrade.toLowerCase() + "=" + "' WHERE User = '" + User + "'");
			
			Mysql.PS.getSecureQuery("UPDATE ActiveUpgrade SET ActiveUpgrade = ? WHERE User = ?",  userups + " " + Class + ":" + Upgrade.toLowerCase() + "=", ""+User);

			}
			else
			Message.P(p, MessageMW.UnactivatedUpgrade, true);
		}
		
		
		}
		
		
		else{
		}
	}
	
	
	
	// This is basically a Time tracker, but ignore it... Its useless for now | but it kinda works.
	public static int getMwTime(String name) throws SQLException {
		int totalGameTime = 0;
		
		ResultSet rs = Mysql.PS.getSecureQuery("SELECT * FROM UserInfo WHERE User = ? LIMIT 1", ""+Mysql.GetUserID(name));
		if (rs.next())
			totalGameTime = rs.getInt("MWPlayTime");
		
		return totalGameTime;
	}
	
	
}
