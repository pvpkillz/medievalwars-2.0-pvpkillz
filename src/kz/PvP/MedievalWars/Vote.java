package kz.PvP.MedievalWars;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import kz.PvP.MedievalWars.Commands.PlayerCommands;
import kz.PvP.MedievalWars.Enums.Game;
import kz.PvP.MedievalWars.Enums.GM;
import kz.PvP.MedievalWars.utilities.IconMenu;
import kz.PvP.MedievalWars.utilities.MessageMW;
import kz.PvP.PkzAPI.utilities.Message;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;


public class Vote {
	public static Main plugin;
	public Vote(Main mainclass) {
		plugin = mainclass;
		}
	public static HashMap<GM, ArrayList<String>> TotalVotes = new HashMap<GM, ArrayList<String>>();// FullTeam Kills

	
	public static void ShowVoteMenu(Player p){
		
        if (Main.GameStatus == Game.PREGAME){
        	
        	
        	
        	if (!TotalVotes.containsKey(GM.CTF))
        		TotalVotes.put(GM.CTF, new ArrayList<String>());
        		
        	if (!TotalVotes.containsKey(GM.TDM))
        		TotalVotes.put(GM.TDM, new ArrayList<String>());
        	if (!TotalVotes.containsKey(GM.Mixed))
        		TotalVotes.put(GM.Mixed, new ArrayList<String>());
        	if (!TotalVotes.containsKey(GM.Elimination))
        		TotalVotes.put(GM.Elimination, new ArrayList<String>());
        	if (!TotalVotes.containsKey(GM.DTC))
        		TotalVotes.put(GM.DTC, new ArrayList<String>());
        	
        for (Entry<GM, ArrayList<String>> gamemodeVotes : TotalVotes.entrySet()){
        	if (gamemodeVotes.getValue().contains(p.getName())){
        		gamemodeVotes.getValue().remove(p.getName());
        		Message.P(p, MessageMW.YourVoteWasDeleted, true);
        	}
        }

		 
	    IconMenu menu = new IconMenu("Voting Period", p.getName(), 9, new IconMenu.OptionClickEventHandler() {
	        @Override
	        public void onOptionClick(final IconMenu.OptionClickEvent event) {
	        	final Player p = event.getPlayer();
	        	if (event.getPosition() == 0 || event.getPosition() == 8){
		        	TotalVotes.get(GM.Mixed).add(p.getName());
		        	Message.P(p, "You have voted for Mixed game.", true);
	        	}
	        	else if (event.getPosition() == 2){
			        TotalVotes.get(GM.TDM).add(p.getName());
		        	Message.P(p, "You have voted for Team Deathmatch.", true);
	        	}
	        	else if (event.getPosition() == 4){
			        TotalVotes.get(GM.DTC).add(p.getName());
		        	Message.P(p, "You have voted for Capture the flag.", true);
	        	}
		        else if (event.getPosition() == 6){
		        	TotalVotes.get(GM.Elimination).add(p.getName());
		        	Message.P(p, "You have voted for Elimination.", true);
	            }
	            event.setWillClose(true);
	            event.setWillDestroy(false);
	        }
	    }, plugin)
	    .setOption(0, new ItemStack(Material.DIAMOND_PICKAXE, 1), ChatColor.GRAY + "Mixed Game", ChatColor.WHITE + "Vote for a Mixed gamemode!")
	    .setOption(2, new ItemStack(Material.DIAMOND_SWORD, 1), ChatColor.GOLD + "TDM", ChatColor.RED + "Vote for Team Deathmatch!")
	    .setOption(4, new ItemStack(Material.WOOL, 1), ChatColor.GREEN + "CTF", ChatColor.YELLOW + "Vote for capture the flag!")
	    .setOption(6, new ItemStack(Material.GOLD_SWORD, 1), ChatColor.RED + "Elimination", ChatColor.AQUA + "Vote for Team Elimination!")
	    .setOption(8, new ItemStack(Material.DIAMOND_PICKAXE, 1), ChatColor.GRAY + "Mixed Game", ChatColor.WHITE + "Vote for a Mixed gamemode!");
	    if (PlayerCommands.MENUS.containsKey(p.getName())){
	    	PlayerCommands.MENUS.get(p.getName()).destroy();
	    	PlayerCommands.MENUS.remove(p.getName());
	    }
	        menu.open(p);
	        PlayerCommands.MENUS.put(p.getName(), menu);
		
        }
        else{
        	p.sendMessage(ChatColor.RED + "You cannot vote at this time. Wait until round ends.");
        }
	}
	
	
	
	
}
