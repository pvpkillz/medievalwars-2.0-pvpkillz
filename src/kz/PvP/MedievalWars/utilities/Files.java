package kz.PvP.MedievalWars.utilities;

import java.util.logging.Logger;

import kz.PvP.MedievalWars.Main;
import kz.PvP.MedievalWars.Enums.Config;

import org.bukkit.Bukkit;


public class Files{
	public static Main plugin;
	static Logger log = Bukkit.getLogger();

	public static Config config;
	public static Config deathconf;
	public static Config classes;
	public static Config upgrades;
	public static Config worlds;
	public static Config worldteleports;
	public static Config bookconf;
	
	
	public Files (Main pl){
		plugin = pl;
		upgrades = new Config("upgrades", plugin);
		worlds = new Config("world", plugin);
		worldteleports = new Config("worldteleports", plugin);
		deathconf = new Config("deathmsgs", plugin);
		classes = new Config("classes", plugin);
		config = new Config("config", plugin);
		bookconf = new Config("book", plugin);
	}
}