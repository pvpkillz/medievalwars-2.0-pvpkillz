package kz.PvP.MedievalWars.utilities;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import kz.PvP.MedievalWars.Main;
import kz.PvP.MedievalWars.InfoSQL;
import kz.PvP.MedievalWars.Commands.PlayerCommands;
import kz.PvP.MedievalWars.Enums.Game;
import kz.PvP.MedievalWars.Enums.KitType;
import kz.PvP.MedievalWars.Enums.Team;
import kz.PvP.MedievalWars.Events.DamageListener;
import kz.PvP.MedievalWars.Events.InteractListener;
import kz.PvP.MedievalWars.Gamemodes.CTF;
import kz.PvP.MedievalWars.Gamemodes.Global;
import kz.PvP.MedievalWars.Methods.PowerUps;
import kz.PvP.MedievalWars.Methods.teamM;
import kz.PvP.PkzAPI.utilities.Message;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;



public class Kits {
	public static HashMap<Player, String> KitChoice = new HashMap<Player, String>();
	public static HashMap<Player, String> KitRound = new HashMap<Player, String>();
	public static HashMap<String, ArrayList<Integer>> Abilities = new HashMap<String, ArrayList<Integer>>();

	public static ArrayList<String> Kits = new ArrayList<String>();
	public Main plugin;
	
	public Kits(Main mainclass) {
	 	this.plugin = mainclass;

		Set<String> kitList = Files.classes.getCustomConfig().getKeys(false);
		for(String kit : kitList) {
			Kits.add(kit.toLowerCase());
		}
	}
	
	
	public static void GiveKit (Player p) throws SQLException{
		p.getInventory().clear();
		p.getInventory().setChestplate(null);
		p.getInventory().setLeggings(null);
		p.getInventory().setBoots(null);
		p.setExp(0);
		p.setLevel(0);
		p.eject();
	    for (PotionEffect effect : p.getActivePotionEffects())
	        p.removePotionEffect(effect.getType());
	    
	    InteractListener.Charges.remove(p.getName());
		CTF.Compass.remove(p.getName());
		DamageListener.ShieldBlocks.remove(p.getName());
		
		
		
		
		
		if (teamM.isInTeam(p.getName())){
			if (!KitChoice.containsKey(p)){
			// If player did not choose a kit
			}
			else {
				if (KitRound.containsKey(p))
				KitRound.remove(p);
				KitRound.put(p, KitChoice.get(p));
				ConfigurationSection kit = Files.classes.getCustomConfig().getConfigurationSection(KitChoice.get(p));
				
				if (!Abilities.containsKey(p.getName()))
					Abilities.put(p.getName(), new ArrayList<Integer>());
				
				if (kit.contains("Abilities"))
				Abilities.get(p.getName()).addAll(kit.getIntegerList("Abilities"));
				
				
				List<String> kititems = kit.getStringList("Items");
				for (String item : kititems) {
					getItemInfo(item, p);
				}
				List<String> pots = kit.getStringList("Potions");
				for(String pot : pots) {
					getPotionInfo(pot, p);
				}
				
				
				
				
				
				int bandaids = kit.getInt("PlayerBandage");
				int Hbandaids = kit.getInt("HorseBandage");
				if (bandaids >= 1){
				ItemStack is = new ItemStack(Material.NETHER_STAR, bandaids);
				ItemMeta im = is.getItemMeta();
				im.setDisplayName(ChatColor.RED + "+ " + ChatColor.WHITE + "Player Bandaid" + ChatColor.RED + " +" );
				List<String> Lore = new ArrayList<String>();
				Lore.add(ChatColor.WHITE + "Use this item, to");
				Lore.add(ChatColor.WHITE + "instantly heal yourself.");
				Lore.add(ChatColor.WHITE + " ");
				Lore.add(ChatColor.WHITE + " ");
				if (p.hasPermission("mw.instantheal"))
				Lore.add(ChatColor.WHITE + "- Tap shift to instantly heal");
				else
				Lore.add(ChatColor.RED + "[VIP+] - Tap shift to instantly heal");
				
				
				
				
				im.setLore(Lore);
				is.setItemMeta(im);
				p.getInventory().addItem(is);
				Lore.clear();
				}
				if (Hbandaids >= 1){
				ItemStack is = new ItemStack(Material.GHAST_TEAR, bandaids);
				ItemMeta im = is.getItemMeta();
				im.setDisplayName(ChatColor.RED + "+ " + ChatColor.WHITE + "Horse Bandaid" + ChatColor.RED + " +" );
				List<String> Lore = new ArrayList<String>();
				Lore.add(ChatColor.WHITE + "Use this item, to");
				Lore.add(ChatColor.WHITE + "instantly heal a horse.");
				im.setLore(Lore);
				is.setItemMeta(im);
				p.getInventory().addItem(is);
				Lore.clear();
				}
				
				
				
				
			}
		}
	Global.GiveTools( p, Team.Red);// Does not matter... They are a player...
	}
	
	
	
	
	
	
	
	@SuppressWarnings("deprecation")
	public static boolean hasItemAbility (Player p, KitType type, ItemStack is, PotionEffect pe) {
		if (KitRound.get(p) != null){
		if (type == KitType.ITEM){
			ConfigurationSection kit = Files.classes.getCustomConfig().getConfigurationSection(KitRound.get(p));
			List<String> items = kit.getStringList("Items");
			for (String item : items){
				String[] oneitem = item.split(",");
				ItemStack i = null;
				Integer id = null;
				Integer amount = null;
				Short durability = null;
				if (oneitem[0].contains(":")) {
					String[] ITEM_ID = oneitem[0].split(":");
					id = Integer.valueOf(Integer.parseInt(ITEM_ID[0]));
					amount = Integer.valueOf(Integer.parseInt(oneitem[1]));
					durability = Short.valueOf(Short.parseShort(ITEM_ID[1]));
					i = new ItemStack(id.intValue(), amount.intValue(),
							durability.shortValue());
				} else {
					id = Integer.valueOf(Integer.parseInt(oneitem[0]));
					amount = Integer.valueOf(Integer.parseInt(oneitem[1]));
					i = new ItemStack(id.intValue(), amount.intValue());
				}
				String name = "";
				if (item.contains(">")){
				String[] I = item.split(">");
				name = I[1];
				}
				
				if (i != null && is != null && i.getTypeId() == is.getTypeId() && ((is.hasItemMeta() && is.getItemMeta().hasDisplayName()) && (ChatColor.stripColor(is.getItemMeta().getDisplayName()).toLowerCase().contains(name.toLowerCase())))){
				return true;
				}
			}
		}
		else if (type == KitType.POTION){
			ConfigurationSection kit = Files.classes.getCustomConfig().getConfigurationSection(KitRound.get(p));
			List<String> potions = kit.getStringList("Potions");
			for (String pot : potions){
					if (pot != null & pot != "") {
						if (!pot.equals(0)) {
							String[] potion = pot.split(",");
							if (Integer.parseInt(potion[0]) != 0) {
								if (Integer.parseInt(potion[1]) == 0) {
									PotionEffect potioneffect = new PotionEffect(PotionEffectType
											.getById(Integer.parseInt(potion[0])),
											 120000, Integer
											.parseInt(potion[2]));
									if (pe == potioneffect)
									return true;
									
									
								} else {
									PotionEffect potioneffect = new PotionEffect(PotionEffectType
											.getById(Integer.parseInt(potion[0])), Integer
											.parseInt(potion[1]) * 20, Integer
											.parseInt(potion[2]));
									if (pe == potioneffect)
										return true;
									
								}
							}
						}
					}
			}
			
		}
		}
		return false;
	}
	
	public static void Choose(Player p, String kitname) throws SQLException {
		// TODO Auto-generated method stub
		  if (Kits.contains(kitname)){
			  Message.P(p, Message.Replacer(MessageMW.ChosenClass, Message.CleanCapitalize(kitname), "%class"), true);
			  if ((Main.GameStatus != Game.PREGAME) && (!teamM.isInSpec(p.getName())) && (!teamM.NotAttacked.contains(p.getName())))
				  Message.P(p, Message.Replacer(MessageMW.ClassChange, Message.CleanCapitalize(kitname), "%class"), true);
			  else if (teamM.isInSpec(p.getName())){
				  PlayerCommands.OpenTeamMenu(p);
			  }
			  KitChoice.remove(p);
			  KitChoice.put(p, kitname);
			  if (teamM.NotAttacked.contains(p.getName())){
				  GiveKit(p);
			  }
		  }
		  else{
			  Message.P(p, MessageMW.InvalidClass, true);
			  PlayerCommands.OpenClassMenu(p);
		  }
	}
	
	
	@SuppressWarnings("deprecation")
	public static PotionEffect getPotionInfo(String pot, Player p) {
		PotionEffect PE = null;
		if (pot != null & pot != "") {
			if (!pot.equals(0)) {
				String[] potion = pot.split(",");
				if (Integer.parseInt(potion[0]) != 0) {
					if (Integer.parseInt(potion[1]) == 0) {
						PE = new PotionEffect(PotionEffectType
								.getById(Integer.parseInt(potion[0])),
								 120000, Integer
								.parseInt(potion[2]));
						if (p != null)
						p.addPotionEffect(PE);
					} else {
						PE = new PotionEffect(PotionEffectType
								.getById(Integer.parseInt(potion[0])), Integer
								.parseInt(potion[1]) * 20, Integer
								.parseInt(potion[2]));
						if (p != null)
						p.addPotionEffect(PE);
					}
				}
			}
		}
		return PE;
	}
	
	@SuppressWarnings("deprecation")
	public static ItemStack getItemInfo (String item, Player p) throws SQLException{
		String[] oneitem = item.split(",");
		ItemStack i = null;
		Integer id = null;
		Integer amount = null;
		Short durability = null;
		String enchantment = null;
		String enchantment1 = null;
		String enchantment2 = null;
		String ench_numb = null;
		String ench_numb1 = null;
		String ench_numb2 = null;
		String itemstring = null;
		
		int blue = 0;
		int green = 0;
		int red = 0;
		if (p != null){
			if (teamM.isInBlue(p.getName())){
				blue = 255;
				green = 0;
				red = 0;
			}
			else if (teamM.isInRed(p.getName())){
				blue = 0;
				green = 0;
				red = 255;
			}
		}
		if (oneitem[0].contains(":")) {
			String[] ITEM_ID = oneitem[0].split(":");
			id = Integer.valueOf(Integer.parseInt(ITEM_ID[0]));
			amount = Integer.valueOf(Integer.parseInt(oneitem[1]));
			durability = Short.valueOf(Short.parseShort(ITEM_ID[1]));
			i = new ItemStack(id.intValue(), amount.intValue(),
					durability.shortValue());
		} else {
			id = Integer.valueOf(Integer.parseInt(oneitem[0]));
			amount = Integer.valueOf(Integer.parseInt(oneitem[1]));
			i = new ItemStack(id.intValue(), amount.intValue());
		}
		
		if (i.hasItemMeta() && i.getItemMeta().hasLore())
			i.getItemMeta().getLore().clear();
		
		
		itemstring = i.getType().toString();
		if (oneitem.length >= 8) {
			enchantment = Enchantment
					.getById(Integer.parseInt(oneitem[2])).getName()
					.toLowerCase();
			ench_numb = oneitem[3];
			
			enchantment1 = Enchantment
					.getById(Integer.parseInt(oneitem[4])).getName()
					.toLowerCase();
			ench_numb1 = oneitem[5];
			
			enchantment2 = Enchantment
					.getById(Integer.parseInt(oneitem[6])).getName()
					.toLowerCase();
			ench_numb2 = oneitem[7];
			
			i.addUnsafeEnchantment(Enchantment.getById(Integer.parseInt(oneitem[2])), Integer.parseInt(oneitem[3]));
			i.addUnsafeEnchantment(Enchantment.getById(Integer.parseInt(oneitem[4])), Integer.parseInt(oneitem[5]));
			i.addUnsafeEnchantment(Enchantment.getById(Integer.parseInt(oneitem[6])), Integer.parseInt(oneitem[7]));

			itemstring = itemstring + " with " + enchantment + " "
					+ ench_numb + ",\n " + enchantment1 + " " + ench_numb1 + " and " + enchantment2 + " " + ench_numb2;
		}
		else if (oneitem.length >= 6) {
			enchantment = Enchantment
					.getById(Integer.parseInt(oneitem[2])).getName()
					.toLowerCase();
			ench_numb = oneitem[3];
			
			enchantment1 = Enchantment
					.getById(Integer.parseInt(oneitem[4])).getName()
					.toLowerCase();
			ench_numb1 = oneitem[5];
			
			i.addUnsafeEnchantment(Enchantment.getById(Integer.parseInt(oneitem[2])), Integer.parseInt(oneitem[3]));
			i.addUnsafeEnchantment(Enchantment.getById(Integer.parseInt(oneitem[4])), Integer.parseInt(oneitem[5]));

			itemstring = itemstring + " with " + enchantment + " "
					+ ench_numb + " and " + enchantment1 + " " + ench_numb1;
		}
		
		else if (oneitem.length >= 4) {
			enchantment = Enchantment
					.getById(Integer.parseInt(oneitem[2])).getName()
					.toLowerCase();
			ench_numb = oneitem[3];
			
			i.addUnsafeEnchantment(Enchantment.getById(Integer.parseInt(oneitem[2])), Integer.parseInt(oneitem[3]));
			itemstring = itemstring + " with " + enchantment + " "
					+ ench_numb;
		}
		
		if (item.contains(">") && p != null){
			String[] I = item.split(">");
			ItemMeta im = i.getItemMeta();
			im.setDisplayName(I[1]);
			i.setItemMeta(im);
		}
		if (itemstring != null && itemstring.contains("_"))
		itemstring = itemstring.replace('_', ' ');
		
		
		
		if (p == null){
			ItemMeta im = i.getItemMeta();
			im.setDisplayName(itemstring);
			i.setItemMeta(im);
		}
		
		
		
		if (p != null){
		String upgrades = InfoSQL.GetUserActiveUps(p.getName());
		Set<String> upgradelist = Files.upgrades.getCustomConfig().getConfigurationSection(KitChoice.get(p)).getKeys(false);
			for(String upgrade : upgradelist){
				if (upgrades != null && upgrades.contains(" " + KitChoice.get(p).toLowerCase() + ":" + upgrade.toLowerCase())){
				int PowerUpID = Files.upgrades.getCustomConfig().getInt(KitChoice.get(p) + "." + upgrade + ".PowerupID");
				
				
				
				if (PowerUpID == id){
					String enchant = Files.upgrades.getCustomConfig().getString(KitChoice.get(p) + "." + upgrade + ".Upgrade");
					String[] enchlist = enchant.split(",");
					int enchlvl = Integer.parseInt(enchlist[1]);
					int enchid = Integer.parseInt(enchlist[0]);
					i.addUnsafeEnchantment(Enchantment.getById(enchid), enchlvl);
				}
					if ((PowerUpID == 1000) || (PowerUpID == 2000) || (PowerUpID == 3000) || (PowerUpID == 4000)){
						
						int enchantmentid = 0;
						if (PowerUpID == 1000)// 1000 = Fire Protection
						enchantmentid = 1;
						else if (PowerUpID == 2000)// 2000 = Blast protection
						enchantmentid = 3;
						else if (PowerUpID == 3000)// 3000 = Projectile protection
						enchantmentid = 4;
						else if (PowerUpID == 4000)// 4000 = Protection
						enchantmentid = 0;
						
						String enchant = Files.upgrades.getCustomConfig().getString(KitChoice.get(p) + "." + upgrade + ".Upgrade");
						String[] enchlist = enchant.split(",");
						int enchlvl = Integer.parseInt(enchlist[1]);
						
						
						if ((id.intValue() == 299) || (id.intValue() == 303)
								|| (id.intValue() == 307) || (id.intValue() == 311)
								|| (id.intValue() == 315)) {
							// Chestplate
							i.addUnsafeEnchantment(Enchantment.getById(enchantmentid), enchlvl);
						} else if ((id.intValue() == 300) || (id.intValue() == 304)
								|| (id.intValue() == 308) || (id.intValue() == 312)
								|| (id.intValue() == 316)) {
							// Leggings
							i.addUnsafeEnchantment(Enchantment.getById(enchantmentid), enchlvl);
						} else if ((id.intValue() == 301) || (id.intValue() == 305)
								|| (id.intValue() == 309) || (id.intValue() == 313)
								|| (id.intValue() == 317)) {
								// Boots
								i.addUnsafeEnchantment(Enchantment.getById(enchantmentid), enchlvl);
						}
					}
					else if (PowerUpID >= 10000){
						if (PowerUps.hasPowerUp(p, PowerUpID) == false)
							PowerUps.addPowerUp(p, PowerUpID);
					}
				// 20000+ are for custom coded abilities.
				}
			}
		}
		
		if ((id.intValue() < 298) || (317 < id.intValue())) {
			if (p != null)
			p.getInventory().addItem(new ItemStack[] { i });
		} else if ((id.intValue() == 298) || (id.intValue() == 302)
				|| (id.intValue() == 306) || (id.intValue() == 310)
				|| (id.intValue() == 314)) {
			if (id.intValue() == 298)
			{
			    LeatherArmorMeta c = (LeatherArmorMeta)i.getItemMeta();
			    c.setColor(Color.fromBGR(blue, green, red));
			    i.setItemMeta(c);
			}
			i.setAmount(1);
			if (p != null)
			p.getInventory().setHelmet(i);
		} else if ((id.intValue() == 299) || (id.intValue() == 303)
				|| (id.intValue() == 307) || (id.intValue() == 311)
				|| (id.intValue() == 315)) {
			if (id.intValue() == 299)
			{
			    LeatherArmorMeta c = (LeatherArmorMeta)i.getItemMeta();
			    c.setColor(Color.fromBGR(blue, green, red));
			    i.setItemMeta(c);
			}
			i.setAmount(1);
			if (p != null)
			p.getInventory().setChestplate(i);
		} else if ((id.intValue() == 300) || (id.intValue() == 304)
				|| (id.intValue() == 308) || (id.intValue() == 312)
				|| (id.intValue() == 316)) {
			
			if (id.intValue() == 300)
			{
			    LeatherArmorMeta l = (LeatherArmorMeta)i.getItemMeta();
			    l.setColor(Color.fromBGR(blue, green, red));
			    i.setItemMeta(l);
			}
			
			i.setAmount(1);
			if (p != null)
			p.getInventory().setLeggings(i);
		} else if ((id.intValue() == 301) || (id.intValue() == 305)
				|| (id.intValue() == 309) || (id.intValue() == 313)
				|| (id.intValue() == 317)) {
			if (id.intValue() == 301)
			{
			    LeatherArmorMeta b = (LeatherArmorMeta)i.getItemMeta();
			    b.setColor(Color.fromBGR(blue, green, red));
			    i.setItemMeta(b);
			}
			i.setAmount(1);
			if (p != null)
			p.getInventory().setBoots(i);
		}
		
		
		return i;
	}
	
	
	public static void ChangeItemname(String newname, ItemStack is){
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(newname);
		is.setItemMeta(im);
	}
	
	
	
	
	
}
