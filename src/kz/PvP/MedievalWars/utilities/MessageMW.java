package kz.PvP.MedievalWars.utilities;

import java.util.logging.Logger;

import kz.PvP.MedievalWars.Main;
import kz.PvP.MedievalWars.Enums.Team;
import kz.PvP.MedievalWars.Methods.teamM;
import kz.PvP.PkzAPI.utilities.Message;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;



public class MessageMW{
	public static Main plugin;
	public static String PREFIX = ChatColor.RED + "[" + ChatColor.GOLD + "MW" + ChatColor.RED + "] " + ChatColor.GREEN;
	/*
	 *  Translation file (Will be extra, since this plugin will be used in english, but I need a challenge)
	 */
	
	public static final String ServerName = "PvPKillz";
	public static final String PrepareTimer =  "The war has begun! May the best team win!";
	public static final String NotEnoughOn = ChatColor.GREEN + "Not enough players on both teams to begin.";
	public static final String GameBegunSpec = ChatColor.LIGHT_PURPLE + "The games have begun, to play you must be on a team. Type " + ChatColor.GOLD + "/team" + ChatColor.LIGHT_PURPLE + " for more info.";
	public static final String DoNotNotify = ChatColor.GRAY + "You currently are using the default class. To choose a class type " + ChatColor.GREEN + "/class" + ChatColor.GRAY + ". Turn off this notification, with " + ChatColor.RED + "/mv notify off";
	public static final String Idle = ChatColor.GRAY + "You have been moved to spectators, due to idling for %time.";
	public static final String JoinGame = ChatColor.GREEN + "To join the game, type " + ChatColor.RED + "/join";
	public static final String YouAlreadyOwnUpgrade = ChatColor.RED + "You already own this upgrade.";
	public static final String SuccessPurchase = ChatColor.GREEN + "You have successfully purchased this upgrade!";
	public static final String CannotAfford = ChatColor.YELLOW + "You cannot afford this purchase. " + ChatColor.RED + "Cost: <cost> " + ChatColor.GRAY + "|" + ChatColor.GREEN + " Balance: <points>";
	public static final String DeactivatedUpgrade = ChatColor.RED + "You have deactivated this upgrade.";
	public static final String ActivatedUpgrade = ChatColor.GREEN + "Upgrade has been activated for the <class> class! To deactivate type " + ChatColor.GOLD + "/activate";
	public static final String UnactivatedUpgrade = ChatColor.RED + "Upgrade not activated. You must purchase the upgrade first. " + ChatColor.GOLD + "/Upgrade";
	public static final String CompassDesc = "Allows user to trace down \ntheir team's or the opposing \nteam's flag.";
	public static final String CTFDesc = "Your objective in this gamemode, is to capture the other team's flag a total of <flags> time(s). First team to capture their enemy's flag <flags> wins the round.";
	public static final String MixedDesc = "Your objective in this gamemode, is to capture the enemy's flag and kill enemy players as many times as you can. \n Point Rewards:\n" + ChatColor.GRAY + "Player kill - " + ChatColor.GREEN + 1 + " Point(s)\n" + ChatColor.GRAY + "Flag capture - " + ChatColor.GREEN + "<points> Point(s)";
	public static final String TDMDesc = "Your objective in this gamemode, is to kill enemy players as many times as you can. \n Point Rewards:\n" + ChatColor.GRAY + "Player kill - " + ChatColor.GREEN + " <perkill> Point(s)\n";
	public static final String TeamEliminationDesc = "Your objective in this gamemode, is to kill enemy players as many times as you can, as quickly as you can.\n " + "Players who die, will be kicked out of the round. First team, to eliminate the full opposing team, wins the round." + "\n" + "Point Rewards:\n" + ChatColor.GRAY + "Player kill - " + ChatColor.GREEN + "<perkill> Point(s)\n";;;
	public static String UpgradeDesc = "Use this item in hand,\n to open the upgrade menu.";
	public static String ClassDesc = "Use this item in hand, \nto open the class selection menu.";
	public static String ActivatorDesc = "Use this item in hand, \nto open the upgrade-activation menu.";
	public static final String GivenPoints = ChatColor.GRAY + "You have earned <earned> points, and have a total of <got> points.";
	public static final String NotEnoughMoney = ChatColor.RED + "You do not have enough coins. You need <need> and have only <got>";
	public static final String PurchaseSuccess = ChatColor.GREEN + "Successful transfer, you now have <got> after paying <paid>";
	public static final String EarnedPoints = ChatColor.GOLD + "You have earned $ points <reason>. Total: <curpoints>";
	public static final String YouJustgotEliminated = "You have just been eliminated from Team Eimination.";
	public static final String TimeLeft = "This round will end in %time.";
	public static final String SwappedDueToBalance = "You have been swapped to the opposing team, due to auto-balance.";
	public static final String TypeHelpforHelp = "Type /help at anytime for more help.";
	public static final String SpectateCompassDesc = "Use this compass\nto teleport to users playing.";
	public static final String FeedbackPlease = "&aPlease help us &7make this gamemode better by leaving your feedback. Type &a/Feedback &7for more info.";
	public static final String TexturePackUsed = "Are you using our specially made resourcepack? Download it today from http://PvP.kz/MW.zip";
	public static final String AllPlayersRandomized = "All players have been assigned a random team. Beware this occurs every 5 rounds.";
	public static final String GivenBuffForDeath3 = "Deathstreak: You have been given a small strength buff.";
	public static final String GivenBuffForDeath5 = "Deathstreak: You have been given a medium strength buff.";
	public static final String GivenBuffForDeath10 = "Deathstreak: You have been given a strong strength buff.";
	public static final String ActivatedCannon = "This cannon has been activated.";
	public static final String Day = "day";
	public static final String Days = "days";
	public static final String HaveTimeLeft = "&7You have &5%time&7 to enter the command.";
	public static final String Hours = "hours";
	public static final String Hour = "hour";
	public static final String Minutes = "minutes";
	public static final String Minute = "minute";	
	public static final String Second = "second";	
	public static final String Seconds = "seconds";
	public static final String YourVoteWasDeleted = "Your previous vote has been over-written.";
	public static final String TeamHasTooMany = "The %team has more players than the opposing team.";
	public static final String ChosenClass = "&7You have chosen the &a%class&7.";
	public static final String ClassChange = "&aYour class adjustment to &a%class&7 will happen on your next respawn.";
	public static final String InvalidClass = "&cError:&7 Invalid class";
	public static final String JoinedServer = "&6%player &7has joined the game.";
	public static final String LeaveTheGame = "You can leave the game by typing /Leave";
	public static final String MWBorder = "&6&m--------&a MedievalWars &6&m--------";
	public static final String CommandTeamUsage = "&7/&aTeam &8&m-- &7Choose a team";
	public static final String CommandPointsUsage = "&7/&aP &8&m-- &7View points";
	public static final String CommandClassUsage = "&7/&aClass &8&m-- &7Choose a class";
	public static final String CommandUpgradeUsage = "&7/&aUpgrade &8&m-- &7Upgrade class";
	public static final String CommandActivateUsage = "&7/&aActivate &8&m-- &7Activate/Deactivate upgrades";
	public static final String CommandNotifyUsage = "&7/&aNotify &8&m-- &7Toggle Notifications";
	public static final String CommandLeaveUsage = "&7/&aLeave &8&m-- &7Spectate";
	public static final String TeleportedToPlayer = "You have teleported to %target.";
	public static final String TeamEliminationActive = "&7Team Elimination is currently active. &cYou are unable to join, until it ends.";
	public static final String NoPermission = "&aDoge&7: &cMuch hackor, such command, very admin.";
	public static final String NotificationDisabled = "&cNotifications have been disabled.";
	public static final String NotificationEnabled = "&aNotifications have been enabled.";
	public static final String StatsForUser = "&6&m-----&a %player's Stats &6&m-----";
	public static final String KillsNDeaths = "&b%kills &7Kills&8  |  &b%deaths &7Deaths";
	public static final String KDRatio = "Kill to Death ratio is %ratio";
	public static final String PointCount = "Point count of %points";
	public static final String CurrentRank = "Rank of %rank";
	public static final String PlayerNeverJoined = "No player joined our server with such name.";
	public static final String HelpCommands = "&7For MedievalWars help, type &a/mw";
	public static final String CurrentGamemode = "&7Active Gamemode: &a %gamemode";
	public static final String CurrentObjective = "&7Current Objective:&a \n %objective";
	public static final String PrivateMessageHelp = "&7Sends a &6private message&7 to a user online.";
	public static final String PrivateMessageCmd = "/tell <player> <message>";
	public static final String SentTo = "&7Me &l➔ &7 %target: %msg";
	public static final String SentFrom = "&7%sender &l➔ &7 Me: %msg";
	public static final String PlayerNotOnline = "&cPlayer is not online.";
	public static final String NoOneContactedYou = "&cYou have no one to reply to.";
	public static final String ReplyHelp = "Quickly reply to the last message sent.";
	public static final String ReplyHelpCmd = "&6/r &7<Message>";
	public static final String ChattingPublic = "All players can view your messages.";
	public static final String ChattingPrivate = "Your team can only view your messages.";
	public static final String ClassIncapableOfHorseRiding = "&cYour class is not fit for horse-riding.";
	public static final String FlagIncapableOfHorseRiding = "&cThe flag is too heavy for the horse to carry.";
	public static final String HorseIsFull = "&7Horse is already at full health.";
	public static final String HorseHealedToFull = "&aHorse has been healed &7to full health.";
	public static final String YourAreFull = "&7You are already at full health.";
	public static final String YouAreNowFull = "&aYou are now healthy.";
	public static final String HealingWandChargeEnd = "&7Your healing wand, has no charges left.";
	public static final String TrackingFlag = "&7Tracking the %team flag. Flag is %dist blocks away.";
	public static final String TrackingFlagHolder = "&7Tracking %team flag held by %holder %dist blocks away.";
	public static final String NowTrackingFlag = "&7Now tracking the %team team flag. ";
	public static final String FlagDropped = "&6The %team flag has been dropped.";
	public static final String FlagReturned = "&7The %team flag has returned back to the base.";
	public static final String FirstBlood = "&cFirst Blood &7by &6%killer";
	public static final String ChosenMap = "&7You are editing the %map map.";
	public static final String NoMapChosen = "You have not chosen a map to edit.";
	public static final String ToolCompassName = "&6Flag Tracker";
	public static final String AdminMapSet = "&7/&bmwa &6setmap &7<&aMap&7> &8 | Valid: &7%maps";
	public static final String AdminWarps = "&7/&bmwa &6warps &7<&bredspawn&7/&bbluespawn&7/&bmainlobby&7/&bdead&7/&bspectator&7>";
	public static final String AdminFlag = "&7/&bmwa &6flag &7<&bblue&7/&cred&7>";
	public static final String AdminFlagstop = "&7/&bmwa &6flagstop &7<&bblue&7/&cred&7>";
	public static final String AdminHorse = "&7/&bmwa &6horse &7<&bblue&7/&cred&7>";
	public static final String CoreHealthDropped = "Your enemy's nexus has dropped by %dmg and is now %hp health.";

	
	public MessageMW (Main mainclass){
		plugin = mainclass;
	}
	
	
	
	
static Logger log = Bukkit.getLogger();

	
	
	
	public static void G(String msg, Team team, boolean Prefix) {
		msg = ChatColor.translateAlternateColorCodes('&', ChatColor.GRAY + msg);
		
		
		for (Player pl : teamM.getTeam(team))
			Message.P(pl, msg, Prefix);
		
		
		log.info(ChatColor.stripColor(PREFIX) + ChatColor.stripColor(msg) + " | sent to all players of " + team + " group.");
	}
	
	
}
