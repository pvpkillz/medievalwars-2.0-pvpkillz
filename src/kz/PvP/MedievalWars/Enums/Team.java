package kz.PvP.MedievalWars.Enums;

public enum Team {
	
	Spectator, Red, Blue, Random, None, Forced;
	
}