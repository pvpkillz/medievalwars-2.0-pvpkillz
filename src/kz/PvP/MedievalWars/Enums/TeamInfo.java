package kz.PvP.MedievalWars.Enums;

import java.util.HashMap;
import java.util.UUID;

import kz.PvP.MedievalWars.Methods.GameManagement;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class TeamInfo {
	  private int flagCaptures;
	  private Player flagCapturer;
	  private Location flagLocation;
	  public Location coreLoc1;
	  public Location coreLoc2;
	  public HashMap<UUID, Location> horseIDs = new HashMap<UUID, Location>();// Horse Records	
	  public HashMap<String, Integer> Kills = new HashMap<String, Integer>();// Kills made by the team	
	  public int totalKills = 0;
	  public int totalScore = 0;
	  public int theCoreHP = GameManagement.CoreHP;
	  public TeamInfo() {
	  flagCaptures = 0;
	  totalKills = 0;
	  totalScore = 0;
	  flagCapturer = null;
	  flagLocation = null;
	  }
	  
	  public Player getCapturer() {
		  return flagCapturer;
		  }
	  public int getCaptures() {
		  return flagCaptures;
		  }
	  public int getCoreHP() {
		  return theCoreHP;
		  }
	  
	  
	  public int modifyCoreHP(int Change) {
		  if (Change > 0)
			  theCoreHP = Change;
		  else
			  theCoreHP = theCoreHP + Change;
		  
		  
		  return theCoreHP;
		  }
	  public Location getFlagLoc() {
		  return flagLocation;
		  }
	  public Location getCoreLoc1() {
		  return coreLoc1;
		  }
	  public Location getCoreLoc2() {
		  return coreLoc2;
		  }
	  public int getScore(){
		  return totalScore;
	  }
	  public HashMap<UUID, Location> getHorses(){
		  return horseIDs;
	  }
	  public int getTotalKills(){
		  return totalKills;
	  }
	  public int addScore(int score) {
		  totalScore = totalScore + score;
		  return totalScore;
		  }
	  
	  public Player changeCapturer(Player capturer) {
		  flagCapturer = capturer;
		  return flagCapturer;
		  }
	  public int addCaptures(int caps) {
		  flagCaptures = flagCaptures + caps;
		  return flagCaptures;
		  }
	  public Location changeFlagLoc(Location loc) {
		  flagLocation = loc;
		  return flagLocation;
		  }
	  public void addHorse(UUID ID, Location loc){
		  getHorses().put(ID, loc);
	  }
	  public void removeHorse(UUID ID){
		  getHorses().remove(ID);
	  }
}
