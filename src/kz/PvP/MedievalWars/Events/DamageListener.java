package kz.PvP.MedievalWars.Events;

import java.util.ArrayList;
import java.util.HashMap;

import kz.PvP.MedievalWars.Main;
import kz.PvP.MedievalWars.Enums.Game;
import kz.PvP.MedievalWars.Methods.PowerUps;
import kz.PvP.MedievalWars.Methods.teamM;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


public class DamageListener implements Listener{
	public static Main plugin;
	public static HashMap<String, Integer> ShieldBlocks = new HashMap<String, Integer>();
	public static ArrayList<String> IncreaseBlock1 = new ArrayList<String>();
	public static ArrayList<String> IncreaseBlock2 = new ArrayList<String>();
	public static ArrayList<String> IncreaseBlock3 = new ArrayList<String>();
	public static ArrayList<String> ArcherBlock1 = new ArrayList<String>();
	public static ArrayList<String> ArcherBlock2 = new ArrayList<String>();
	public static ArrayList<String> ArcherBlock3 = new ArrayList<String>();
	public static int HealingHP = 10;
	public DamageListener(Main mainclass) {
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
		}
	
	@EventHandler
	public void OnArrowLand (ProjectileHitEvent e){
		if (e.getEntity() instanceof Arrow)
		e.getEntity().remove();
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void OnPlayerAttack (EntityDamageByEntityEvent e){
		if (Main.GameStatus == Game.GAME){
		Entity damager = e.getDamager();
		Entity defender = e.getEntity();
		if (damager instanceof Player && defender instanceof Player){
			Player dam = (Player) damager;
			Player def = (Player) defender;			
			
			
			if (teamM.isInRed(dam.getName()) && teamM.isInRed(def.getName()))
				e.setCancelled(true);
			else if (teamM.isInBlue(dam.getName()) && teamM.isInBlue(def.getName()))
				e.setCancelled(true);
			else if (teamM.isInSpec(dam.getName()) && teamM.isInSpec(def.getName()))
				e.setCancelled(true);
			else if (DeathListener.x.containsKey(def) || DeathListener.x.containsKey(dam))
				e.setCancelled(true);
			else if ((teamM.isInBlue(dam.getName()) && teamM.isInRed(def.getName())) || (teamM.isInBlue(def.getName()) && teamM.isInRed(dam.getName()))){
				
				
				
			if (def.getItemInHand().hasItemMeta() && def.getItemInHand().getItemMeta().hasDisplayName()){
				if (!ShieldBlocks.containsKey(def.getName()))
					ShieldBlocks.put(def.getName(), 64);
				if (def.getItemInHand().getItemMeta().getDisplayName().toLowerCase().contains("shield") && ShieldBlocks.get(def.getName()) > 0){
					double newshield = 0.0;
					
					if (IncreaseBlock1.contains(def.getName()))
						newshield = (e.getDamage()/5) * 2;
					else if (IncreaseBlock2.contains(def.getName()))
						newshield = (e.getDamage()/10) * 3;
					else if (IncreaseBlock3.contains(def.getName()))
						newshield = (e.getDamage()/5) * 1;
					else
						newshield = (e.getDamage()/2) * 1;
					
					
					ShieldBlocks.put(def.getName(), ShieldBlocks.get(def.getName()) - 1);
					ItemMeta im = def.getItemInHand().getItemMeta();
					String[] realnamesplit = def.getItemInHand().getItemMeta().getDisplayName().split("   ");
					String realname = realnamesplit[0];
					im.setDisplayName(realname + "   " +  ChatColor.GOLD +  ShieldBlocks.get(def.getName()) + "/64");
					def.getItemInHand().setItemMeta(im);
					e.setDamage(newshield);
				}
				else if (def.getItemInHand().getItemMeta().getDisplayName().toLowerCase().contains("shield") && ShieldBlocks.get(def.getName()) <= 0){
					def.sendMessage(ChatColor.GRAY + "Your shield has broken.");
					def.setItemInHand(new ItemStack(Material.AIR));
					def.updateInventory();
				}
			}
			if (dam.getItemInHand().hasItemMeta()){
				if (dam.getItemInHand().getItemMeta().getDisplayName().toLowerCase().contains("shield")){
				e.setCancelled(true);
				dam.sendMessage(ChatColor.GRAY + "You are carrying a shield, which cannot be used to attack.");
				}
			}
			}
		}
		else if (e.getDamager() instanceof Projectile){
			Projectile projectile = (Projectile) e.getDamager();
			if (projectile.getShooter() instanceof Player){
				Player dam = (Player) projectile.getShooter();
				if (defender instanceof Player){
					Player def = (Player) defender;
					
					if (InteractListener.Healshot.contains(projectile.getUniqueId())){
						if (def.getHealth() >= (20 - HealingHP)){
							def.setHealth(20);
						}
						else {
							def.setHealth(def.getHealth() + HealingHP);
						}
						def.sendMessage(ChatColor.WHITE + "++  " + ChatColor.AQUA + "You have been healed by " + dam.getName() + ChatColor.WHITE + "  ++");
					}
					
					if ((teamM.isInBlue(dam.getName()) && teamM.isInRed(def.getName())) || (teamM.isInBlue(def.getName()) && teamM.isInRed(dam.getName()))){
						if (InteractListener.Crossbow.contains(projectile.getUniqueId())){
							e.setDamage(4.0);
							InteractListener.Crossbow.remove(projectile.getUniqueId());
						}
						if (InteractListener.Longbow.contains(projectile.getUniqueId())){
							e.setDamage(e.getDamage() + 3);
							InteractListener.Longbow.remove(projectile.getUniqueId());
						}
						if (InteractListener.PoisonousArrow.contains(projectile.getUniqueId())){
							Player p = teamM.getPlayerFromSome(dam.getName());
							
							if (PowerUps.hasPowerUp(p, PowerUps.PoisonUpgrade1))
								ChancePoison(5, def, 5);
							else if (PowerUps.hasPowerUp(p, PowerUps.PoisonUpgrade2))
								ChancePoison(10, def, 5);
							else if (PowerUps.hasPowerUp(p, PowerUps.PoisonUpgrade3))
								ChancePoison(15, def, 5);
						}
						
						if (def.getItemInHand().hasItemMeta()){
							if (!ShieldBlocks.containsKey(def.getName()))
							ShieldBlocks.put(def.getName(), 64);
						if (def.getItemInHand().getItemMeta().getDisplayName().toLowerCase().contains("shield") && ShieldBlocks.get(def.getName()) > 0){
							double newshield = 0.0;
							
							if (ArcherBlock1.contains(def.getName()))
							newshield = (e.getDamage()/5) * 3;
							else if (ArcherBlock2.contains(def.getName()))
							newshield = (e.getDamage()/2) * 1;
							else if (ArcherBlock3.contains(def.getName()))
							newshield = (e.getDamage()/5) * 2;
							else
							newshield = (e.getDamage()/4) * 3;
							
							
							ShieldBlocks.put(def.getName(), ShieldBlocks.get(def.getName()) - 1);
							ItemMeta im = def.getItemInHand().getItemMeta();
							String[] realnamesplit = def.getItemInHand().getItemMeta().getDisplayName().split("   ");
							String realname = realnamesplit[0];
							im.setDisplayName(realname + "   " +  ChatColor.GOLD +  ShieldBlocks.get(def.getName()) + "/64");
							def.getItemInHand().setItemMeta(im);
							e.setDamage(newshield);
						}
						}
					}
					else
						e.setCancelled(true);
			}
		}
		}
		}
		else{
			e.setCancelled(true);
		}
	}
	@EventHandler
	public void DamagePlayer (EntityDamageEvent e){
		if (e.getEntity() instanceof Player){
			Player def = (Player) e.getEntity();
			def.playSound(def.getLocation(), Sound.HURT_FLESH, 1f, 1f);
			if (teamM.NotAttacked.contains(def.getName())){
				teamM.NotAttacked.remove(def.getName());
			}
			if (teamM.Invincibility.contains(def.getName())){
				e.setCancelled(true);
			}
		}
	}
	
	private void ChancePoison(int i, Player def, int length) {
		double chance = (i * 0.01);
		double ran = Math.random();
		if (ran <= chance){
			def.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 20 * length, 2));
			def.sendMessage(ChatColor.DARK_GREEN + "You have been arrow poisoned.");
		}
	}
}
