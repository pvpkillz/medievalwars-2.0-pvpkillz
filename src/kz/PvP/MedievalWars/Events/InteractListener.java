package kz.PvP.MedievalWars.Events;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import kz.PvP.MedievalWars.Games;
import kz.PvP.MedievalWars.Main;
import kz.PvP.MedievalWars.Enums.GM;
import kz.PvP.MedievalWars.Enums.Team;
import kz.PvP.MedievalWars.Gamemodes.CTF;
import kz.PvP.MedievalWars.Methods.Cannons;
import kz.PvP.MedievalWars.Methods.GameManagement;
import kz.PvP.MedievalWars.Methods.LocationsMW;
import kz.PvP.MedievalWars.Methods.PowerUps;
import kz.PvP.MedievalWars.Methods.teamC;
import kz.PvP.MedievalWars.Methods.teamM;
import kz.PvP.MedievalWars.utilities.Files;
import kz.PvP.MedievalWars.utilities.Kits;
import kz.PvP.MedievalWars.utilities.MessageMW;
import kz.PvP.PkzAPI.utilities.Message;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.ExplosionPrimeEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.vehicle.VehicleExitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;


public class InteractListener implements Listener{
	public static Main plugin;
	public static ArrayList<UUID> Longbow = new ArrayList<UUID>();
	public static ArrayList<UUID> Crossbow = new ArrayList<UUID>();
	public static ArrayList<UUID> Healshot = new ArrayList<UUID>();
	public static ArrayList<UUID> PoisonousArrow = new ArrayList<UUID>();
	public static ArrayList<String> DelayShooter = new ArrayList<String>();
	
	int MaxCharges = 3;
	
	public static HashMap<String, Integer> Charges = new HashMap<String, Integer>();
	public static ArrayList<String> NotifiedPlayerSlot = new ArrayList<String>();


	public InteractListener(Main mainclass) {
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
	}
	
	
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void UseHorse (PlayerInteractEntityEvent e){
		if(e.getPlayer() instanceof Player && e.getRightClicked() instanceof Horse){
			Player p = e.getPlayer();
			Horse h = (Horse) e.getRightClicked();
			String Class = Kits.KitRound.get(p);
			if (Class != null && !Class.toLowerCase().contains("mounted")){
				e.setCancelled(true);
				Message.P(p, MessageMW.ClassIncapableOfHorseRiding, true);
			}
			else if (teamC.getRed().getCapturer() == p || teamC.getBlue().getCapturer() == p){
				e.setCancelled(true);
				Message.P(p, MessageMW.FlagIncapableOfHorseRiding, true);
			}
			else{
				if (p.getItemInHand().hasItemMeta() && p.getItemInHand().getItemMeta().hasDisplayName()){
					if (p.getItemInHand().getItemMeta().getDisplayName().toLowerCase().contains("horse bandaid")){
							if (h.getHealth() == h.getMaxHealth())
								Message.P(p, MessageMW.HorseIsFull, true);
							else if (h.getHealth() < h.getMaxHealth()){
								h.setHealth(h.getMaxHealth());
								if (p.getItemInHand().getAmount() > 1)
									p.getItemInHand().setAmount(p.getItemInHand().getAmount() - 1);
								else
									p.setItemInHand(new ItemStack(Material.AIR));
								p.updateInventory();
								Message.P(p, MessageMW.HorseHealedToFull, true);
							}
							e.setCancelled(true);
					}
				}
				else{
					boolean fillup = false;
					if (h.getMaxHealth() == h.getHealth())
						fillup = true;
							
					if (PowerUps.hasPowerUp(p, PowerUps.HorseHealthUpgrade1))
						h.setMaxHealth(24.0 + 6.0);
					else if (PowerUps.hasPowerUp(p, PowerUps.HorseHealthUpgrade2))
						h.setMaxHealth(24.0 + 12.0);
					else if (PowerUps.hasPowerUp(p, PowerUps.HorseHealthUpgrade3))
						h.setMaxHealth(24.0 + 20.0);
					if (fillup == true)
						h.setHealth(h.getMaxHealth());
				}
			}
		}
	}
	
	@EventHandler
	public void OnBlockChoose (VehicleExitEvent e){
		if (e.getVehicle() instanceof Horse && e.getExited() instanceof Player){
			Horse h = (Horse)e.getVehicle();
			
			if (h.getHealth() == h.getMaxHealth())
				h.setHealth(12.0);
			else if (h.getHealth() > 12.0)
				h.setHealth(11.0);
			
			h.setMaxHealth(12.0);
		}
	}
@SuppressWarnings("deprecation")
@EventHandler
public void OnBlockChoose (PlayerInteractEvent e){
	final Player p = e.getPlayer();
	
	if ((e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK)
			&& p.getItemInHand().hasItemMeta() == true && p.getItemInHand().getItemMeta().hasDisplayName() == true){
		
		
		if (p.getItemInHand().getItemMeta().getDisplayName().toLowerCase().contains("horse bandaid")){
			if (p.getVehicle() instanceof Horse){
				Horse h = (Horse)p.getVehicle();
				if (h.getHealth() == h.getMaxHealth()){
					Message.P(p, MessageMW.HorseIsFull, true);
				}
				else if (h.getHealth() < h.getMaxHealth()){
					h.setHealth(h.getMaxHealth());
					if (p.getItemInHand().getAmount() > 1)
					p.getItemInHand().setAmount(p.getItemInHand().getAmount() - 1);
					else
					p.setItemInHand(new ItemStack(Material.AIR));
					
					
					p.updateInventory();
					Message.P(p, MessageMW.HorseHealedToFull, true);
				}
			}
		}
		else if (p.getItemInHand().getItemMeta().getDisplayName().toLowerCase().contains("player bandaid")){
			if (p.getHealth() == p.getMaxHealth())
				Message.P(p, MessageMW.YourAreFull, true);
			else if (p.getHealth() < p.getMaxHealth()){
				p.setHealth(p.getMaxHealth());
				if (p.getItemInHand().getAmount() > 1)
					p.getItemInHand().setAmount(p.getItemInHand().getAmount() - 1);
				else
					p.setItemInHand(new ItemStack(Material.AIR));
				p.updateInventory();
				Message.P(p, MessageMW.YouAreNowFull, true);
    			
				p.setFireTicks(0);
			    for (PotionEffect effect : p.getActivePotionEffects()){
			        
			    	if (effect.getDuration() <= 20 * 60 * 5)
			    	p.removePotionEffect(effect.getType());
			    }
			}
			
		}
		else if (p.getItemInHand().getItemMeta().getDisplayName().toLowerCase().contains("healing wand") && Kits.KitRound.containsKey(p) && Files.classes.getCustomConfig().getBoolean(Kits.KitRound.get(p) + ".AllowHeal")){
			if (!Charges.containsKey(p.getName()))
			Charges.put(p.getName(), MaxCharges);
			
			
			if (!DelayShooter.contains(p.getName())){
			if (Charges.get(p.getName()) >= 1){
			Fireball fb = p.launchProjectile(Fireball.class);
			fb.setVelocity(p.getLocation().getDirection().multiply(2));
			fb.setShooter(p);
			fb.setIsIncendiary(false);
			
			Healshot.add(fb.getUniqueId());
			DelayShooter.add(p.getName());
			Charges.put(p.getName(), Charges.get(p.getName()) - 1);
			
			int ChargesLeft = Charges.get(p.getName());
			String[] wepname = p.getItemInHand().getItemMeta().getDisplayName().split("    ");
			String wep = wepname[0];
			ItemMeta im = p.getItemInHand().getItemMeta();
			im.setDisplayName(wep + "    " + ChatColor.YELLOW + ChargesLeft + "/" + MaxCharges);
			p.getItemInHand().setItemMeta(im);
			
			plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
				public void run() {
					DelayShooter.remove(p.getName());
				}
				}, 20L * 4);
			}
			else
				Message.P(p, MessageMW.HealingWandChargeEnd, true);
		  }
		}		
	}
	//Cannons.LaunchProjectile(p, e.getClickedBlock());
	if (Main.Gamemode == GM.CTF || Main.Gamemode == GM.Mixed){
		if (e.getClickedBlock() != null){
			if (e.getClickedBlock().getTypeId() == 159 && (e.getAction() == Action.LEFT_CLICK_AIR || e.getAction() == Action.LEFT_CLICK_BLOCK)){
				if (e.getClickedBlock().getData() == 11 || e.getClickedBlock().getData() == 14){
					if (e.getClickedBlock().getData() == 11 && teamM.isInRed(p.getName()) && teamC.getBlue().getFlagLoc().getBlockY() == e.getClickedBlock().getLocation().getBlockY() && teamC.getBlue().getFlagLoc().getBlockZ() == e.getClickedBlock().getLocation().getBlockZ() && teamC.getBlue().getFlagLoc().getBlockX() == e.getClickedBlock().getLocation().getBlockX()){
						plugin.getServer().getWorld("war").getBlockAt(e.getClickedBlock().getLocation()).setTypeId(0);
						ItemStack IS = new ItemStack(Material.STAINED_CLAY);
						IS.setDurability((short) 11);
						ItemMeta im = IS.getItemMeta();
						im.setDisplayName(ChatColor.BLUE + "Blue Flag");
						IS.setItemMeta(im);
						//e.getPlayer().getInventory().addItem(IS);
						ItemStack old = e.getPlayer().getInventory().getItemInHand();
						p.getInventory().setItemInHand(IS);
						p.getInventory().addItem(old);
						GameManagement.teamInfo.get(Team.Blue).changeCapturer(p);
						p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20 * 60 * 200, 0));
						plugin.getServer().broadcastMessage(ChatColor.GRAY + "The blue flag is now captured by " + ChatColor.RED + p.getName());
					}
					else if (e.getClickedBlock().getData() == 14 && teamM.isInBlue(p.getName())  && teamC.getRed().getFlagLoc().getBlockY() == e.getClickedBlock().getLocation().getBlockY() && teamC.getRed().getFlagLoc().getBlockZ() == e.getClickedBlock().getLocation().getBlockZ() && teamC.getRed().getFlagLoc().getBlockX() == e.getClickedBlock().getLocation().getBlockX()){
						plugin.getServer().getWorld("war").getBlockAt(e.getClickedBlock().getLocation()).setTypeId(0);
						ItemStack IS = new ItemStack(Material.STAINED_CLAY);
						IS.setDurability((short) 14);
						ItemMeta im = IS.getItemMeta();
						im.setDisplayName(ChatColor.RED + "Red Flag");
						IS.setItemMeta(im);
						//e.getPlayer().getInventory().addItem(IS);
						ItemStack old = e.getPlayer().getInventory().getItemInHand();
						p.getInventory().setItemInHand(IS);
						p.getInventory().addItem(old);
						GameManagement.teamInfo.get(Team.Red).changeCapturer(p);
						p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20 * 60 * 200, 0));
						plugin.getServer().broadcastMessage(ChatColor.GRAY + "The red flag is now captured by " + ChatColor.AQUA + p.getName());
					}
					
					if (p.getVehicle() != null)
					p.getVehicle().eject();
					e.getPlayer().updateInventory();
				}
			}
		}
	}
	if (Main.Gamemode == GM.CTF || Main.Gamemode == GM.Mixed){
		if (p.getItemInHand() != null){// User is holding an item of interest maybe?
			if (p.getItemInHand().hasItemMeta()){// User is getting interesting :P
				if (p.getItemInHand().getItemMeta().hasDisplayName()){// This person wont quit teasing us!
					String iih = ChatColor.stripColor(p.getItemInHand().getItemMeta().getDisplayName());// Item In Hand | Abbreviation
					if (iih.equalsIgnoreCase(ChatColor.stripColor(MessageMW.ToolCompassName))){
						if (e.getAction() == Action.LEFT_CLICK_AIR || e.getAction() == Action.LEFT_CLICK_BLOCK){
							CTF.changeFlagTarget(p);
						}
						else if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK){
							CTF.getFlagDistance(p);
						}
					}
				}
			}
		}
	}
}

@SuppressWarnings("deprecation")
@EventHandler
public void EntityExplode (EntityExplodeEvent e){
	if (Healshot.contains(e.getEntity().getUniqueId())) {
		Healshot.remove(e.getEntity().getUniqueId());
			e.setCancelled(true);
	}
	if (Main.Gamemode == GM.CTF || Main.Gamemode == GM.Mixed){
	for (Block b : e.blockList()){
		if ((b.getData() == 11 || b.getData() == 14) && b.getTypeId() == 159)
		e.setCancelled(true);
	}
	}
	
}
public void onExplosionPrime(ExplosionPrimeEvent e) {
	e.setFire(false); //Only really needed for fireballs
	if (Healshot.contains(e.getEntity().getUniqueId())) {
		Healshot.remove(e.getEntity().getUniqueId());
			e.setCancelled(true);
			e.setRadius(0);
	}
}
@SuppressWarnings("deprecation")
@EventHandler
public void OnSlotChangeWhileFlag(final PlayerItemHeldEvent e){
	if (Main.Gamemode == GM.CTF || Main.Gamemode == GM.Mixed){
		if (e.getPlayer().getInventory().getItem(e.getPreviousSlot()) != null){
			if (e.getPlayer().getInventory().getItem(e.getPreviousSlot()).getTypeId() == 159 && (teamC.getBlue().getCapturer() == e.getPlayer() || teamC.getRed().getCapturer() == e.getPlayer())){
			e.setCancelled(true);
			if (!NotifiedPlayerSlot.contains(e.getPlayer().getName())){
			e.getPlayer().sendMessage(ChatColor.YELLOW + "You must drop the flag, to use other items. To drop flag press 'Q' (default).");
			NotifiedPlayerSlot.add(e.getPlayer().getName());
			plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				NotifiedPlayerSlot.remove(e.getPlayer().getName());
			}
			}, 20L * 3);
			}
			}
		}
	
	}
}
	@SuppressWarnings("deprecation")
	@EventHandler
	public void InventoryUse (InventoryClickEvent e){
		if (e.getSlot() == 39){
			e.setCancelled(true);
			}
		else if (e.getCurrentItem() != null && e.getCurrentItem().getTypeId() == 159 && (e.getCurrentItem().getDurability() == 14 || e.getCurrentItem().getDurability() == 11)){
			e.setCancelled(true);
		}
	
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void OnPlayerDropItem (PlayerDropItemEvent e){
		
        Item drop = e.getItemDrop();
        int item = drop.getItemStack().getTypeId();
        if (item != 159 && (item == 345 || (item >= 298 && item <= 301)))
        e.setCancelled(true);
        else if (drop.getItemStack().getTypeId() == 159){
        	Location loc = e.getPlayer().getLocation();
        	if (teamC.getBlue().getCapturer() == e.getPlayer()){
        		CTF.SpawnFlag(loc, Team.Blue);
        		CTF.FlagDespawnTimer(loc, Team.Blue);
        	}
        	else if (teamC.getRed().getCapturer() == e.getPlayer()){
				CTF.SpawnFlag(loc, Team.Red);
        		CTF.FlagDespawnTimer(loc, Team.Red);
        	}
        	e.getItemDrop().remove();
        }
	}
	
	@EventHandler
	public void OnPlayerShoot (ProjectileLaunchEvent e) throws SQLException{
		if (e.getEntity().getShooter() instanceof Player){
			Player p = (Player) e.getEntity().getShooter();
			if (p.getItemInHand().hasItemMeta() == true){
				if (p.getItemInHand().getItemMeta().hasDisplayName() && p.getItemInHand().getItemMeta().getDisplayName().contains("Maple Longbow")){
					Vector vel = e.getEntity().getVelocity().multiply(0.80);
					e.getEntity().setVelocity(vel);
					Longbow.add(e.getEntity().getUniqueId());
				}
				
				if (PowerUps.hasPowerUp(p, PowerUps.PoisonUpgrade1) || PowerUps.hasPowerUp(p, PowerUps.PoisonUpgrade2) || PowerUps.hasPowerUp(p, PowerUps.PoisonUpgrade3))
						PoisonousArrow.add(e.getEntity().getUniqueId());
				}
			}
		}
	
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void CrossBowEvent (PlayerInteractEvent e){
		final Player p = e.getPlayer();
		if (p.getItemInHand().hasItemMeta() && p.getItemInHand().getItemMeta().hasDisplayName()){
		if (p.getItemInHand().getItemMeta().getDisplayName().contains("Crossbow")){
			   Arrow arrow = p.launchProjectile(Arrow.class);
        	   Vector vel = p.getLocation().getDirection().normalize().multiply(4);
   			   arrow.setVelocity(vel);
   			   arrow.setShooter(p);
        	   Crossbow.add(arrow.getUniqueId());
        	    final ItemStack im = p.getItemInHand();
        	    p.setItemInHand(new ItemStack(Material.AIR));
				p.updateInventory();
        		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
        			public void run() {
        				p.setItemInHand(im);
        				p.updateInventory();
        			}
        			}, 20L * 2);
			}
		}
	}
	
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void OnMoveEvent (PlayerMoveEvent e) throws SQLException{
		Player p = e.getPlayer();
		if (Main.Gamemode == GM.CTF || Main.Gamemode == GM.Mixed){ 
			int ID = p.getItemInHand().getTypeId();
			short dura = p.getItemInHand().getDurability();
		
		
			if (teamC.getRed().getFlagLoc() != null && teamC.getBlue().getFlagLoc() != null){
				int newlocx = e.getTo().getBlockX();
				int newlocy = e.getTo().getBlockY();
				int newlocz = e.getTo().getBlockZ();
				
				int oldlocx = e.getFrom().getBlockX();
				int oldlocy = e.getFrom().getBlockY();
				int oldlocz = e.getFrom().getBlockZ();
				if (newlocx != oldlocx || newlocz != oldlocz || newlocy != oldlocy){
					
					if (ID == 159 && (dura == 14 || dura == 11) && (teamC.getBlue().getCapturer() == p || teamC.getRed().getCapturer() == p)){
						if (LocationsMW.locationBetweenPoints(p.getLocation(), LocationsMW.blueFlagClaimPt1, LocationsMW.blueFlagClaimPt2) && dura == 11 && teamC.getBlue().getCapturer() == p){
							p.setItemInHand(new ItemStack(Material.AIR));
							p.updateInventory();
							p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 0, 0), true);
							CTF.SpawnFlag(LocationsMW.blueFlag, Team.Blue);
						}
						else if (LocationsMW.locationBetweenPoints(p.getLocation(), LocationsMW.redFlagClaimPt1, LocationsMW.redFlagClaimPt2) && dura == 14 && teamC.getRed().getCapturer() == p){
							p.setItemInHand(new ItemStack(Material.AIR));
							p.updateInventory();
							p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 0, 0), true);
							CTF.SpawnFlag(LocationsMW.redFlag, Team.Red);
						}
						
						Games.CheckUpStatus();
		
						
						
					}
					if (teamC.getBlue().getCapturer() == p || teamC.getRed().getCapturer() == p){
						if (p == teamC.getBlue().getCapturer())
							teamC.getBlue().changeFlagLoc(p.getLocation());
						else if (p == teamC.getRed().getCapturer())
							teamC.getRed().changeFlagLoc(p.getLocation());
					}
					CTF.updateCompass(p);
				}
			}
		}
	}
}
