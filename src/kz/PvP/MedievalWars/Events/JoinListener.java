package kz.PvP.MedievalWars.Events;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import kz.PvP.MedievalWars.Games;
import kz.PvP.MedievalWars.Main;
import kz.PvP.MedievalWars.InfoSQL;
import kz.PvP.MedievalWars.Commands.PlayerCommands;
import kz.PvP.MedievalWars.Enums.Game;
import kz.PvP.MedievalWars.Enums.GM;
import kz.PvP.MedievalWars.Enums.Team;
import kz.PvP.MedievalWars.Gamemodes.CTF;
import kz.PvP.MedievalWars.Gamemodes.Global;
import kz.PvP.MedievalWars.Methods.GameManagement;
import kz.PvP.MedievalWars.Methods.ProtocolLib;
import kz.PvP.MedievalWars.Methods.teamC;
import kz.PvP.MedievalWars.Methods.teamM;
import kz.PvP.MedievalWars.utilities.MessageMW;
import kz.PvP.PkzAPI.utilities.Message;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.plugin.java.JavaPlugin;




public class JoinListener implements Listener{
	public static Main plugin;
	public static HashMap<String, Integer> TimedGaming = new HashMap<String, Integer>();
	
	
	public JoinListener(Main mainclass) {
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
		}
	@EventHandler
	public void OnServerJoin (PlayerJoinEvent e) throws SQLException{
		final Player p = e.getPlayer();
		
		if (Main.TabAPIEnable){
			ProtocolLib.TabsetPriority(p, 2);
			ProtocolLib.updateTabAll();
		}
		
		Global.clearInventory(p, true);		
		/*
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
			public void run(){
				try {
					PlayerCommands.OpenTeamMenu(p);
				} catch (SQLException e) {e.printStackTrace();}
			}
		}, 20);
		*/
		
		
    	plugin.getServer().getScheduler().runTaskTimerAsynchronously(plugin, new Runnable() {
    	    @Override  
    	    public void run() {
    	    	if (!TimedGaming.containsKey(p.getName()))
    	    		TimedGaming.put(p.getName(), 1);
    	    	else
    	    		TimedGaming.put(p.getName(), TimedGaming.get(p.getName()) + 1);
    	    }
    	}, 20L * 60, 20L * 60);
    	
    	
		teamM.assignTeam(p.getName(), Team.Random);
		
		
		//Message.P(p, ChatColor.GOLD + "" + ChatColor.ITALIC + "To join game, type /Team", true);
		
		
		ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
		BookMeta meta = (BookMeta) book.getItemMeta();
		meta.setTitle(ChatColor.GRAY + "" + ChatColor.BOLD + "MW Tutorial");
		meta.setAuthor(ChatColor.DARK_PURPLE + "MedievalWars");
		List<String> pages = new ArrayList<String>();
		pages.add(ChatColor.DARK_GREEN + "Welcome to Medievalwars!\n\n"  + 
		ChatColor.GRAY + "" + ChatColor.UNDERLINE + "Table of Contents\n" +
			ChatColor.RESET + ChatColor.DARK_BLUE +
			
			
				"2 - How to play?\n" +
				"3 - Notable Commands\n" +
				"4 - Classes\n" + 
				"5 - Upgrading\n" +
				"6 - Rewards\n" +
				"7 - Tools\n");
		
		pages.add(ChatColor.RED + "How to play?" + ChatColor.RESET + ChatColor.BLACK +
				"\nMW is a mixture of multiple minigames, but with a twist!\n\n" +
				" Your goal can be determined on the gamemode for that round. You may check round information by typing /help. Each gamemode, has a different goal for the team.");
		pages.add(ChatColor.RED + "= Commands = \n\n" + ChatColor.DARK_GRAY +
				"/Class" + ChatColor.GRAY + " - Choose class\n" + ChatColor.DARK_GRAY +
				"/Upgrade" + ChatColor.GRAY + " - Upgrade class\n" + ChatColor.DARK_GRAY +
				"/Activate" + ChatColor.GRAY + " - Activate upgrade\n" + ChatColor.DARK_GRAY +
				"/Vote" + ChatColor.GRAY + " - Vote for Gamemode\n" + ChatColor.DARK_GRAY +
				"/Leave" + ChatColor.GRAY + " - Spectate\n" + ChatColor.DARK_GRAY +
				"/Help" + ChatColor.GRAY + " - Round Info\n" + ChatColor.DARK_GRAY +
				"/MW" + ChatColor.GRAY + " - Commands Index\n");

		
		pages.add(ChatColor.RED + "= Classes = \n\n" + ChatColor.BLACK + "Type /class to choose a class.\n\n" + "Each class has its own advantages, and disadvantages. Some classes are melee or ranged. They can also be aggressive or defensive. \n\n" + ChatColor.RED + "So pick wisely!");
		
		pages.add(ChatColor.RED + "Upgrading\n\n" + ChatColor.BLACK + "Upgrading is a huge aspect in MedievlWars." +
				"Each class has its own set of upgrades which enhance your gameplayer. Points are used to purchase upgrades. To open upgrade menu, type /upgrade");
		pages.add(ChatColor.RED + "" + ChatColor.BOLD + "Rewards" + ChatColor.RESET + ChatColor.BLACK + "\n\n Player Kill:\n" + ChatColor.DARK_BLUE + DeathListener.RewardKill + " point(s)" + ChatColor.BLACK + ".\n\n Flag capture\n " + ChatColor.DARK_GREEN + GameManagement.CTFPointsPerCap + " point(s).");
		
		meta.setPages(pages);
		book.setItemMeta(meta);
		p.getInventory().addItem(book);
		
		
	}
	
	static public void JoinGame(Player p){
		if (Main.TabAPIEnable)
		ProtocolLib.TabRefresh(p);
		
		if (Main.GameStatus != Game.PREGAME){
			Message.P(p, MessageMW.LeaveTheGame, true);
			try {
				teamM.Start(p, true);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	@EventHandler
	public void OnKick (PlayerKickEvent e) throws SQLException{
		e.setLeaveMessage(null);
		Player p = e.getPlayer();
		QuitServer(p);
	}
	
	private void QuitServer(Player p) throws SQLException {
		if (TimedGaming.containsKey(p.getName())){
			//PreparedStatement ps = InfoSQL.PS("UPDATE UserInfo SET MWPlayTime = '" + (InfoSQL.getMwTime(p.getName()) + TimedGaming.get(p.getName())) + "' WHERE User = '" + InfoSQL.GetUserID(p.getName()) + "'");
			//ps.executeUpdate();
			TimedGaming.remove(p.getName());
		}
		
		
		if (Main.GameStatus == Game.GAME && teamM.isInTeam(p.getName()))
			Message.P(p, "", false);
		
		
		
		Global.DeleteFromArrays(p, 'F');
		if (Main.TabAPIEnable)
		ProtocolLib.TabsetPriority(p, -2);
		
		
		Location loc = p.getLocation();
		if (Main.GameStatus == Game.GAME){
			if (teamC.getBlue().getCapturer() == p){
				CTF.SpawnFlag(loc, Team.Blue);
				CTF.FlagDespawnTimer(loc, Team.Blue);
			}
			else if (teamC.getRed().getCapturer() == p){
				CTF.SpawnFlag(loc, Team.Red);
				CTF.FlagDespawnTimer(loc, Team.Red);
			}
		}
		ProtocolLib.updateTabAll();
		Games.CheckUpStatus();
		
	}
	@EventHandler
	public void OnServerQuit (PlayerQuitEvent e) throws SQLException{
		e.setQuitMessage(null);
		Player p = e.getPlayer();
		QuitServer(p);
	}
	
	
}
