package kz.PvP.MedievalWars.Events;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.UUID;

import kz.PvP.MedievalWars.Games;
import kz.PvP.MedievalWars.Main;
import kz.PvP.MedievalWars.InfoSQL;
import kz.PvP.MedievalWars.Vote;
import kz.PvP.MedievalWars.Enums.Game;
import kz.PvP.MedievalWars.Enums.GM;
import kz.PvP.MedievalWars.Enums.KitType;
import kz.PvP.MedievalWars.Enums.Team;
import kz.PvP.MedievalWars.Gamemodes.CTF;
import kz.PvP.MedievalWars.Gamemodes.Global;
import kz.PvP.MedievalWars.Methods.ChangeMaps;
import kz.PvP.MedievalWars.Methods.ConvertTimings;
import kz.PvP.MedievalWars.Methods.GameManagement;
import kz.PvP.MedievalWars.Methods.LocationsMW;
import kz.PvP.MedievalWars.Methods.Scoreboards;
import kz.PvP.MedievalWars.Methods.Stats;
import kz.PvP.MedievalWars.Methods.teamC;
import kz.PvP.MedievalWars.Methods.teamM;
import kz.PvP.MedievalWars.Timers.EndGameTimer;
import kz.PvP.MedievalWars.Timers.PreGameTimer;
import kz.PvP.MedievalWars.utilities.Files;
import kz.PvP.MedievalWars.utilities.Kits;
import kz.PvP.MedievalWars.utilities.MessageMW;
import kz.PvP.PkzAPI.utilities.Message;
import kz.PvP.PkzAPI.utilities.Mysql;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;


public class DeathListener implements Listener{
	public static Main plugin;
	
	
	public DeathListener(Main mainclass) {
		plugin = mainclass;
		
		for (String msgs : Files.deathconf.getCustomConfig().getStringList("PvP"))
			PvPDeathMessages.add(msgs);
		for (String msgs : Files.deathconf.getCustomConfig().getStringList("PvP-Bow"))
			BowDeathMessages.add(msgs);
		for (String msgs : Files.deathconf.getCustomConfig().getStringList("Fall"))
			FallDeathMessages.add(msgs);
		for (String msgs : Files.deathconf.getCustomConfig().getStringList("Explode"))
			ExplodeDeathMessages.add(msgs);
		for (String msgs : Files.deathconf.getCustomConfig().getStringList("Drown"))
			DrownDeathMessages.add(msgs);
		for (String msgs : Files.deathconf.getCustomConfig().getStringList("Poison"))
			PoisonDeathMessages.add(msgs);
		for (String msgs : Files.deathconf.getCustomConfig().getStringList("Fire"))
			FireDeathMessages.add(msgs);
		for (String msgs : Files.deathconf.getCustomConfig().getStringList("Suicide"))
			SuicideDeathMessages.add(msgs);
		for (String msgs : Files.deathconf.getCustomConfig().getStringList("FallBlock"))
			FallblockDeathMessages.add(msgs);
		for (String msgs : Files.deathconf.getCustomConfig().getStringList("Lightning"))
			LightningDeathMessages.add(msgs);
		
		
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
		}
	public static int RewardKill = 1;
	public static int RespawnTimer = 5;
	public static HashMap<String, Integer> x = new HashMap<String, Integer>();
	public static HashMap<String, Integer> PrevKills = new HashMap<String, Integer>();
	public static HashMap<String, Integer> PrevDeaths = new HashMap<String, Integer>();
	public static HashMap<String, BukkitTask> BT = new HashMap<String, BukkitTask>();
	
	public static ArrayList<String> PvPDeathMessages = new ArrayList<String>();
	public static ArrayList<String> BowDeathMessages = new ArrayList<String>();
	public static ArrayList<String> FallDeathMessages = new ArrayList<String>();
	public static ArrayList<String> ExplodeDeathMessages = new ArrayList<String>();
	public static ArrayList<String> DrownDeathMessages = new ArrayList<String>();
	public static ArrayList<String> PoisonDeathMessages = new ArrayList<String>();
	public static ArrayList<String> FireDeathMessages = new ArrayList<String>();
	public static ArrayList<String> SuicideDeathMessages = new ArrayList<String>();
	public static ArrayList<String> FallblockDeathMessages = new ArrayList<String>();
	public static ArrayList<String> LightningDeathMessages = new ArrayList<String>();
	
	
	
	
	
	
	
	
    int task = 0;
	
	int respawntimer;
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onKill (PlayerDeathEvent e) throws SQLException{
		if (e.getEntity() instanceof Player){
			final Player p = e.getEntity();
			Location loc = new Location(p.getWorld(), p.getLocation().getX(), p.getLocation().getY(),
			p.getLocation().getZ());
			
			
			Location bloodloc = p.getLocation().add(0, 1, 0);
			bloodloc.getWorld().playEffect(bloodloc, Effect.STEP_SOUND, Material.REDSTONE_WIRE);
			bloodloc.getWorld().playEffect(bloodloc, Effect.STEP_SOUND, Material.REDSTONE_WIRE);
			bloodloc.getWorld().playEffect(bloodloc, Effect.STEP_SOUND, Material.REDSTONE_WIRE);
			bloodloc.getWorld().playEffect(bloodloc, Effect.STEP_SOUND, Material.REDSTONE_WIRE);
			bloodloc.getWorld().playEffect(bloodloc, Effect.STEP_SOUND, Material.REDSTONE_WIRE);
			bloodloc.getWorld().playEffect(bloodloc, Effect.STEP_SOUND, Material.REDSTONE_WIRE);
			bloodloc.getWorld().playEffect(bloodloc, Effect.STEP_SOUND, Material.REDSTONE_WIRE);
			bloodloc.getWorld().playEffect(bloodloc, Effect.STEP_SOUND, Material.REDSTONE_WIRE);
			
			if (teamC.getBlue().getCapturer() == p){
				CTF.SpawnFlag(loc, Team.Blue);
				CTF.FlagDespawnTimer(loc, Team.Blue);
			}
			else if (teamC.getRed().getCapturer() == p){
				CTF.SpawnFlag(loc, Team.Red);
				CTF.FlagDespawnTimer(loc, Team.Red);
			}
			
			for (ItemStack im : e.getDrops()){
				int item = im.getTypeId();
		        if (item == 261 || item == 262 || item == 283 || item == 268 || item == 267 || item == 272 || item == 276 || item == 329 ||item == 345 || item ==  388 || item == 398 || item == 288 || item == 159 || (item >= 298 && item <= 317) || (item == 35 && (im.getDurability() == (short)14 || im.getDurability() == (short)11)))
		        	im.setTypeId(0);
		        else if ((item == 399 || item == 370) && im.getAmount() >= 5){
		        	im.setAmount(5);
		        }
			}
			e.getDrops().clear();
		    for (PotionEffect effect : p.getActivePotionEffects())
		        p.removePotionEffect(effect.getType());
			
			if (p.getKiller() instanceof Player){
			Mysql.PS.getSecureQuery("INSERT INTO DEATHS (Victim, Killer) VALUES (?,?)", ""+Mysql.GetUserID(p.getName()), ""+Mysql.GetUserID(p.getKiller().getName()));
			InfoSQL.modifyUserPts(p.getKiller().getName(), RewardKill, true);
			
			if (teamM.isInTeam(p.getName())){
								
				
				if (teamC.getRed().getTotalKills() == 0 && teamC.getBlue().getTotalKills() == 0){
					Message.G(Message.Replacer(MessageMW.FirstBlood, p.getKiller().getName(), "%killer"), true);					
					teamM.playSoundTeam(Team.Red, Sound.BLAZE_DEATH);
					teamM.playSoundTeam(Team.Blue, Sound.BLAZE_DEATH);
				}
				
				
				Stats.earnedAKill(p);

				
				
				
				if (PrevKills.containsKey(p.getName()))
					PrevKills.remove(p.getName());
				
				
				if (PrevDeaths.containsKey(p.getKiller().getName()))
					PrevDeaths.remove(p.getKiller().getName());
				
				if (!PrevDeaths.containsKey(p.getName()))
				PrevDeaths.put(p.getName(), 0);
				
				PrevDeaths.put(p.getName(), PrevDeaths.get(p.getName()) + 1);

				
				if (!PrevKills.containsKey(p.getKiller().getName()))
				PrevKills.put(p.getKiller().getName(), 0);
				
				PrevKills.put(p.getKiller().getName(), PrevKills.get(p.getKiller().getName()) + 1);
				
				
				if (PrevKills.get(p.getKiller().getName()) % 4 == 0){
					Message.G(p.getKiller().getDisplayName() + ChatColor.GOLD + " is on a " + PrevKills.get(p.getKiller().getName()) + " killstreak!", false);
					Message.P(p.getKiller(), "You have been rewarded with strength for " + PrevKills.get(p.getKiller().getName()) * 5 + " seconds!", true);
					//Put potion effect
    				p.getKiller().addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 20 * 5 * PrevKills.get(p.getKiller().getName()), 0));
				}
				
				
				
				if (Main.Gamemode == GM.Mixed){
					Games.CheckUpStatus();
				}
				else if (Main.Gamemode == GM.TDM){
					Games.CheckUpStatus();
				}
				else if (Main.Gamemode == GM.Elimination){
					teamM.assignTeam(p.getName(), Team.Spectator);
					Games.CheckUpStatus();
				}
				
				
				}
			}
			
			
			if (plugin.getServer().getOnlinePlayers().length <= 25)
				e.setDeathMessage(RandomlyGenerateFunnyQuote(p));
			
			
			
			
			
			
			
			
			
			
			if (p.isDead() == true){
				if (teamM.isInTeam(p.getName())){
					p.setHealth(20.0);
					if (p.getVehicle() != null)
					p.getVehicle().eject();
			        for (ItemStack i : p.getInventory().getContents())
			        {
			        	if (i != null){
			        	int item = i.getTypeId();	
			        	
			        	if (Kits.hasItemAbility(p, KitType.ITEM, i, null))
			        	i.setAmount(0);
			        	
				        if ((item == 159 && (i.getDurability() == (short)14 || i.getDurability() == (short)11))){i.setAmount(0);}
				        else{
				        if (i == null || i.getType() == Material.AIR || i.getTypeId() == 0){
				        	
				        }
				        else{
			            if (item == 399 || item == 370){
			            	double r = Math.random();
			            	if (r <= 0.05)
			            	i.setAmount(i.getAmount());
			            	else if (r <= 0.10)
				            i.setAmount((int) ((int)i.getAmount() * 0.75));
			            	else if (r <= 0.25)
			            	i.setAmount((int) ((int)i.getAmount() * 0.5));
			            	else if (r <= 0.5)
				            i.setAmount((int) ((int)i.getAmount() * 0.40));
			            	else if (r <= 0.75)
				            i.setAmount((int) ((int)i.getAmount() * 0.30));
			            	else
				            i.setAmount((int) ((int)i.getAmount() * 0.2));
			            }
			            if (i.getAmount() >= 1)
				        p.getWorld().dropItemNaturally(p.getLocation(), i);
				        }
			            p.getInventory().remove(i);
				        }
				        }
			        }
			        p.getInventory().clear();
			        p.closeInventory();
			        if (Main.Gamemode == GM.Elimination){
						
						teamM.assignTeam(p.getName(), Team.Spectator);
						Message.P(p, MessageMW.YouJustgotEliminated, false);
						if (teamM.getTeam(Team.Blue).size() >= 1 && teamM.getTeam(Team.Red).size() == 0)
							GameManagement.endGame(Team.Blue);
						else if (teamM.getTeam(Team.Red).size() >= 1 && teamM.getTeam(Team.Blue).size() == 0)
							GameManagement.endGame(Team.Red);
					}
			        else{
			        if (BT.containsKey(p.getName())){
	    			BT.get(p.getName()).cancel();
    	    		BT.remove(p.getName());
			        }
			    	x.put(p.getName(), RespawnTimer);
			    	
			    	
			    	
					p.teleport(LocationsMW.DeathSpot);
					for (PotionEffect pe : p.getActivePotionEffects())
					p.removePotionEffect(pe.getType());
					p.sendMessage(ChatColor.GRAY + "Respawning in " + RespawnTimer + " seconds.");
					p.setVelocity(new Vector());
					BukkitTask task = new kz.PvP.MedievalWars.RepeatingTasks.RespawnTimer(p).runTaskTimerAsynchronously(plugin, 20L, 20L);
			    	BT.put(p.getName(), task);
			        }
					
					
				}
				else{
					Global.GiveTools(p, Team.Spectator);
				}
			}
		}
	}
	
	private String RandomlyGenerateFunnyQuote(Player p) {
		String phrase = "";
		String defwep = Message.CleanCapitalize(p.getItemInHand().getType().toString());
		Player dam = p.getKiller();
		if (dam != null){
			String damwep = Message.CleanCapitalize(dam.getItemInHand().getType().toString());
			if (damwep == "bow")
			phrase = BowDeathMessages.get(ConvertTimings.randomInt(0, BowDeathMessages.size() - 1));
			else
			phrase = PvPDeathMessages.get(ConvertTimings.randomInt(0, PvPDeathMessages.size() - 1));
			
	
			phrase = Message.Replacer(phrase, damwep, "<damwep>");
			phrase = Message.Replacer(phrase, dam.getName(), "<dam>");
		}
		else if (p.getLastDamageCause().getCause() == DamageCause.FALL)
			phrase = FallDeathMessages.get(ConvertTimings.randomInt(0, FallDeathMessages.size() - 1));
		else if (p.getLastDamageCause().getCause() == DamageCause.BLOCK_EXPLOSION)
			phrase = ExplodeDeathMessages.get(ConvertTimings.randomInt(0, ExplodeDeathMessages.size() - 1));
		else if (p.getLastDamageCause().getCause() == DamageCause.DROWNING)
			phrase = DrownDeathMessages.get(ConvertTimings.randomInt(0, DrownDeathMessages.size() - 1));
		else if (p.getLastDamageCause().getCause() == DamageCause.FIRE || p.getLastDamageCause().getCause() == DamageCause.LAVA || p.getLastDamageCause().getCause() == DamageCause.FIRE_TICK)
			phrase = FireDeathMessages.get(ConvertTimings.randomInt(0, FireDeathMessages.size() - 1));
		else if (p.getLastDamageCause().getCause() == DamageCause.LIGHTNING)
			phrase = LightningDeathMessages.get(ConvertTimings.randomInt(0, LightningDeathMessages.size() - 1));
		else if (p.getLastDamageCause().getCause() == DamageCause.SUICIDE)
			phrase = SuicideDeathMessages.get(ConvertTimings.randomInt(0, SuicideDeathMessages.size() - 1));
		else if (p.getLastDamageCause().getCause() == DamageCause.POISON)
			phrase = PoisonDeathMessages.get(ConvertTimings.randomInt(0, PoisonDeathMessages.size() - 1));
		else if (p.getLastDamageCause().getCause() == DamageCause.FALLING_BLOCK)
			phrase = FallblockDeathMessages.get(ConvertTimings.randomInt(0, FallblockDeathMessages.size() - 1));
		
		
		
		phrase = Message.Replacer(phrase, defwep, "<defwep>");
		phrase = Message.Replacer(phrase, p.getName(), "<def>");
		
		return Message.Colorize(phrase);
	}


	@EventHandler
	public void onEntityDeath(final EntityDeathEvent e) {

	    
	    if (e.getEntity() instanceof Horse){
			final UUID ID = e.getEntity().getUniqueId();
			if (teamC.getBlue().getHorses().containsKey(ID)){
				// Blue Horse
				plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                    public void run() {
        				Global.SpawnBlueHorse(teamC.getBlue().getHorses().get(ID));
                    	teamC.getBlue().getHorses().remove(ID);
                    }
                }, 20L * 10);
			}
			else if (teamC.getRed().getHorses().containsKey(ID)){
				// Red Horse
				plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
                    	Global.SpawnRedHorse(teamC.getRed().getHorses().get(ID));
                    	teamC.getRed().getHorses().remove(ID);
                    	}
					}, 20L * 10);
				}
			
			e.getDrops().clear();
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
