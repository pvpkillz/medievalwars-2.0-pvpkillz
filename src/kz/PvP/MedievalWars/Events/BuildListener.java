package kz.PvP.MedievalWars.Events;

import java.sql.SQLException;

import kz.PvP.MedievalWars.Main;
import kz.PvP.MedievalWars.Enums.GM;
import kz.PvP.MedievalWars.Enums.Team;
import kz.PvP.MedievalWars.Gamemodes.CTF;
import kz.PvP.MedievalWars.Methods.Cannons;
import kz.PvP.MedievalWars.Methods.CoreM;
import kz.PvP.MedievalWars.Methods.GameManagement;
import kz.PvP.MedievalWars.Methods.LocationsMW;
import kz.PvP.MedievalWars.Methods.PowerUps;
import kz.PvP.MedievalWars.Methods.teamC;
import kz.PvP.MedievalWars.utilities.MessageMW;
import kz.PvP.PkzAPI.methods.Locations;
import kz.PvP.PkzAPI.utilities.Message;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


public class BuildListener implements Listener{
	public static Main plugin;

	
	public BuildListener(Main mainclass) {
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
		}
	@SuppressWarnings("deprecation")
	@EventHandler
	public void PlacingFlag (BlockPlaceEvent e) throws SQLException{
		Player p = e.getPlayer();
		Block b = e.getBlock();
		
		
		if (!e.isCancelled()){
			if (Main.Gamemode == GM.CTF || Main.Gamemode == GM.Mixed){
			
				if (e.getBlock().getTypeId() == 159){
					Location blockloc = e.getBlock().getLocation();
					
					
					if (teamC.getBlue().getCapturer() == p || teamC.getRed().getCapturer() == p){
				        	if (teamC.getBlue().getCapturer() == p){
				        		CTF.FlagDespawnTimer(blockloc, Team.Blue);
				        	}
				        	else if (teamC.getRed().getCapturer() == p){
				        		CTF.FlagDespawnTimer(blockloc, Team.Red);
				        	}
				        	e.getBlock().setData((byte) e.getBlock().getData());
				    }
					
					e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 0, 1), true);
		        }
			}
			 if (e.getBlock().getType() == Material.TNT){
					if (PowerUps.hasPowerUp(p, 20000) || PowerUps.hasPowerUp(p, 20001) || PowerUps.hasPowerUp(p, 20002) || PowerUps.hasPowerUp(p, 20003) || PowerUps.hasPowerUp(p, 20004)){
						if (b.getType() == Material.TNT){
							b.setType(Material.AIR);
							TNTPrimed tnt = (TNTPrimed) b.getWorld().spawn(b.getLocation(), TNTPrimed.class);
							//tnt.setIsIncendiary(true);
							
							if (PowerUps.hasPowerUp(p, 20003))
							tnt.setYield(5.5f);
							else if (PowerUps.hasPowerUp(p, 20002))
							tnt.setYield(5.0f);
							else if (PowerUps.hasPowerUp(p, 20001))
							tnt.setYield(4.5f);
							else if (PowerUps.hasPowerUp(p, 20000))
							tnt.setYield(4f);
							
							if (PowerUps.hasPowerUp(p, 20004))
							tnt.setIsIncendiary(true);
						}
					}
			}
			 
				if (b != null){
					Cannons.IsACannon(p, e.getBlock());
				}
			
			
			
		}
	}
	@SuppressWarnings("deprecation")
	@EventHandler
	public void DestroyingFlag (BlockBreakEvent e){
		int id = e.getBlock().getTypeId();
		Player p = e.getPlayer();
		Block b = e.getBlock();
		if (Main.Gamemode == GM.CTF || Main.Gamemode == GM.Mixed){
			if (id == 159 && (e.getBlock().getData() == 11 || e.getBlock().getData() == 14))
				e.setCancelled(true);
		}
		else if (Main.Gamemode == GM.DTC){
		if (Locations.locationBetweenPoints(b.getLocation(), teamC.getBlue().getCoreLoc1(), teamC.getBlue().getCoreLoc2())){
			if (e.getBlock().getType() == Material.STAINED_GLASS){
				if (b.getData() == 11){
					if (teamC.getBlue().getCoreHP() > 0){
						if (teamC.getBlue().getCoreHP() <= GameManagement.CoreHPDmg)
							teamC.getBlue().modifyCoreHP(-teamC.getBlue().getCoreHP());
						else
							teamC.getBlue().modifyCoreHP(-GameManagement.CoreHPDmg);
						Message.P(p, Message.Replacer(Message.Replacer(MessageMW.CoreHealthDropped, ""+teamC.getBlue().getCoreHP(), "%hp"), ""+GameManagement.CoreHPDmg, "%dmg"), true);
						e.setCancelled(true);
						CoreM.checkupScores();
					}
					else{
						teamC.getBlue().theCoreHP = 0;
						Message.P(p, Message.Replacer(Message.Replacer(MessageMW.CoreHealthDropped, ""+teamC.getBlue().getCoreHP(), "%hp"), ""+GameManagement.CoreHPDmg, "%dmg"), true);
						e.setCancelled(true);
					}
				}
			}
		}
		else if (Locations.locationBetweenPoints(b.getLocation(), teamC.getRed().getCoreLoc1(), teamC.getRed().getCoreLoc2())){
			if (e.getBlock().getType() == Material.STAINED_GLASS){
				if (b.getData() == 14){
					if (teamC.getRed().getCoreHP() > 0){
						if (teamC.getRed().getCoreHP() <= GameManagement.CoreHPDmg)
							teamC.getRed().modifyCoreHP(-teamC.getRed().getCoreHP());
						else
							teamC.getRed().modifyCoreHP(-GameManagement.CoreHPDmg);						Message.P(p, Message.Replacer(Message.Replacer(MessageMW.CoreHealthDropped, ""+teamC.getRed().getCoreHP(), "%hp"), ""+GameManagement.CoreHPDmg, "%dmg"), true);
						e.setCancelled(true);
						CoreM.checkupScores();
					}
					else{
						teamC.getRed().theCoreHP = 0;
						Message.P(p, Message.Replacer(Message.Replacer(MessageMW.CoreHealthDropped, ""+teamC.getRed().getCoreHP(), "%hp"), ""+GameManagement.CoreHPDmg, "%dmg"), true);
						e.setCancelled(true);
					}
				}
				
			}
		}
		}
	}
}
