package kz.PvP.MedievalWars.Events;

import java.sql.SQLException;

import kz.PvP.MedievalWars.Main;
import kz.PvP.MedievalWars.Commands.PlayerCommands;
import kz.PvP.MedievalWars.Enums.Game;
import kz.PvP.MedievalWars.Enums.KitType;
import kz.PvP.MedievalWars.Methods.teamM;
import kz.PvP.MedievalWars.utilities.Kits;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.spigotmc.event.entity.EntityMountEvent;


public class Spectator implements Listener{
	public static Main plugin;

	
	
	public Spectator(Main mainclass) {
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
		}
	
	@EventHandler
	public void PlayerQuit (PlayerQuitEvent e){
		if (teamM.isInSpec(e.getPlayer().getName())){
			e.setQuitMessage(null);
		}
	}
	@EventHandler
	public void InventoryUse (InventoryDragEvent e){
		if (e.getWhoClicked() instanceof Player){
		if (teamM.isInSpec(e.getWhoClicked().getName())){
			e.setCancelled(true);
			}
		}
	}
	@EventHandler
	public void InventoryUse (PlayerDropItemEvent e){
		if (teamM.isInSpec(e.getPlayer().getName())){
			e.setCancelled(true);
		}
		else if (Kits.hasItemAbility(e.getPlayer(), KitType.ITEM, e.getItemDrop().getItemStack(), null)){
			e.setCancelled(true);
			e.getPlayer().sendMessage(ChatColor.RED + "You cannot drop this item, as it is part of the class.");
		}
	}
	@EventHandler
	public void InventoryPickUp (PlayerPickupItemEvent e){
		if (teamM.isInSpec(e.getPlayer().getName())){
			e.setCancelled(true);
		}
	}
	@EventHandler
	public void BlockBreak (BlockBreakEvent e){
		if (teamM.isInSpec(e.getPlayer().getName()))
			e.setCancelled(true);
		if (Main.GameStatus == Game.PREGAME)
			e.setCancelled(true);
	}
	@EventHandler
	public void BlockPlace (BlockPlaceEvent e){
		if (teamM.isInSpec(e.getPlayer().getName()))
			e.setCancelled(true);
		if (Main.GameStatus == Game.PREGAME)
			e.setCancelled(true);
	}
	@EventHandler
	public void BlockDeath (PlayerDeathEvent e){
		if (e.getEntity() instanceof Player){
			Player p = (Player) e.getEntity();
			if (teamM.isInSpec(p.getName())){
				e.setDeathMessage(null);
			}
		}
	}
	@EventHandler
	public void BlockFishing (PlayerFishEvent e){
		if (teamM.isInSpec(e.getPlayer().getName()))
			e.setCancelled(true);
		if (Main.GameStatus == Game.PREGAME)
			e.setCancelled(true);
	}
	@EventHandler
	public void BlockBedEntry (PlayerBedEnterEvent e){
		if (teamM.isInSpec(e.getPlayer().getName()))
			e.setCancelled(true);
		if (Main.GameStatus == Game.PREGAME)
			e.setCancelled(true);
	}
	@EventHandler
	public void BlockMount (EntityMountEvent e){
		if (e.getEntity() instanceof Player){
			Player p = (Player) e.getEntity();
			if (teamM.isInSpec(p.getName()))
				e.setCancelled(true);
			if (Main.GameStatus == Game.PREGAME)
				e.setCancelled(true);
		}
	}
	@EventHandler
	public void BlockAttackingAnyDmg (EntityDamageEvent e){
		if (e.getEntity() instanceof Player){
			Player p = (Player) e.getEntity();
		if (teamM.isInSpec(p.getName())){
			e.setCancelled(true);
			}
		if (p.getKiller() != null)
		if (teamM.isInSpec(p.getKiller().getName())){
			e.setCancelled(true);
			}
		if (Main.GameStatus == Game.PREGAME)
			e.setCancelled(true);
		}
	}
	@EventHandler
	public void BlockAttackingEntDmg (EntityDamageByEntityEvent e){
		if (e.getEntity() instanceof Player){
			Player p = (Player) e.getEntity();
		if (teamM.isInSpec(p.getName())){
			e.setCancelled(true);
			}
		if (Main.GameStatus == Game.PREGAME)
			e.setCancelled(true);
		
		
		
		
		}
		else if (e.getDamager() instanceof Player){
			Player p = (Player) e.getDamager();
			if (teamM.isInSpec(p.getName()))
				e.setCancelled(true);
			if (Main.GameStatus == Game.PREGAME)
				e.setCancelled(true);
		
			if (e.getEntity().getPassenger() != null){
				if (e.getEntity().getPassenger() instanceof Player){
					Player pl = (Player) e.getEntity().getPassenger();
					if (teamM.isInRed(p.getName()) && teamM.isInRed(pl.getName()))
						e.setCancelled(true);
					if (teamM.isInBlue(p.getName()) && teamM.isInBlue(pl.getName()))
						e.setCancelled(true);
				}
			  }
	   }
	}
	
	
	@EventHandler
	public void Block (EntityShootBowEvent e){
		if (e.getEntity() instanceof Player){
			Player p = (Player) e.getEntity();
			if (teamM.isInSpec(p.getName())){
				e.setCancelled(true);
			}
		}
		if (Main.GameStatus == Game.PREGAME)
			e.setCancelled(true);
	}
	
	@EventHandler
	public void OnPlayerShoot (ProjectileLaunchEvent e){
		if (e.getEntity() instanceof Player){
			Player p = (Player) e.getEntity();
		if (teamM.isInSpec(p.getName())){
			e.setCancelled(true);
			}
		}
		if (Main.GameStatus == Game.PREGAME)
			e.setCancelled(true);
	}
	
	@EventHandler
	public void OnItemInteractEvent (PlayerInteractEvent e) throws SQLException{
		if (e.getPlayer().getItemInHand().hasItemMeta() == true && e.getPlayer().getItemInHand().getItemMeta().hasDisplayName() == true){
		String Item = e.getPlayer().getItemInHand().getItemMeta().getDisplayName().toLowerCase();
		if (Item.contains("class"))
			PlayerCommands.OpenClassMenu(e.getPlayer());
		else if (Item.contains("upgrade"))
			PlayerCommands.UpgradeMenuWithoutClass(e.getPlayer());
		else if (Item.contains("activator"))
			PlayerCommands.ActivateMenuWithoutClass(e.getPlayer());
		else if (Item.contains("spectate compass"))
			PlayerCommands.OpenPlayersMenu(e.getPlayer());
		}
		else if (teamM.isInSpec(e.getPlayer().getName())){
			e.setCancelled(true);
		}
	}
	@EventHandler
	public void OnUseChest (InventoryClickEvent e){
		if (e.getWhoClicked() instanceof Player){
			Player p = (Player) e.getWhoClicked();
			if (e.getClickedInventory() != null && p.getInventory().getType() != e.getClickedInventory().getType()){
				if (Kits.hasItemAbility(p, KitType.ITEM, e.getCurrentItem(), null)){
					e.setCancelled(true);
					p.sendMessage(ChatColor.RED + "You are unable to move this item, as it is part of the class.");
				}
			}
		}
	}
	
	@EventHandler
	public void onChunkLoad(ChunkLoadEvent e){
	    if (e.isNewChunk())
	        e.getChunk().unload(false, true);
	}
	
	
	
}
