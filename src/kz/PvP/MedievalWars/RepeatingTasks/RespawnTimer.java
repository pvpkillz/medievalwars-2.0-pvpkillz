package kz.PvP.MedievalWars.RepeatingTasks;

import java.sql.SQLException;

import kz.PvP.MedievalWars.Events.DeathListener;
import kz.PvP.MedievalWars.Methods.teamM;

import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;

 
public class RespawnTimer extends BukkitRunnable {
	Player p;
    public RespawnTimer(Player pl) {
        p = pl;
    }

	@Override
	public void run() {
        	
    	if (DeathListener.x.containsKey(p.getName()) && DeathListener.x.get(p.getName()) >= 1){
        	for (PotionEffect pe : p.getActivePotionEffects())
        		p.removePotionEffect(pe.getType());
        	
    		p.setLevel(DeathListener.x.get(p.getName()));
    		p.setFireTicks(0);
    		DeathListener.x.put(p.getName(), DeathListener.x.get(p.getName()) - 1);
    		p.getInventory().clear();
    	}
    	else {
    	if (!DeathListener.x.containsKey(p.getName())){
    		try {
				teamM.Start(p, true);
    			DeathListener.BT.get(p.getName()).cancel();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	else if (DeathListener.x.get(p.getName()) <= 0){
			DeathListener.x.remove(p.getName());
    		try {
				teamM.Start(p, true);
    			DeathListener.BT.get(p.getName()).cancel();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	}
	}
 
}