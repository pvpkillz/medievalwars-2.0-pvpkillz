package kz.PvP.MedievalWars.Commands;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import kz.PvP.MedievalWars.Main;
import kz.PvP.MedievalWars.Enums.Config;
import kz.PvP.MedievalWars.Enums.Game;
import kz.PvP.MedievalWars.Enums.Team;
import kz.PvP.MedievalWars.Methods.GameManagement;
import kz.PvP.MedievalWars.Timers.PreGameTimer;
import kz.PvP.MedievalWars.utilities.Files;
import kz.PvP.MedievalWars.utilities.MessageMW;
import kz.PvP.PkzAPI.methods.WorldEditHook;
import kz.PvP.PkzAPI.utilities.Message;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;



public class GameCommands extends JavaPlugin implements CommandExecutor{
	

	public static HashMap<String, String> mapChosen = new HashMap<String, String>();// Editors Map chosen

	
	public static Main plugin;
	   
	   public GameCommands(Main main) {
		 	plugin = main;

	}
	   
	   
@SuppressWarnings("deprecation")
@Override
public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
{
  Player p = (Player)sender;
  Location loc = p.getLocation();
  double XLook = p.getTargetBlock(null, 100).getX();
  double YLook = p.getTargetBlock(null, 100).getY();
  double ZLook = p.getTargetBlock(null, 100).getZ();
  Location locLook = new Location (p.getWorld(), XLook, YLook, ZLook);
  
  if (cmd.getName().equalsIgnoreCase("mwa") && ((p.hasPermission("mw.*") || (p.hasPermission("mw.admin"))))){
	if (p.hasPermission("mw.mapworks")){
	  if (args.length == 0){
		  String Maps = Files.worlds.getCustomConfig().getStringList("Worlds").toString();
		  Message.P(p, " ", false);
		  Message.P(p, MessageMW.MWBorder, false);
		  Message.P(p, Message.Replacer(MessageMW.AdminMapSet, Maps, "%maps"), false);
		  Message.P(p, MessageMW.AdminWarps, false);
		  Message.P(p, MessageMW.AdminHorse, false);
		  }
		  else if (args[0].equalsIgnoreCase("setMap") && args.length == 2){
			  mapChosen.put(p.getName(), args[1]);
			  Message.P(p, Message.Replacer(MessageMW.ChosenMap, args[1], "%map"), true);
		  }
		  else if (args[0].equalsIgnoreCase("warps") && (args.length == 1)){
			  p.sendMessage(ChatColor.GOLD + "Valid arguments:");
			  p.sendMessage(ChatColor.GOLD + "redspawn, bluespawn, mainlobby, spectator, dead");
		  }
		  else if (args[0].equalsIgnoreCase("warps") && (args.length == 2)){
			  if (args[1].equalsIgnoreCase("redspawn")){
				  saveLocations(p, loc, "Map.Red.Spawns", Files.worldteleports, true);
			  }
			  else if (args[1].equalsIgnoreCase("bluespawn")){
				  saveLocations(p, loc, "Map.Blue.Spawns", Files.worldteleports, true);
			  }
			  else if (args[1].equalsIgnoreCase("mainlobby")){
				  saveLocation(p, loc, "MainLobby", Files.worldteleports, true);
			  }
			  else if (args[1].equalsIgnoreCase("dead")){
				  saveLocation(p, loc, "Map.Death.Spawn", Files.worldteleports, true);
			  }
			  else if (args[1].equalsIgnoreCase("spectator")){
				  saveLocation(p, loc, "Map.Spectator.Spawn", Files.worldteleports, true);
			  }
			  else{
				  p.sendMessage(ChatColor.RED + "Invalid choice");
			  }
		  }
		  else if (args.length == 3 && args[0].equalsIgnoreCase("warps")){
			  if (args[1].equalsIgnoreCase("redspawn")){
				  saveLocations(p, loc, "Map.Red.ClassSpawns." + args[2].toLowerCase(), Files.worldteleports, true);
			  }
			  else if (args[1].equalsIgnoreCase("bluespawn")){
				  saveLocations(p, loc, "Map.Blue.ClassSpawns." + args[2].toLowerCase(), Files.worldteleports, true);
			  }
		  }
		  else if (args[0].equalsIgnoreCase("flag")){
			  
			  
			  if (args[1].equalsIgnoreCase("red"))
				  saveLocation(p, locLook, "Map.Red.Flag", Files.worldteleports, false);
			  else if (args[1].equalsIgnoreCase("blue"))
				  saveLocation(p, locLook, "Map.Blue.Flag", Files.worldteleports, false);
			  
			  
			  
		  }
		  else if (args[0].equalsIgnoreCase("flagstop")){
			  if (kz.PvP.PkzAPI.Main.WorldEditEnable == false){
				  if (args[1].equalsIgnoreCase("blue")){
	
					  if (args[2].equalsIgnoreCase("1"))
						  saveLocation(p, locLook, "Map.Blue.FlagClaim.1", Files.worldteleports, false);
					  else if (args[2].equalsIgnoreCase("2"))
						  saveLocation(p, locLook, "Map.Blue.FlagClaim.2", Files.worldteleports, false);
				  }
				  else if (args[1].equalsIgnoreCase("red")){
				  if (args[2].equalsIgnoreCase("1"))
					  saveLocation(p, locLook, "Map.Red.FlagClaim.1", Files.worldteleports, false);
				  else if (args[2].equalsIgnoreCase("2"))
					  saveLocation(p, locLook, "Map.Red.FlagClaim.2", Files.worldteleports, false);
				  }
			  }
			  else{
				  if (args[1].equalsIgnoreCase("blue")){
					  saveLocation(p, WorldEditHook.getSelectionPoint(p, 1), "Map.Blue.FlagClaim.1", Files.worldteleports, false);
					  saveLocation(p, WorldEditHook.getSelectionPoint(p, 2), "Map.Blue.FlagClaim.2", Files.worldteleports, false);
				  }
				  else if (args[1].equalsIgnoreCase("red")){
					  saveLocation(p, WorldEditHook.getSelectionPoint(p, 1), "Map.Red.FlagClaim.1", Files.worldteleports, false);
					  saveLocation(p, WorldEditHook.getSelectionPoint(p, 2), "Map.Red.FlagClaim.2", Files.worldteleports, false);
				  }
			  }
		  }
		  else if (args[0].equalsIgnoreCase("core")){
			  if (kz.PvP.PkzAPI.Main.WorldEditEnable == false){
				  if (args[1].equalsIgnoreCase("blue")){
	
					  if (args[2].equalsIgnoreCase("1"))
						  saveLocation(p, locLook, "Map.Blue.Core.1", Files.worldteleports, false);
					  else if (args[2].equalsIgnoreCase("2"))
						  saveLocation(p, locLook, "Map.Blue.Core.2", Files.worldteleports, false);
				  }
				  else if (args[1].equalsIgnoreCase("red")){
				  if (args[2].equalsIgnoreCase("1"))
					  saveLocation(p, locLook, "Map.Red.Core.1", Files.worldteleports, false);
				  else if (args[2].equalsIgnoreCase("2"))
					  saveLocation(p, locLook, "Map.Red.Core.2", Files.worldteleports, false);
				  }
			  }
			  else{
				  if (args[1].equalsIgnoreCase("blue")){
					  saveLocation(p, WorldEditHook.getSelectionPoint(p, 1), "Map.Blue.Core.1", Files.worldteleports, false);
					  saveLocation(p, WorldEditHook.getSelectionPoint(p, 2), "Map.Blue.Core.2", Files.worldteleports, false);
				  }
				  else if (args[1].equalsIgnoreCase("red")){
					  saveLocation(p, WorldEditHook.getSelectionPoint(p, 1), "Map.Red.Core.1", Files.worldteleports, false);
					  saveLocation(p, WorldEditHook.getSelectionPoint(p, 2), "Map.Red.Core.2", Files.worldteleports, false);
				  }
			  }
		  }
		  else if (args[0].equalsIgnoreCase("horse")){
			  if (args.length == 1){
				 	p.sendMessage(ChatColor.RED + "/mwa Horse " + ChatColor.GRAY + "<" + ChatColor.RED + "red" + ChatColor.GRAY + "/" + ChatColor.AQUA + "blue" + ChatColor.GRAY + "/" + ChatColor.GOLD + "random" + ChatColor.GRAY + ">");
			  }
			  else{
				  if (args[1].equalsIgnoreCase("red"))
					  saveLocations(p, loc, "Map.Red.Horses", Files.worldteleports, true);
				  else if (args[1].equalsIgnoreCase("blue"))
					  saveLocations(p, loc, "Map.Blue.Horses", Files.worldteleports, true);
			  }
		  }
	  }
	if (p.hasPermission("mw.rounds")){
			if (args.length == 0){
				Message.P(p, "&7/&bmwa &aStartRound", false);
				Message.P(p, "&7/&bmwa &cEndRound", false);
			}
			else if (args[0].equalsIgnoreCase("endround")){
			  if (Main.GameStatus == Game.GAME)
				  GameManagement.endGame(Team.Forced);
			  else
			  p.sendMessage(ChatColor.RED + "You can't stop a game that never started.");
			  }
			else if (args[0].equalsIgnoreCase("startround")){
			  if (Main.GameStatus == Game.PREGAME){
				  Bukkit.getScheduler().cancelTask(PreGameTimer.pgtimer);
				  try {
					GameManagement.BeginGame();
				} catch (SQLException e) { e.printStackTrace(); }
			  }
			  else
			  p.sendMessage(ChatColor.RED + "You can't start a game, that already started.");
		  }
		}
	  return true; 	
	  }
  return false;

}


	public static void saveLocation(Player p, Location loc, String Config, Config worldteleports, boolean UsePitchYaw){
		if (mapChosen.containsKey(p.getName())){
			Config = Config.replace("Map", mapChosen.get(p.getName()));
			worldteleports.getCustomConfig().set(Config + ".X", loc.getBlockX());
			worldteleports.getCustomConfig().set(Config + ".Y", loc.getBlockY());
			worldteleports.getCustomConfig().set(Config + ".Z", loc.getBlockZ());
			worldteleports.getCustomConfig().set(Config + ".World", loc.getWorld().getName());
			if (UsePitchYaw){
				worldteleports.getCustomConfig().set(Config + ".Yaw", loc.getYaw());
				worldteleports.getCustomConfig().set(Config + ".Pitch", loc.getPitch());
			}
			worldteleports.saveCustomConfig();
			worldteleports.reloadCustomConfig();
		}
		else
			Message.P(p, MessageMW.NoMapChosen, true);
	}
	public static void saveLocations(Player p, Location loc, String config, Config configFile, boolean usePitchYaw) {
		if (mapChosen.containsKey(p.getName())){
			configFile.reloadCustomConfig();
			config = config.replace("Map", mapChosen.get(p.getName()));
			if (configFile.getCustomConfig().contains(config) && configFile.getCustomConfig().getConfigurationSection(config).getKeys(false).size() >= 1){
				Set<String> Entries = configFile.getCustomConfig().getConfigurationSection(config).getKeys(false);
				ArrayList<String> array = new ArrayList<String>();
				array.addAll(Entries);
				int lastEntry = Integer.parseInt(array.get(array.size() - 1));
				saveLocation(p, loc, config + "." + (lastEntry + 1), configFile, usePitchYaw);
			}
			else
				saveLocation(p, loc, config + "." + 1, configFile, usePitchYaw);
		}
	}

}