package kz.PvP.MedievalWars.Commands;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import kz.PvP.MedievalWars.Main;
import kz.PvP.MedievalWars.InfoSQL;
import kz.PvP.MedievalWars.Vote;
import kz.PvP.MedievalWars.Enums.ChatType;
import kz.PvP.MedievalWars.Enums.GM;
import kz.PvP.MedievalWars.Enums.Team;
import kz.PvP.MedievalWars.Gamemodes.Global;
import kz.PvP.MedievalWars.Methods.ConvertTimings;
import kz.PvP.MedievalWars.Methods.GameManagement;
import kz.PvP.MedievalWars.Methods.ProtocolLib;
import kz.PvP.MedievalWars.Methods.Stats;
import kz.PvP.MedievalWars.Methods.teamM;
import kz.PvP.MedievalWars.utilities.Files;
import kz.PvP.MedievalWars.utilities.IconMenu;
import kz.PvP.MedievalWars.utilities.Kits;
import kz.PvP.MedievalWars.utilities.MessageMW;
import kz.PvP.PkzAPI.utilities.Message;
import kz.PvP.PkzAPI.utilities.Mysql;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;




public class PlayerCommands implements CommandExecutor{
	
	public static Main plugin;
	   

	   public PlayerCommands(Main main) {
	 	PlayerCommands.plugin = main;
	 	}
		public static HashMap<String, IconMenu> MENUS = new HashMap<String, IconMenu>();
		public static HashMap<String, Integer> Count = new HashMap<String, Integer>();
		public static HashMap<Player, Player> LastMessaged = new HashMap<Player, Player>();
		//public static HashMap<Player, Player> LastMessaged2 = new HashMap<Player, String>();

		
		int count;
		int task;
@Override
public boolean onCommand(CommandSender sender, Command cmd, String label, final String[] args)
{
  final Player p = (Player)sender;
  if (cmd.getName().equalsIgnoreCase("mw")){
	  	  MainMenu(p);
		  return true;
  }
  else if (cmd.getName().equalsIgnoreCase("stats")){
	  if (args.length == 0){
		  Message.P(p, "You can display stats by typing the following:", true);
		  Message.P(p, "/Stats <Player>", false);
	  }
	  else{
		  String pname = args[0];
		  try {
			if (Mysql.GetUserID(pname) != 0){
				int Kills = Stats.getTotalKills(pname);
				int Deaths = Stats.getTotalDeaths(pname);
				Message.P(p, Message.Replacer(MessageMW.StatsForUser, pname, "%player"), false);
				Message.P(p, Message.Replacer(Message.Replacer(MessageMW.KillsNDeaths, Deaths + "" , "%deaths"), "" + Kills, "%kills"), false);
				if (Deaths == 0)
					Deaths = 1;
				double KD = Kills/Deaths;
				DecimalFormat df = new DecimalFormat();
				df.setMaximumFractionDigits(2);
				Message.P(p, Message.Replacer(MessageMW.KDRatio, df.format(KD), "%ratio"), false);
				Message.P(p, Message.Replacer(MessageMW.PointCount, InfoSQL.getPlayerPts(pname) + "", "%points"), false);
				Message.P(p, Message.Replacer(MessageMW.CurrentRank, Global.ChatRank(pname, false), "%rank"), false);
			}
			else
				  Message.P(p, MessageMW.PlayerNeverJoined, false);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	  }
	  return true;
  }
  	else if (cmd.getName().equalsIgnoreCase("help")){
	  Message.P(p, " ", false);
	  Message.P(p, MessageMW.MWBorder, false);
	  Message.P(p, " ", false);
	  Message.P(p, MessageMW.HelpCommands, false);
	  Message.P(p, Message.Replacer(MessageMW.CurrentGamemode, Main.Gamemode.toString(), "%gamemode"), false);
	  Message.P(p, " ", false);
	  Message.P(p, Message.Replacer(MessageMW.CurrentObjective, GameManagement.objectives, "%objective"), false);
	  Message.P(p, " ", false);
  	  return true;
  	}
  	else if (cmd.getName().equalsIgnoreCase("tell")){
  		if (args.length <= 1){
  	  		Message.P(p, MessageMW.PrivateMessageHelp, true);
  	  		Message.P(p, MessageMW.PrivateMessageCmd, true);
  		}
  		else if (args.length >= 2){
  	  		String recp = args[0];
  	  		String msg = "";

  	  		for (int a = 1; a <= args.length - 1; a++)
  	  			msg = msg + " " + args[a];
  	  		
  	  		Player pl = teamM.getPlayerFromSome(recp);
  	  		if (pl != null){
  	  	  		Message.P(p,Message.Replacer(Message.Replacer(MessageMW.SentTo, pl.getName(), "%target"), msg, "%msg"), false);
  	  	  		Message.P(pl,Message.Replacer(Message.Replacer(MessageMW.SentFrom, p.getName(), "%sender"), msg, "%msg"), false);
  	  	  		LastMessaged.put(p, pl);
  	  	  		LastMessaged.put(pl, p);
  	  	  	}
  	  		else
  	  	  		Message.P(p, MessageMW.PlayerNotOnline, true);
  		}
  		
	  return true;
  	}
	  else if (cmd.getName().equalsIgnoreCase("leave")){
		  teamM.assignTeam(p.getName(), Team.Spectator);
		  return true;
	  }
	  else if (cmd.getName().equalsIgnoreCase("points")){
		  try {
			 Message.P(p, Message.Replacer(MessageMW.PointCount, InfoSQL.getPlayerPts(p.getName()) + "", "%points"), false);
		} catch (SQLException e) {
			Message.P(p, ChatColor.RED + "Failed getting points. Contact Undeadkillz!", true);
		}
		  return true;
	  }
	  else if (cmd.getName().equalsIgnoreCase("rtv")){
		  Vote.ShowVoteMenu(p);
		  return true;
	  }
	  else if (cmd.getName().equalsIgnoreCase("vote")){
		  Vote.ShowVoteMenu(p);
		  return true;
	  }
	  else if (cmd.getName().equalsIgnoreCase("chat")){
		  if (teamM.Chatting.containsKey(p.getName())){
			  if (teamM.Chatting.get(p.getName()) == ChatType.Private){
				  teamM.Chatting.put(p.getName(), ChatType.Public);
				  Message.P(p, MessageMW.ChattingPublic, true);
			  }
			  else{
				  teamM.Chatting.put(p.getName(), ChatType.Private);
				  Message.P(p, MessageMW.ChattingPrivate, true);
			  }
		  }
		  else{
			  teamM.Chatting.put(p.getName(), ChatType.Public);
			  Message.P(p, MessageMW.ChattingPublic, true);
		  }
		  return true;
	  }
	  else if (cmd.getName().equalsIgnoreCase("notify")){
		if (teamM.DoNotNotify.contains(p.getName())){
			Message.P(p, MessageMW.NotificationEnabled, false);
			teamM.DoNotNotify.remove(p.getName());
		}
		else{
			Message.P(p, MessageMW.NotificationDisabled, false);
			teamM.DoNotNotify.add(p.getName());
		}
		return true;
	  }
	  else if (cmd.getName().equalsIgnoreCase("class")){
				try {
					OpenClassMenu(p);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		  return true;
	  }
	  else if (cmd.getName().equalsIgnoreCase("activate")){
		  try {
			ActivateMenuWithoutClass(p);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	  return true;
	}
	  else if (cmd.getName().equalsIgnoreCase("upgrade")){
		  try {
			UpgradeMenuWithoutClass(p);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	  return true;
	}
	  else if (cmd.getName().equalsIgnoreCase("team")){
		  try {
			OpenTeamMenu(p);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		  return true;
	  }
  return false;
}


public static void OpenTeamMenu(Player p) throws SQLException {
	if (Main.Gamemode != GM.Elimination){
		// if(MENUS.containsKey(p)) {
		//	 MENUS.get(p).destroy();
		//	 MENUS.remove(p);
		// }
    IconMenu menu = new IconMenu("Team Selection | Choose A Team", p.getName(), 9, new IconMenu.OptionClickEventHandler() {
        @Override
        public void onOptionClick(final IconMenu.OptionClickEvent event) {
        	final Player p = event.getPlayer();
        	if (event.getPosition() == 2)
        		teamM.assignTeam(p.getName(), Team.Red);
        	else if (event.getPosition() == 4)
            	teamM.assignTeam(p.getName(), Team.Random);
            else if (event.getPosition() == 6)
            	teamM.assignTeam(p.getName(), Team.Blue);
        	
        	
			if (!Kits.KitChoice.containsKey(p)){
    		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
    			public void run() {
    				try {
						OpenClassMenu(event.getPlayer());
					} catch (SQLException e) {
						e.printStackTrace();
					}
    			}
    		}, 3L);
			}
            event.setWillClose(true);
            event.setWillDestroy(false);
            

        }
    }, plugin)
    .setOption(0, new ItemStack(Material.WOOL, 1), ChatColor.GRAY + "Spectate", ChatColor.WHITE + "Click me, to spectate!")
    .setOption(2, new ItemStack(Material.WOOL, 1,(short)14), ChatColor.RED + "Red Team", ChatColor.RED + "Click me, to join the red team!")
    .setOption(4, new ItemStack(Material.WOOL, 1,(short)4), ChatColor.YELLOW + "Random Team", ChatColor.YELLOW + "Click me, to join any team!")
    .setOption(6, new ItemStack(Material.WOOL, 1,(short)11), ChatColor.AQUA + "Blue Team", ChatColor.AQUA + "Click me, to join the blue team!")
    .setOption(8, new ItemStack(Material.WOOL, 1), ChatColor.GRAY + "Spectate", ChatColor.WHITE + "Click me, to spectate!");
    if (MENUS.containsKey(p.getName())){
        MENUS.get(p.getName()).destroy();
        MENUS.remove(p.getName());
    }
        menu.open(p);
        MENUS.put(p.getName(), menu);
	}
	else {
		Message.P(p, MessageMW.TeamEliminationActive, true);
	}
}


@SuppressWarnings("deprecation")
public static void OpenPlayersMenu(final Player p) throws SQLException {
		 Integer invsize = 54;

		 IconMenu menu = new IconMenu(ChatColor.GOLD + "Current Players Alive", p.getName(), invsize, new IconMenu.OptionClickEventHandler() {
	            public void onOptionClick(IconMenu.OptionClickEvent event) {
	            	Player target = teamM.getPlayerFromSome(ChatColor.stripColor(event.getName()));
	            	p.teleport(target.getLocation().add(0, 2, 0));
	            	Message.P(p, Message.Replacer(MessageMW.TeleportedToPlayer, target.getName(), "%target"), true);
	                event.setWillClose(true);
	                event.setWillDestroy(false);
	    		    if (MENUS.containsKey(p.getName()))
	    		        MENUS.get(p.getName()).destroy();
	            }
	        }, plugin);
		 int pos = 0;
		 int maxpos = 27;
		 for (Player player : teamM.getTeam(Team.Red)){
			 if (pos == maxpos)
				 break;
					ArrayList<String> container = new ArrayList<String>();
					String KitName = "";
					if (!Kits.KitRound.containsKey(player))
					KitName = "knight";
					else
					KitName = Kits.KitRound.get(player);
						
					ConfigurationSection kit = Files.classes.getCustomConfig().getConfigurationSection(KitName);
					
					String Item = kit.getString("ITEMMENU");
					int itemid = 0;
					ItemStack i = null;
					short durability = 0;
					
					if (Item.contains(":")){
					String[] Icon = Item.split(":");
					itemid = Integer.parseInt(Icon[0]);
					durability = (short) Integer.parseInt(Icon[1]);
					i = new ItemStack(itemid, 1,
							durability);
					}
					else{
						i = new ItemStack(Integer.parseInt(Item), 1);
					}
					int TotalBandages = 0;
		        	for (ItemStack is : player.getInventory().getContents()){
		        		if (is != null && is.getTypeId() == 399){
		        			if (is.hasItemMeta() && is.getItemMeta().hasDisplayName() == true && is.getItemMeta().getDisplayName().toLowerCase().contains("player bandaid"))
		        				TotalBandages = is.getAmount() + TotalBandages;
		        		}
		        	}
		        	int Kills = Stats.getTotalKills(player.getName());
		        	
					container.add("&7Bandages: " + TotalBandages);
					container.add("&7HP " + Math.round(player.getHealth() / 2) + " &4&l♥");
					container.add("&7Total Kills: &c" + Kills);
					container.add("&7Kill Rank: &6" + Global.ChatRank(player.getName(), false));
					if (Item.contains(",")){
						String[] itemench = Item.split(",");
						i.addUnsafeEnchantment(
							Enchantment.getById(Integer.parseInt(itemench[0])),
							Integer.parseInt(itemench[1]));
					}
					String[] info = new String[container.size()];
					info = container.toArray(info);
					
					
					boolean fakeenchant = kit.getBoolean("EnchantFakeIcon");
					
					if (fakeenchant == true && Main.ProtocolLibEnable){
					i = ProtocolLib.setGlowing(i);
					}
					menu.setOption(pos, i, ChatColor.RED + player.getName(), info);
					pos++;
					container.clear();
		 }
		 pos = 27;
		 maxpos = 54;
		 for (Player player : teamM.getTeam(Team.Blue)){
			 if (pos == maxpos)
				 break;
				ArrayList<String> container = new ArrayList<String>();
				String KitName = "";
				if (!Kits.KitRound.containsKey(player))
				KitName = "knight";
				else
				KitName = Kits.KitRound.get(player);
					
				ConfigurationSection kit = Files.classes.getCustomConfig().getConfigurationSection(KitName);
				
				String Item = kit.getString("ITEMMENU");
				int itemid = 0;
				ItemStack i = null;
				short durability = 0;
				
				if (Item.contains(":")){
				String[] Icon = Item.split(":");
				itemid = Integer.parseInt(Icon[0]);
				durability = (short) Integer.parseInt(Icon[1]);
				i = new ItemStack(itemid, 1,
						durability);
				}
				else{
					i = new ItemStack(Integer.parseInt(Item), 1);
				}
				int TotalBandages = 0;
	        	for (ItemStack is : player.getInventory().getContents()){
	        		if (is != null && is.getTypeId() == 399){
	        			if (is.hasItemMeta() && is.getItemMeta().hasDisplayName() == true && is.getItemMeta().getDisplayName().toLowerCase().contains("player bandaid"))
	        				TotalBandages = is.getAmount() + TotalBandages;
	        		}
	        	}
	        	int Kills = Stats.getTotalKills(player.getName());
	        	
				container.add("&7Bandages: " + TotalBandages);
				container.add("&7HP " + Math.round(player.getHealth() / 2) + " &4&l♥");
				container.add("&7Total Kills: &c" + Kills);
				container.add("&7Kill Rank: &6" + Global.ChatRank(player.getName(), false));
				if (Item.contains(",")){
					String[] itemench = Item.split(",");
					i.addUnsafeEnchantment(
						Enchantment.getById(Integer.parseInt(itemench[0])),
						Integer.parseInt(itemench[1]));
				}
				String[] info = new String[container.size()];
				info = container.toArray(info);
				
				
				boolean fakeenchant = kit.getBoolean("EnchantFakeIcon");
				
				
				if (fakeenchant == true && Main.ProtocolLibEnable){
					i = ProtocolLib.setGlowing(i);
				}
				
				menu.setOption(pos, i, ChatColor.AQUA + player.getName(), info);
				pos++;
				container.clear();
	 }
					
		    if (MENUS.containsKey(p.getName())){
		        MENUS.get(p.getName()).destroy();
		        MENUS.remove(p.getName());
		    }
		        menu.open(p);
		        MENUS.put(p.getName(), menu);
		        
		        
	  }


@SuppressWarnings("deprecation")
public static void OpenActivateMenu(final Player p, final String Class1) throws SQLException {
	  Set<String> classs = Files.upgrades.getCustomConfig().getConfigurationSection(Class1.toLowerCase()).getKeys(false);

		 Integer invsize = 27;

			// if(MENUS.containsKey(p)) {
			//	 MENUS.get(p).destroy();
			//	 MENUS.remove(p);
			// }

		 IconMenu menu = new IconMenu(ChatColor.RED + "Activate Menu  Pts: " + InfoSQL.getPlayerPts(p.getName()), p.getName(), invsize, new IconMenu.OptionClickEventHandler() {
	            public void onOptionClick(IconMenu.OptionClickEvent event) {
	            	try {
						InfoSQL.ActivateUpgrade(event.getPlayer(), ChatColor.stripColor(event.getName().toLowerCase()),Class1.toLowerCase());
						} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	        		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
	        			public void run() {
	    	            	try {
								OpenActivateMenu(p, Class1);
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
	        			}
	        		}, 3L);	    
	            	
	                event.setWillClose(true);
	                event.setWillDestroy(false);
	                
	    

	            }
	        }, plugin);

		 for (String upgrade : classs){
			 try {
				  ConfigurationSection upgrades = Files.upgrades.getCustomConfig().getConfigurationSection(Class1.toLowerCase() + "." + upgrade);
					ArrayList<String> container = new ArrayList<String>();
					String desc = upgrades.getString("Description");
					if (desc.contains("<endline>")){
						String[] splitinfo = desc.split("<endline>");
						for (String descline : splitinfo){
							container.add(ChatColor.LIGHT_PURPLE + "" + ChatColor.ITALIC + descline);
						}
						
						
					}
					else
					container.add(ChatColor.LIGHT_PURPLE + "" + ChatColor.ITALIC + desc);
					
					
					
					
					if (InfoSQL.GetUserActiveUps(p.getName()).contains(" " + Class1.toLowerCase() + ":" + upgrades.getName().toLowerCase())){
						container.add(ChatColor.GREEN + "Activated");
						container.add(ChatColor.GRAY + "Click to deactivate");
					}
					else{
						if (!InfoSQL.GetUserUps(p.getName()).contains(" " + Class1.toLowerCase() + ":" + upgrades.getName().toLowerCase()))
							container.add(ChatColor.RED + "Upgrade not purchased");
						else{
						container.add(ChatColor.GRAY + "Inactive");
						container.add(ChatColor.GRAY + "Click to activate");
						}
						}
					
					String Item = upgrades.getString("IconItem");
					int itemid = 0;
					ItemStack i = null;
					short durability = 0;
					
					if (Item.contains(":")){
					String[] Icon = Item.split(":");
					itemid = Integer.parseInt(Icon[0]);
					durability = (short) Integer.parseInt(Icon[1]);
					i = new ItemStack(itemid, 1,
							durability);
					}
					else{
						i = new ItemStack(Integer.parseInt(Item), 1);
					}
					
					if (Item.contains(",")){
						String[] itemench = Item.split(",");
						i.addUnsafeEnchantment(
							Enchantment.getById(Integer.parseInt(itemench[0])),
							Integer.parseInt(itemench[1]));
					}
					
					
					int pos = upgrades.getInt("Position");

					Material.getMaterial(itemid);
					String[] info = new String[container.size()];
					info = container.toArray(info);
					
					
					boolean fakeenchant = upgrades.getBoolean("EnchantFakeIcon");
					if (fakeenchant == true && Main.ProtocolLibEnable){
						i = ProtocolLib.setGlowing(i);
					}
					
					
					
					menu.setOption(pos, i, ChatColor.GREEN + upgrades.getName(), info);
					container.clear();
			} catch (Exception e) {
				e.printStackTrace();
			}
		 }
		 //MENUS.put(p, menu);
		    if (MENUS.containsKey(p.getName())){
		        MENUS.get(p.getName()).destroy();
		        MENUS.remove(p.getName());
		    }
		        menu.open(p);
		        MENUS.put(p.getName(), menu);
		 //MENUS.clear();
	  }


public static void OpenClassMenu(Player p) throws SQLException {
	  Set<String> kits = Files.classes.getCustomConfig().getKeys(false);
		 Integer invsize = 9;
		 for(int i=0; i<=10; i++) {
			 if((i*9) >= kits.size()) {
				 invsize = invsize + i*9;
				 break;
			 }
		 }

		// if(MENUS.containsKey(p)) {
		//	 MENUS.get(p).destroy();
		//	 MENUS.remove(p);
		// }
		 int userid = Mysql.GetUserID(p.getName());
		 IconMenu menu = new IconMenu(ChatColor.RED + "Class selection menu", p.getName(), invsize, new IconMenu.OptionClickEventHandler() {
	            public void onOptionClick(IconMenu.OptionClickEvent event) {
	            	try {
						Kits.Choose(event.getPlayer(), ChatColor.stripColor(event.getName().toLowerCase()));
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	                event.setWillClose(true);
	                event.setWillDestroy(false);
	                
	    

	            }
	        }, plugin);
		 OpenMenu(menu, userid, invsize, kits);
		 //MENUS.put(p, menu);
		    if (MENUS.containsKey(p.getName())){
		        MENUS.get(p.getName()).destroy();
		        MENUS.remove(p.getName());
		    }
		        menu.open(p);
		        MENUS.put(p.getName(), menu);
		 //MENUS.clear();
}


public static void UpgradeMenuWithoutClass(final Player p) throws SQLException{
	  Set<String> kits = Files.classes.getCustomConfig().getKeys(false);

		 Integer invsize = 9;
		 for(int i=0; i<=10; i++) {
			 if((i*9) >= kits.size()) {
				 invsize = invsize + i*9;
				 break;
			 }
		 }

			// if(MENUS.containsKey(p)) {
			//	 MENUS.get(p).destroy();
			//	 MENUS.remove(p);
			// }

		 int userid = Mysql.GetUserID(p.getName());
		 
		 IconMenu menu = new IconMenu(ChatColor.RED + "Class upgrade menu", p.getName(), invsize, new IconMenu.OptionClickEventHandler() {
	            public void onOptionClick(final IconMenu.OptionClickEvent event) {
	        		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
	        			public void run() {
	    	            	try {
								UpgradeMenuWithClass(event.getPlayer(), ChatColor.stripColor(event.getName().toLowerCase()));
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
	        			}
	        		}, 3L);	                
	                event.setWillClose(true);
	                event.setWillDestroy(false);
	                
	    

	            }
	        }, plugin);

		 OpenMenu(menu, userid, invsize, kits);
		 //MENUS.put(p, menu);
		    if (MENUS.containsKey(p.getName())){
		        MENUS.get(p.getName()).destroy();
		        MENUS.remove(p.getName());
		    }
		        menu.open(p);
		        MENUS.put(p.getName(), menu);
		 //MENUS.clear();
}



public static void ActivateMenuWithoutClass(final Player p) throws SQLException{
	  Set<String> kits = Files.classes.getCustomConfig().getKeys(false);

		 Integer invsize = 9;
		 for(int i=0; i<=10; i++) {
			 if((i*9) >= kits.size()) {
				 invsize = invsize + i*9;
				 break;
			 }
		 }

			// if(MENUS.containsKey(p)) {
			//	 MENUS.get(p).destroy();
			//	 MENUS.remove(p);
			// }
		 int userid = Mysql.GetUserID(p.getName());
		 IconMenu menu = new IconMenu(ChatColor.RED + "Class activate menu", p.getName(), invsize, new IconMenu.OptionClickEventHandler() {
	            public void onOptionClick(final IconMenu.OptionClickEvent event) {
	        		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
	        			public void run() {
	    	            	try {
								OpenActivateMenu(event.getPlayer(), ChatColor.stripColor(event.getName().toLowerCase()));
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
	        			}
	        		}, 3L);	                
	                event.setWillClose(true);
	                event.setWillDestroy(false);
	    

	            }
	        }, plugin);
OpenMenu(menu, userid, invsize, kits);
		 //MENUS.put(p, menu);
if (MENUS.containsKey(p.getName())){
    MENUS.get(p.getName()).destroy();
    MENUS.remove(p.getName());
}
    menu.open(p);
    MENUS.put(p.getName(), menu);
		 //MENUS.clear();
}



@SuppressWarnings("deprecation")
private static void OpenMenu(IconMenu menu, int userid, Integer invsize, Set<String> kits) {

	 Integer mypos = 0;
	 Integer othpos = 1;
	 for(String kitname : kits) {	
		 try {

				ArrayList<String> container = new ArrayList<String>();
				container.add("&a&m--->>&a Description");
				ConfigurationSection kit = Files.classes.getCustomConfig().getConfigurationSection(kitname.toLowerCase());
				List<String> kititems = kit.getStringList("Items");
				for (String itemone : kititems){
				 ItemStack is = Kits.getItemInfo(itemone, null);
				 String Item = Message.CleanCapitalize(is.getType().toString());
				 if (is.hasItemMeta() && is.getItemMeta().hasDisplayName())
					 Item = Message.CleanCapitalize(is.getItemMeta().getDisplayName());
				 container.add("&c ● &7" + is.getAmount() + "&8x &7" + Item);
				 }

				boolean fakeenchant = kit.getBoolean("EnchantFakeIcon");
				
				String iconmenu = kit.getString("ITEMMENU");
				
				String[] iconitem = null;
				int iconid = 1;
				int iconamount = 1;
				short icondata = 0;
				if (iconmenu != null && iconmenu.contains(",")){
					iconitem = iconmenu.split(",");
					if (iconitem[0].contains(":")){
					iconamount = Integer.parseInt(iconitem[1]);
					iconitem = iconitem[0].split(":");
					iconid = Integer.parseInt(iconitem[0]);
					icondata = (short) Integer.parseInt(iconitem[1]);
					}
					else{
					iconid = Integer.parseInt(iconitem[0]);
					iconamount = Integer.parseInt(iconitem[1]);
					}
				}
				else if (iconmenu != null){
					iconid = Integer.parseInt(iconmenu);
				}
				
				ItemStack is = new ItemStack(Material.getMaterial(iconid), iconamount, icondata);
				
				container.add(" ");
				
				if (kit.contains("PlayerBandage"))
				container.add(ChatColor.GOLD + "Player bandages: " + kit.getString("PlayerBandage"));
				if (kit.contains("HorseBandage"))
				container.add(ChatColor.GOLD + "Horse bandages: " + kit.getString("HorseBandage"));
				
				
				container.add(" ");
				if (kit.contains("Potions")){
					container.add(" ");
					
					List<String> pots = kit.getStringList("Potions");
					if (pots.size() >= 1){
						container.add("&a&m--->>&a Potions");
						for(String pot : pots) {
							if (pot != null & pot != "") {
								PotionEffect pe = Kits.getPotionInfo(pot, null);
								  String Potion = Message.CleanCapitalize(pe.getType().getName());
								  String Time = ConvertTimings.convertTime((int)pe.getDuration()/20);
								  if (Time.contains("hour"))
									  Time = "| &7Permenant";
								  else
									  Time = "for &7" + Time;
								  
								  container.add("&c ● &7" + Potion + " " + Time);
							}
						}
					}
				}
				
				if (kit.contains("Abilities")){
					container.add("&a&m--->>&a Abilities");
					List<String> abilities = kit.getStringList("Abilities");
					for (String ability : abilities)
					container.add(ChatColor.GRAY + " * " + ability);
					container.add(" ");
				}
				if (kit.contains("Strengths")){
					container.add("&a&m--->>&a Strengths");
				List<String> strengths = kit.getStringList("Strengths");
				for (String strength : strengths)
					container.add(ChatColor.GRAY + " * " +  strength);
				}
				container.add(" ");
				String paid = "";
				if (kit.getBoolean("Paid"))
					paid = "Paid";
				else
					paid = "Free";
				container.add("Type: " + paid);

				
				
				
				
				
				
				if (fakeenchant == true && Main.ProtocolLibEnable == true){
					is = ProtocolLib.setGlowing(is);
				}
				
				ArrayList<String> coloredcontainer = new ArrayList<String>();
				
				for (String continfo : container){
					if (continfo != null)
					coloredcontainer.add(ChatColor.translateAlternateColorCodes('&', continfo));
				}
				
				String[] info = new String[coloredcontainer.size()];
				info = coloredcontainer.toArray(info);
				
				if (kit.getBoolean("Paid")){
					//Paid Kit
					menu.setOption(invsize - othpos, is, ChatColor.GREEN + Message.CleanCapitalize(kitname), info);
					mypos++;
					othpos++;
				}
				else{
					//Free Kit

					menu.setOption(mypos, is, ChatColor.GREEN + Message.CleanCapitalize(kitname), info);
					mypos++;
				}
				
				
				
				
			container.clear();
		} catch (Exception e) {
			e.printStackTrace();
		}	
	 }
}


@SuppressWarnings("deprecation")
private static void UpgradeMenuWithClass(final Player p, final String CClass) throws SQLException {
	  Set<String> classs = Files.upgrades.getCustomConfig().getConfigurationSection(CClass.toLowerCase()).getKeys(false);

		 Integer invsize = 27;

			  IconMenu menu = new IconMenu(ChatColor.GOLD + "Class upgrade | Pts: " + InfoSQL.getPlayerPts(p.getName()), p.getName(), invsize, new IconMenu.OptionClickEventHandler() {
			        public void onOptionClick(IconMenu.OptionClickEvent event) {
			        	try {
							InfoSQL.BuyUpgrade(event.getPlayer(), ChatColor.stripColor(event.getName().toLowerCase()), Files.upgrades.getCustomConfig().getInt(CClass.toLowerCase() + "." + ChatColor.stripColor(event.getName()) + ".Price"), CClass.toLowerCase());
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
		                event.setWillClose(true);
		                event.setWillDestroy(false);
			        }
			    }, plugin);

		 for (String upgrade : classs){
			 try {
				  ConfigurationSection upgrades = Files.upgrades.getCustomConfig().getConfigurationSection(CClass.toLowerCase() + "." + upgrade);
					ArrayList<String> container = new ArrayList<String>();
					String desc = upgrades.getString("Description");
					if (desc.contains("<endline>")){
						String[] splitinfo = desc.split("<endline>");
						for (String descline : splitinfo){
							container.add(ChatColor.LIGHT_PURPLE + "" + ChatColor.ITALIC + descline);
						}
						
						
					}
					else
					container.add(ChatColor.LIGHT_PURPLE + "" + ChatColor.ITALIC + desc);
					
					
					
					
					if (InfoSQL.GetUserUps(p.getName()).contains(" " + CClass.toLowerCase() + ":" + upgrades.getName().toLowerCase())){
						container.add(ChatColor.GREEN + "Already owned");
					}
					else{
						container.add(ChatColor.GOLD + "Price: " + upgrades.getInt("Price") + " Points");
					}
					
					String Item = upgrades.getString("IconItem");
					int itemid = 0;
					ItemStack i = null;
					short durability = 0;
					
					if (Item.contains(":")){
					String[] Icon = Item.split(":");
					itemid = Integer.parseInt(Icon[0]);
					durability = (short) Integer.parseInt(Icon[1]);
					i = new ItemStack(itemid, 1,
							durability);
					}
					else{
						i = new ItemStack(Integer.parseInt(Item), 1);
					}
					
					if (Item.contains(",")){
						String[] itemench = Item.split(",");
						i.addUnsafeEnchantment(
							Enchantment.getById(Integer.parseInt(itemench[0])),
							Integer.parseInt(itemench[1]));
					}
					
					
					int pos = upgrades.getInt("Position");

					Material.getMaterial(itemid);
					String[] info = new String[container.size()];
					info = container.toArray(info);
					
					boolean fakeenchant = upgrades.getBoolean("EnchantFakeIcon");
					if (fakeenchant == true && Main.ProtocolLibEnable){
						i = ProtocolLib.setGlowing(i);
					}
					menu.setOption(pos, i, ChatColor.GREEN + upgrades.getName(), info);
					container.clear();
			} catch (Exception e) {
				e.printStackTrace();
			}
		 }
		    if (MENUS.containsKey(p.getName())){
		        MENUS.get(p.getName()).destroy();
		        MENUS.remove(p.getName());
		    }
		        menu.open(p);
		        MENUS.put(p.getName(), menu);
}





public static void MainMenu(Player p) {
	  Message.P(p, MessageMW.MWBorder, false);
	  Message.P(p, MessageMW.CommandTeamUsage, false);
	  Message.P(p, MessageMW.CommandPointsUsage, false);
	  Message.P(p, MessageMW.CommandClassUsage, false);
	  Message.P(p, MessageMW.CommandUpgradeUsage, false);
	  Message.P(p, MessageMW.CommandActivateUsage, false);
	  Message.P(p, MessageMW.CommandNotifyUsage, false);
	  Message.P(p, MessageMW.CommandLeaveUsage, false);
	  Message.P(p, MessageMW.MWBorder, false);
}

}
