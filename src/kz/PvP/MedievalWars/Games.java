package kz.PvP.MedievalWars;

import kz.PvP.MedievalWars.Enums.GM;
import kz.PvP.MedievalWars.Enums.Team;
import kz.PvP.MedievalWars.Methods.GameManagement;
import kz.PvP.MedievalWars.Methods.ProtocolLib;
import kz.PvP.MedievalWars.Methods.teamC;
import kz.PvP.MedievalWars.Methods.teamM;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.ScoreboardManager;



public class Games {
	public static Main plugin;
	
	
	public Games(Main mainclass) {
		plugin = mainclass;
		}
	public static int Gamesrun = 0;
	// Organized Variables
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static void RefreshTags() {
		if (Main.TabAPIEnable)
		for (Player p : Bukkit.getOnlinePlayers()){
			ProtocolLib.TabRefresh(p);
		}
	}
	static org.bukkit.scoreboard.Scoreboard board;
	static ScoreboardManager manager;
	public static Objective objective;
	
	
	



	public static void CheckUpStatus() {
		// TODO Auto-generated method stub
		GM Gamemode = Main.Gamemode;
		
		
		if (Gamemode == GM.CTF){
			if (teamC.getBlue().getCaptures() >= GameManagement.CTFCapturesNeeded){
				GameManagement.endGame(Team.Blue);
			}
			else if (teamC.getRed().getCaptures() >= GameManagement.CTFCapturesNeeded){
				GameManagement.endGame(Team.Red);
			}
			 
		}
		else if (Gamemode == GM.Elimination){
			if (teamM.getTeamSize(Team.Red) == 0){
				GameManagement.endGame(Team.Blue);
			}
			else if (teamM.getTeamSize(Team.Blue) == 0){
				GameManagement.endGame(Team.Red);
			}
		}
		else if (Gamemode == GM.Mixed){
			if (teamC.getBlue().getScore() >= GameManagement.MixedScoreReq){
				GameManagement.endGame(Team.Blue);
			}
			else if (teamC.getRed().getScore() >= GameManagement.MixedScoreReq){
				GameManagement.endGame(Team.Red);
			}
			
		}
		else if (Gamemode == GM.TDM){
			if (teamC.getBlue().getTotalKills() >= GameManagement.TDMKillsReq){
				GameManagement.endGame(Team.Blue);
			}
			else if (teamC.getRed().getTotalKills() >= GameManagement.TDMKillsReq){
				GameManagement.endGame(Team.Red);
			}
		}
		else if (Gamemode == GM.DTC){
			if (teamC.getBlue().getTotalKills() >= GameManagement.TDMKillsReq){
				GameManagement.endGame(Team.Blue);
			}
			else if (teamC.getRed().getTotalKills() >= GameManagement.TDMKillsReq){
				GameManagement.endGame(Team.Red);
			}
			
		}
	}
	
	
	
}
