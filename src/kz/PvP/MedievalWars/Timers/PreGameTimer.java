package kz.PvP.MedievalWars.Timers;

import java.sql.SQLException;
import java.util.ArrayList;

import kz.PvP.MedievalWars.Games;
import kz.PvP.MedievalWars.Main;
import kz.PvP.MedievalWars.Enums.Game;
import kz.PvP.MedievalWars.Enums.GM;
import kz.PvP.MedievalWars.Enums.Team;
import kz.PvP.MedievalWars.Methods.GameManagement;
import kz.PvP.MedievalWars.Methods.teamM;
import kz.PvP.MedievalWars.utilities.MessageMW;
import kz.PvP.PkzAPI.utilities.Message;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;



public class PreGameTimer{
	public static int pgtimer;
	
	
	public static ArrayList<Player> RandomizerTeam = new ArrayList<Player>();
	
	
	public static Main plugin;
	
	public PreGameTimer(Main main) {
	plugin = main;
	}
	static String TMinutes;
	static String time;
	public static int WaitingTime = GameManagement.PregameWaiting;
	public static GM gmed;
	public static void BeginTimer() {
	pgtimer = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
	    @Override  
	    public void run() {
			for (Player p : plugin.getServer().getOnlinePlayers()){
				p.setLevel(WaitingTime);
			}
	    	if (WaitingTime > 0){
	    		if (WaitingTime % 10 == 0){
	    			double Minutes = 0;
					if (WaitingTime >= 60){
	    			Minutes = WaitingTime/60;
	    			if ((int)Minutes == 1)
	    			TMinutes = (int)Minutes  + " minute.";
	    			else
		    		TMinutes = (int)Minutes  + " minutes.";
	    			
	    			//Bukkit.getServer().broadcastMessage(ChatColor.GRAY + "Game starting in " + TMinutes);
					}
					else{
		    			if (WaitingTime == 1)
			    			time = " second";
			    			else
			    			time = " seconds";
						//Bukkit.getServer().broadcastMessage(ChatColor.GRAY + "Game starting in " + WaitingTime + time);
					}
	    		}
	    		else if (WaitingTime == 20 && Games.Gamesrun == 5){
	    			Message.G(MessageMW.AllPlayersRandomized, true);
	    			RandomizerTeam.addAll(teamM.getTeam(Team.Blue));
	    			RandomizerTeam.addAll(teamM.getTeam(Team.Red));
	    			teamM.getTeam(Team.Red).clear();
	    			teamM.getTeam(Team.Blue).clear();
	    			for (Player p : RandomizerTeam)
	    				teamM.assignTeam(p.getName(), Team.Random);
	    				
	    				
	    			Games.Gamesrun = 0;
	    		}
	    		else if (WaitingTime <= 9){
	    			
	    			for (Player pl : Bukkit.getServer().getOnlinePlayers()){
						pl.playSound(pl.getLocation(), Sound.CLICK, 1.0F, (byte) 1);
	    			}
	    		}
	    		WaitingTime--;
	    	}
	    	else if (WaitingTime == 0){
	    		
	    		
	    		
	    		if ((teamM.getTeamSize(Team.Red) >= GameManagement.MinPlayersNeeded) && (teamM.getTeamSize(Team.Blue)>= GameManagement.MinPlayersNeeded) && (Main.GameStatus == Game.PREGAME)){
					try {
						GameManagement.BeginGame();
				    	Bukkit.getScheduler().cancelTask(pgtimer);
					} catch (SQLException e) {
						e.printStackTrace();
					}
	    		}
	    		else{
	    			WaitingTime = GameManagement.PregameWaiting;
	    		}
	    	}
	    }
	}, 20L, 20L);
	}
}
