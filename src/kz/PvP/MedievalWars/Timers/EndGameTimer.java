package kz.PvP.MedievalWars.Timers;

import kz.PvP.MedievalWars.Games;
import kz.PvP.MedievalWars.Main;
import kz.PvP.MedievalWars.Enums.GM;
import kz.PvP.MedievalWars.Methods.ConvertTimings;
import kz.PvP.MedievalWars.Methods.GameManagement;
import kz.PvP.MedievalWars.Methods.teamM;
import kz.PvP.MedievalWars.utilities.MessageMW;
import kz.PvP.PkzAPI.utilities.Message;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;



public class EndGameTimer {
	public static int pgtimer;
	
	public static Main plugin;
	
	public EndGameTimer(Main main) {
	plugin = main;
	}
	static String TMinutes;
	static String TSeconds;
	public static int WaitingTime = 0;
	public static GM gmed;
	public static void BeginTimer() {
	WaitingTime = GameManagement.GameTime;
	pgtimer = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
	    @Override  
	    public void run() {
	    	if (WaitingTime > 0){
	    		if (WaitingTime >= 30){
	    			if (WaitingTime == (60 * 5.0) || WaitingTime == (60 * 1.0)){
	    				Message.G(Message.Replacer(MessageMW.TimeLeft, ConvertTimings.convertTime(WaitingTime), "%time"), true);
	    			}
	    		}
	    		
	    		if (WaitingTime % 20 == 0 && (Main.Gamemode != GM.Voting && Main.Gamemode != GM.Elimination)){
	    			teamM.AutoBalanceTeams();
	    		}
	    		
	    		WaitingTime--;
	    	}
	    	else if (WaitingTime == 0){
				Games.CheckUpStatus();
	    		Bukkit.getScheduler().cancelTask(pgtimer);
	    	}
	    }
	}, 20L, 20L);
	}
}
