package kz.PvP.MedievalWars.Methods;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

import kz.PvP.MedievalWars.Main;
import kz.PvP.MedievalWars.utilities.MessageMW;
import kz.PvP.PkzAPI.utilities.Message;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class TimedCommand {
	public static Main plugin;
	static Logger log = Bukkit.getLogger();
	public static HashMap<String, ArrayList<String>> TimedCommands = new HashMap<String, ArrayList<String>>();// Team list

	
	public TimedCommand (Main mainclass){
		plugin = mainclass;
	}
	
	
	
	
	public static void TimedCommands (final Player p, int TimeInSecond, final String Command){
				
		if (!TimedCommands.containsKey(p.getName()))
		TimedCommands.put(p.getName(), new ArrayList<String>());
		
		TimedCommands.get(p.getName()).add(Command);
		
		if (TimeInSecond > 0){
		Message.P(p, Message.Replacer(MessageMW.HaveTimeLeft, ConvertTimings.convertTime(TimeInSecond), "%time"), false);
		
		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				TimedCommands.get(p.getName()).remove(Command);
			}
		}, 20L * TimeInSecond);
		}
	}
	
	
	
	
	
	
	
	
	
}
