package kz.PvP.MedievalWars.Methods;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import kz.PvP.MedievalWars.Main;
import kz.PvP.MedievalWars.Gamemodes.Global;
import kz.PvP.MedievalWars.utilities.Files;
import kz.PvP.MedievalWars.utilities.Kits;

public class LocationsMW {
	
	
	public static Location DeathSpot = null;
	public static Location spectator = null;
	public static Location lobbyMain = null;
	/*
	 * Red Team Locations
	 */
	public static Location redFlag = null;
	public static Location redFlagClaimPt1 = null;
	public static Location redFlagClaimPt2 = null;
	/*
	 * Blue Team Locations
	 */
	public static Location blueFlag = null;
	public static Location blueFlagClaimPt1 = null;
	public static Location blueFlagClaimPt2 = null;
	
	public static Main plugin;

	public LocationsMW(Main mainclass) {
		plugin = mainclass;
		reloadLocations();
	}
	
	
	private void reloadLocations() {
		if (Files.worldteleports.getCustomConfig().contains(Main.WarDisplayname + ".Death.Spawn"))
			DeathSpot = Global.GetLocOfString(Main.WarDisplayname + ".Death.Spawn", Files.worldteleports);
		
		if (Files.worldteleports.getCustomConfig().contains(Main.WarDisplayname + ".Spectators.Spawn"))
			spectator = Global.GetLocOfString(Main.WarDisplayname + ".Spectators.Spawn", Files.worldteleports);
		
		
		if (Files.worldteleports.getCustomConfig().contains("MainLobby"))
			lobbyMain = Global.GetLocOfString("MainLobby", Files.worldteleports);
		/*
		 * Red Team Locations
		 */
		if (Files.worldteleports.getCustomConfig().contains(Main.WarDisplayname + ".Red.Flag"))
			redFlag = Global.GetLocOfString(Main.WarDisplayname + ".Red.Flag", Files.worldteleports);
		
		if (Files.worldteleports.getCustomConfig().contains(Main.WarDisplayname + ".Red.FlagClaim.1"))
			redFlagClaimPt1 = Global.GetLocOfString(Main.WarDisplayname + ".Red.FlagClaim.1", Files.worldteleports);
		
		if (Files.worldteleports.getCustomConfig().contains(Main.WarDisplayname + ".Red.FlagClaim.2"))
			redFlagClaimPt2 = Global.GetLocOfString(Main.WarDisplayname + ".Red.FlagClaim.2", Files.worldteleports);
		
		/*
		 * Blue Team Locations
		 */
		if (Files.worldteleports.getCustomConfig().contains(Main.WarDisplayname + ".Blue.Flag"))
			blueFlag = Global.GetLocOfString(Main.WarDisplayname + ".Blue.Flag", Files.worldteleports);
		
		if (Files.worldteleports.getCustomConfig().contains(Main.WarDisplayname + ".Blue.FlagClaim.1"))
			blueFlagClaimPt1 = Global.GetLocOfString(Main.WarDisplayname + ".Blue.FlagClaim.1", Files.worldteleports);
		
		if (Files.worldteleports.getCustomConfig().contains(Main.WarDisplayname + ".Blue.FlagClaim.2"))
			blueFlagClaimPt2 = Global.GetLocOfString(Main.WarDisplayname + ".Blue.FlagClaim.2", Files.worldteleports);
		
	}


	public static Location randomBlueLoc(){
		Location blueSpawn;
		int num = 1 + ConvertTimings.randomInt(0, Files.worldteleports.getCustomConfig().getConfigurationSection(Main.WarDisplayname + ".Blue.Spawns").getKeys(false).size() - 1);
		
		blueSpawn = Global.GetLocOfString(Main.WarDisplayname + ".Blue.Spawns." + num, Files.worldteleports);
		return blueSpawn;
	}
	
	public static Location randomRedLoc(){
		Location redSpawn;
		int num = 1 + ConvertTimings.randomInt(0, Files.worldteleports.getCustomConfig().getConfigurationSection(Main.WarDisplayname + ".Red.Spawns").getKeys(false).size() - 1);
		
		redSpawn = Global.GetLocOfString(Main.WarDisplayname + ".Red.Spawns." + num, Files.worldteleports);
		return redSpawn;
	}
	public static boolean locationSameAs(Location pointShould, Location pointGot){
		if (pointShould.getBlockX() == pointGot.getBlockX() &&
				pointShould.getBlockY() == pointGot.getBlockY() &&
					pointShould.getBlockZ() == pointGot.getBlockZ())
		return true;
		else
		return false;
	}
	
	
	
	public static boolean locationBetweenPoints(Location loc, Location point1, Location point2){
		if (((loc.getBlockX() >= point1.getBlockX() && loc.getBlockX() <= point2.getBlockX()) || 
			(loc.getBlockX() <= point1.getBlockX() && loc.getBlockX() >= point2.getBlockX())) &&
			((loc.getBlockY() >= point1.getBlockY() && loc.getBlockY() <= point2.getBlockY()) || 
			(loc.getBlockY() <= point1.getBlockY() && loc.getBlockY() >= point2.getBlockY())) &&
			((loc.getBlockZ() >= point1.getBlockZ() && loc.getBlockZ() <= point2.getBlockZ()) || 
			(loc.getBlockZ() <= point1.getBlockZ() && loc.getBlockZ() >= point2.getBlockZ())))
			return true;
		else
			return false;
	}
	
	public static Location getSpawnClass(Player p){
		
		if (teamM.isInBlue(p.getName())){
			if (Kits.KitChoice.get(p) != null && Files.worldteleports.getCustomConfig().contains(Main.WarDisplayname + ".Blue.ClassSpawns." + Kits.KitChoice.get(p).toLowerCase())){
				String kit = Kits.KitChoice.get(p).toLowerCase();
				int num = 1 + ConvertTimings.randomInt(0, Files.worldteleports.getCustomConfig().getConfigurationSection(Main.WarDisplayname + ".Blue.ClassSpawns." + kit).getKeys(false).size() - 1);
				return Global.GetLocOfString(Main.WarDisplayname + ".Blue.ClassSpawns." + kit + "." + num, Files.worldteleports);
			}
			else
				return randomBlueLoc();
		}
		else if (teamM.isInRed(p.getName())){
			if (Kits.KitChoice.get(p) != null && Files.worldteleports.getCustomConfig().contains(Main.WarDisplayname + ".Red.ClassSpawns." + Kits.KitChoice.get(p).toLowerCase())){
				String kit = Kits.KitChoice.get(p).toLowerCase();
				int num = 1 + ConvertTimings.randomInt(0, Files.worldteleports.getCustomConfig().getConfigurationSection(Main.WarDisplayname + ".Red.ClassSpawns." + kit).getKeys(false).size() - 1);
				return Global.GetLocOfString(Main.WarDisplayname + ".Red.ClassSpawns." + kit + "." + num, Files.worldteleports);
			}
			else
				return randomRedLoc();
		}
		else
			return null;
		
	}
	
	
	
}
