package kz.PvP.MedievalWars.Methods;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import kz.PvP.MedievalWars.Games;
import kz.PvP.MedievalWars.Main;
import kz.PvP.MedievalWars.Enums.ChatType;
import kz.PvP.MedievalWars.Enums.Game;
import kz.PvP.MedievalWars.Enums.Team;
import kz.PvP.MedievalWars.Gamemodes.Global;
import kz.PvP.MedievalWars.utilities.Kits;
import kz.PvP.MedievalWars.utilities.MessageMW;
import kz.PvP.PkzAPI.utilities.Message;

import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class teamM{

	
	private static final int SafeTeamDifference = 1;
	
	public static Main plugin;
	public static ArrayList<Player> RedTeam = new ArrayList<Player>();
	public static ArrayList<Player> BlueTeam = new ArrayList<Player>();
	public static ArrayList<Player> Spectators = new ArrayList<Player>();
	public static ArrayList<String> NotAttacked = new ArrayList<String>();
	public static ArrayList<String> Invincibility = new ArrayList<String>();
	public static ArrayList<String> DoNotNotify = new ArrayList<String>();
	public static HashMap<String, ChatType> Chatting = new HashMap<String, ChatType>();// FullTeam Kills	

	
	public teamM(Main mainclass) {
		plugin = mainclass;
	}
	/*
	 * Team Methods
	 */
	public static boolean isInTeam(String p){
		Player pl = getPlayerFromSome(p); 
		if (RedTeam.contains(pl) || BlueTeam.contains(pl))
			return true;
		else
			return false;
		}
	public static boolean isInBlue(String p){ return BlueTeam.contains(getPlayerFromSome(p));}
	public static boolean isInRed(String p){ return RedTeam.contains(getPlayerFromSome(p));}
	public static boolean isInSpec(String p){ return Spectators.contains(getPlayerFromSome(p));}
	public static ArrayList<Player> getTeam(Team team){ if (team == Team.Blue) return BlueTeam; else if (team == Team.Red) return RedTeam; else return Spectators;}
	public static void joinTeam(Team team, String pname){ 
		Player p = getPlayerFromSome(pname);
		if (team == Team.Blue)
			BlueTeam.add(p); 
		else if (team == Team.Red)
			RedTeam.add(p);
		else 
			Spectators.add(p);
		}
	/*
	 * Team methods End
	 */
	
	
	/*
	 * Team joining and leaving?
	 */
	public static Team assignTeam (String pname, Team team){
		Player p = getPlayerFromSome(pname);
		
		if (p.getVehicle() != null)
			p.eject();
		  
		  
		if (teamM.getTeam(Team.Red).contains(p))
		teamM.getTeam(Team.Red).remove(p);
		if (teamM.getTeam(Team.Blue).contains(p))
		teamM.getTeam(Team.Blue).remove(p);
		if (teamM.getTeam(Team.Spectator).contains(p))
		teamM.getTeam(Team.Spectator).remove(p);
		
		
		if (team == Team.Random){
			if (getTeamSize(Team.Red) <= (getTeamSize(Team.Blue) + SafeTeamDifference))
				joinTeam(Team.Red, pname);
				// Red Team has more than required  |  Join Blue
			else if (getTeamSize(Team.Blue) <= (getTeamSize(Team.Red) + SafeTeamDifference))
				joinTeam(Team.Blue, pname);
				// Blue Team has more than required  | Join Red 
		}
		else if (team == Team.Red){
			if (getTeamSize(Team.Red) <= getTeamSize(Team.Blue) + SafeTeamDifference)
				joinTeam(Team.Red, pname);
			else
				Message.P(p, Message.Replacer(MessageMW.TeamHasTooMany, team.toString(), "%team"), true);
		}
		else if (team == Team.Blue){
			if (getTeamSize(Team.Blue) <= getTeamSize(Team.Red) + SafeTeamDifference)
				joinTeam(Team.Blue, pname);
			else
				Message.P(p, Message.Replacer(MessageMW.TeamHasTooMany, team.toString(), "%team"), true);
		}
		else if (team == Team.Spectator){
			if (Main.GameStatus == Game.GAME)
			Message.P(p, MessageMW.GameBegunSpec, false);
			
			joinTeam(Team.Spectator, p.getName());	
			
			if (Main.GameStatus != Game.PREGAME)
				p.teleport(LocationsMW.spectator);
		}
		
		p.setHealth(20.0);
		p.setGameMode(GameMode.SURVIVAL);
		p.setFoodLevel(20);
		p.setExp(0);
		Global.clearInventory(p, true);
		Global.GiveTools(p, getTeam(p.getName()));
		
		
		
		if (Main.GameStatus == Game.GAME && teamM.isInTeam(p.getName()))
			try {
				Start(p, true);
			} catch (SQLException e) { e.printStackTrace(); }
		
		
		adjustDisplayName(p);
		return team;
	}
	
	
	
	
	public static void playSoundTeam(Team team, Sound sound){
		for (Player pl : teamM.getTeam(team))
			pl.playSound(pl.getLocation(), sound, 1f, 1f);
	}
	public static void playEffectTeam(Team team, Effect effect){
		for (Player pl : teamM.getTeam(team))
			pl.playEffect(pl.getLocation(), effect, 2f);
	}
	
	
	
	
	
	public static void Start(final Player p, boolean TpToSpawn) throws SQLException {
		
		teamM.adjustVisibility(p);

		if (!Kits.KitChoice.containsKey(p) && !DoNotNotify.contains(p.getName()))
		Message.P(p, MessageMW.DoNotNotify, false);
				
		//Invincibility.remove(p.getName());
		NotAttacked.add(p.getName());
		Invincibility.add(p.getName());
		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				Invincibility.remove(p.getName());
			}
		}, 20L * 10);
		
		
		if (TpToSpawn)
			p.teleport(LocationsMW.getSpawnClass(p));
		
		
		p.setFlying(false);
		p.setAllowFlight(false);
		p.setHealth(20.0);
		p.setFoodLevel(20);

		Kits.GiveKit(p);
		
		ProtocolLib.updateTabAll();
		
		
		
		
		}
	
	
	public static Team getTeam(String p){
		Team team = null;
		if (isInBlue(p))
		team = Team.Blue;
		else if (isInRed(p))
		team = Team.Red;
		else if (isInSpec(p))
		team = Team.Spectator;
		else
		team = Team.None;
		return team;
	}
	
	
	
	public static void AutoBalanceTeams() {
		// We make a check to see if teams are balanced. If not then we will auto-balance them.
		if (getTeamSize(Team.Red) - (SafeTeamDifference + 1) >= getTeamSize(Team.Blue)){//Red team is stacked
			Player player = getTeam(Team.Red).get(getTeamSize(Team.Red) - 1);
			Global.DeleteFromArrays(player, 'F');
			Message.P(player, MessageMW.SwappedDueToBalance, true);
			assignTeam(player.getName(), Team.Blue);
		}
		else if (getTeamSize(Team.Blue) - (SafeTeamDifference + 1) >= getTeamSize(Team.Red)){//Blue team is stacked
			Player player = getTeam(Team.Blue).get(getTeamSize(Team.Blue) - 1);
			Global.DeleteFromArrays(player, 'F');
			Message.P(player, MessageMW.SwappedDueToBalance, true);
			assignTeam(player.getName(), Team.Red);
		}
	}
	
	
	
	
	
	
	public static Player getPlayerFromSome(String partname) {
		Player target = null;
		// We do 3 searches... We will prioritize the Gamers first, and if we don't find any gamers, then we search others...
		target = plugin.getServer().getPlayerExact(partname);
		
		if (target == null)
		for (Player p : plugin.getServer().getOnlinePlayers()){
			if (p.getName().toLowerCase().contains(partname.toLowerCase())){
				target = p;
				return target;
			}
		}
		return target;
	}
	public static void adjustVisibility(Player p) {
		for (Player pl : getTeam(Team.Spectator)){
			if (!pl.canSee(p))
			pl.showPlayer(p);
			if (p.canSee(pl))
			p.hidePlayer(pl);
		}
		for (Player pl : getTeam(Team.Red)){
			if (!pl.canSee(p))
			pl.showPlayer(p);
			if (!p.canSee(pl))
			p.showPlayer(pl);
		}
		for (Player pl : getTeam(Team.Blue)){
			if (!pl.canSee(p))
			pl.showPlayer(p);
			if (!p.canSee(pl))
			p.showPlayer(pl);
		}
	}
	public static void adjustDisplayName(Player p) {
		if (isInRed(p.getName()))
			p.setDisplayName(ChatColor.RED + p.getName() + ChatColor.GRAY);
		else if (isInBlue(p.getName()))
			p.setDisplayName(ChatColor.AQUA + p.getName() + ChatColor.GRAY);
		else if (isInSpec(p.getName()))
			p.setDisplayName(ChatColor.GRAY + p.getName());
	}
	public static Integer getTeamSize(Team Team) {
		return getTeam(Team).size();
	}
	
	
	
	
	
}
