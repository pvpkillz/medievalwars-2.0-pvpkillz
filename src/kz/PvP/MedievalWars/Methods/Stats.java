package kz.PvP.MedievalWars.Methods;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

import kz.PvP.MedievalWars.Games;
import kz.PvP.MedievalWars.Main;
import kz.PvP.MedievalWars.InfoSQL;
import kz.PvP.MedievalWars.Enums.GM;
import kz.PvP.PkzAPI.utilities.Mysql;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

public class Stats implements Listener{
	
	public static Main plugin;
	static Logger log = Bukkit.getLogger();
	public static HashMap<String, Integer> TotalKills = new HashMap<String, Integer>();// Total Kills of a player - Polled from DB
	public static HashMap<String, Integer> RoundKills = new HashMap<String, Integer>();// Total Kills of a player - During round
	public static HashMap<String, Integer> RoundDeaths = new HashMap<String, Integer>();// Total Deaths of a player - During round
	public static HashMap<String, Integer> TotalDeaths = new HashMap<String, Integer>();// Total Deaths of a player - Polled from DB
	public static HashMap<String, Double> DamageDone = new HashMap<String, Double>();// Total Damage Done - During round
	public static HashMap<String, ArrayList<String>> Assists = new HashMap<String, ArrayList<String>>();// Total Assists during game

	
	public Stats (Main mainclass){
		plugin = mainclass;
	}
	
	/*  All add Methods */

	public static void earnedAKill(Player p){
		if (!RoundKills.containsKey(p.getName()))
			RoundKills.put(p.getName(), 0);
		
		RoundKills.put(p.getName(), getKills(p.getName()) + 1);
		
		if (teamM.isInBlue(p.getName()))
			teamC.getBlue().totalKills++;
		else if (teamM.isInRed(p.getName()))
			teamC.getRed().totalKills++;
		Games.CheckUpStatus();
	}
	
	public static void addDamage(Player p, double dmg){
		if (!DamageDone.containsKey(p.getName()))
			DamageDone.put(p.getName(), 0.0);
			
		double Damage = DamageDone.get(p.getName());
		DamageDone.put(p.getName(), Damage + dmg);
		
		
	}
	public static void addToAssist(Player dam, Player def){
		if (!Assists.containsKey(dam.getName()))
			Assists.put(dam.getName(), new ArrayList<String>());
			
		
		if (!Assists.get(dam.getName()).contains(def.getName()))
			Assists.get(dam.getName()).add(def.getName());
	}
	
	
	public static ArrayList<String> getAssists(Player p){
		if (!Assists.containsKey(p.getName()))
			Assists.put(p.getName(), new ArrayList<String>());
			
			ArrayList<String> Assist = Assists.get(p.getName());
		
		return Assist;
	}
	public static double getDamage(String p){
		if (!DamageDone.containsKey(p))
			DamageDone.put(p, 0.0);
			
			double Damage = DamageDone.get(p);
			return Damage;
	}

	public static int getKills(String p){
		if (!RoundKills.containsKey(p))
			RoundKills.put(p, 0);
			return RoundKills.get(p);
	}
	
	
	public static int getDeaths(String p){
		if (!RoundDeaths.containsKey(p))
			RoundDeaths.put(p, 0);
			return RoundDeaths.get(p);
	}
	
	
	
	
	/*
	 * MySQL Functions below... For agthering data from DB... :D
	 */
	
	
	
	public static int getTotalKills(String p) throws SQLException{
		if (!TotalKills.containsKey(p)){
			int Kills = 0;
			
			ResultSet rs = Mysql.PS.getSecureQuery("SELECT *, COUNT(*) AS Kills FROM DEATHS WHERE Killer = ? GROUP BY Killer", p);

			Kills = rs.getInt("Kills");
			TotalKills.put(p, Kills);
		}
		
		
		int Kills = 0;
		Kills = TotalKills.get(p) + getKills(p);
		return Kills;
	}
	public static int getTotalCaptures(String pname){
		int captures = 0;
		try {
			ResultSet rs = Mysql.PS.getSecureQuery("SELECT * FROM UserInfo WHERE User = ?", ""+Mysql.GetUserID(pname));
			if (rs.next())
				captures =  rs.getInt("FlagCaptures");
		} catch (SQLException e) { e.printStackTrace(); }
		
		return captures;
	}
	public static void addFlagCapture(String pname){
		Player p = teamM.getPlayerFromSome(pname);
		
		ResultSet rs;
		try {
			rs = Mysql.PS.getSecureQuery("SELECT * FROM UserInfo WHERE User = ?", ""+Mysql.GetUserID(p.getName()));
			
			if (rs.next())
				Mysql.PS.getSecureQuery("UPDATE UserInfo SET FlagCaptures = ? WHERE User = ?", ""+rs.getInt("FlagCaptures") + 1, ""+Mysql.GetUserID(p.getName()));
			
			if (Main.Gamemode == GM.CTF){
				InfoSQL.modifyUserPts(p.getName(), GameManagement.CTFPointsPerCap, true);
				GameManagement.teamInfo.get(teamM.getTeam(pname)).addCaptures(1);
			}				
			else if (Main.Gamemode == GM.Mixed){
				InfoSQL.modifyUserPts(p.getName(), GameManagement.MixedPointsPerCap, true);
				GameManagement.teamInfo.get(teamM.getTeam(pname)).addCaptures(1);
				GameManagement.teamInfo.get(teamM.getTeam(pname)).addScore(GameManagement.MixedScorePerCap);
			}
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
		
	public static int getTotalDeaths(String p){
			if (!TotalDeaths.containsKey(p)){
				int Deaths = 0;
				try {
					ResultSet rs = Mysql.PS.getSecureQuery("SELECT *, COUNT(*) AS Deaths FROM DEATHS WHERE Victim = ? GROUP BY Victim", ""+Mysql.GetUserID(p));
					Deaths = rs.getInt("Deaths");
				} catch (SQLException e) {
					e.printStackTrace();
				}
				TotalKills.put(p, Deaths);
			}
			int Deaths = 0;
			Deaths = TotalDeaths.get(p) + getDeaths(p);
			return Deaths;
		}

	public static String getChatRank(String pname, boolean Brackets) throws SQLException {
		int Kills = getTotalKills(pname);
		
		String P = "";
		if (Kills <= 100)
			P = "Peasant";
		else if (Kills <= 250)
			P = "Lionheart";
		else if (Kills <= 650)
			P = "Squire";
		else if (Kills <= 875)
			P = "Crusader";
		else if (Kills <= 1200)
			P = "Baron";
		else if (Kills <= 1600)
			P = "Count";
		else if (Kills <= 2200)
			P = "Desperado";
		else if (Kills <= 3200)
			P = "War-Chief";
		else if (Kills <= 4000)
			P = "Overlord";
		else if (Kills <= 6000)
			P = "Bandito";
		else if (Kills <= 8000)
			P = "Justiciar";
		else if (Kills <= 10000)
			P = "Archon";
		else if (Kills <= 13000)
			P = "Emperor";
		else if (Kills <= 17000)
			P = "Wunderkind";
		else if (Kills <= 20000)
			P = "The Amazing";
		
		
		String Name = "";
		
		if (Brackets)
		Name = ChatColor.GRAY + "(" + ChatColor.GOLD  + P + ChatColor.GRAY + ") ";
		else
		Name =  ChatColor.GOLD  + P + ChatColor.GRAY;
		
		return Name;
	}

	public static void earnedADeath(Player p) {
		if (!RoundDeaths.containsKey(p.getName()))
			RoundDeaths.put(p.getName(), 0);
		
		RoundDeaths.put(p.getName(), getDeaths(p.getName()) + 1);
	}
	
	
	
	
	
}

