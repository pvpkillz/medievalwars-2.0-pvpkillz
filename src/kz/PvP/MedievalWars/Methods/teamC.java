package kz.PvP.MedievalWars.Methods;

import kz.PvP.MedievalWars.Main;
import kz.PvP.MedievalWars.Enums.Team;
import kz.PvP.MedievalWars.Enums.TeamInfo;

public class teamC {
	public static Main plugin;

	public teamC(Main mainclass) {
		plugin = mainclass;
	}
	
	
	
	public static TeamInfo getRed(){
		return GameManagement.teamInfo.get(Team.Red);
	}
	
	public static TeamInfo getBlue(){
		return GameManagement.teamInfo.get(Team.Blue);
	}
	
}
