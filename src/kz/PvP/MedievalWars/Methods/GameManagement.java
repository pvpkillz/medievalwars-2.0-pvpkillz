package kz.PvP.MedievalWars.Methods;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.logging.Logger;

import kz.PvP.MedievalWars.Games;
import kz.PvP.MedievalWars.Main;
import kz.PvP.MedievalWars.Vote;
import kz.PvP.MedievalWars.Commands.PlayerCommands;
import kz.PvP.MedievalWars.Enums.Game;
import kz.PvP.MedievalWars.Enums.GM;
import kz.PvP.MedievalWars.Enums.Team;
import kz.PvP.MedievalWars.Enums.TeamInfo;
import kz.PvP.MedievalWars.Events.DeathListener;
import kz.PvP.MedievalWars.Gamemodes.CTF;
import kz.PvP.MedievalWars.Gamemodes.Global;
import kz.PvP.MedievalWars.Timers.EndGameTimer;
import kz.PvP.MedievalWars.Timers.PreGameTimer;
import kz.PvP.MedievalWars.utilities.Files;
import kz.PvP.MedievalWars.utilities.MessageMW;
import kz.PvP.PkzAPI.utilities.Message;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

public class GameManagement {

	public static Main plugin;
	static Logger log = Bukkit.getLogger();
	
	public GameManagement (Main mainclass){
		plugin = mainclass;
		setupTeams();
	}
	
	private static void setupTeams() {
		teamInfo.clear();
		teamInfo.put(Team.Red, new TeamInfo());
		teamInfo.put(Team.Blue, new TeamInfo());
		
		if (Files.worldteleports.getCustomConfig().contains(Main.WarDisplayname + ".Red.Core.2"))
			teamC.getRed().coreLoc2 = Global.GetLocOfString(Main.WarDisplayname + ".Red.Core.2", Files.worldteleports);
		
		if (Files.worldteleports.getCustomConfig().contains(Main.WarDisplayname + ".Red.Core.1"))
			teamC.getRed().coreLoc1 = Global.GetLocOfString(Main.WarDisplayname + ".Red.Core.1", Files.worldteleports);
		
		if (Files.worldteleports.getCustomConfig().contains(Main.WarDisplayname + ".Blue.Core.2"))
			teamC.getBlue().coreLoc2 = Global.GetLocOfString(Main.WarDisplayname + ".Blue.Core.2", Files.worldteleports);
		
		if (Files.worldteleports.getCustomConfig().contains(Main.WarDisplayname + ".Blue.Core.1"))
			teamC.getBlue().coreLoc1 = Global.GetLocOfString(Main.WarDisplayname + ".Blue.Core.1", Files.worldteleports);	
	}

	/*
	 * Team Deathmatch
	 */
	public static int TDMRoundLength = 0;
	public static int TDMPointsPerKill = 0;
	public static int TDMKillsReq = 75;
	public static String TDMDescription;
	/*
	 * Capture The Flag
	 */
	public static int CTFRoundLength = 0;
	public static int CTFCapturesNeeded = 0;
	public static int CTFPointsPerCap = 0;
	public static int CTFPointsPerKill = 0;
	public static String CTFDescription;
	/*
	 * Mixed Gamemode
	 */
	public static int MixedRoundLength = 0;
	public static int MixedScoreReq = 0;
	public static int MixedScorePerCap = 0;
	public static int MixedScorePerKill = 0;
	public static int MixedPointsPerCap = 0;
	public static int MixedPointsPerKill = 0;
	public static String MixedDescription;
	/*
	 * Elimination
	 */
	public static int EliminationRoundLength = 0;
	public static String EliminationDescription;
	public static int EliminationPlayersNeeded = 0;
	
	/*
	 * Destroy the Core
	 */
	public static final int CoreHP = 200;
	public static final int CoreHPDmg = 30;
	
	/*
	 * General Settings
	 */
	public static int FlagDespawnTime = 0;
	public static int PlayersStackFix = 0;
	public static int IdleMaxTime = 0;
	public static int MinPlayersNeeded = 0;
	public static int PregameWaiting = 60;
	
	
	
	public static HashMap<Team, TeamInfo> teamInfo = new HashMap<Team, TeamInfo>();// FullTeam Kills
	public static HashMap<String, Location> IdleUsers = new HashMap<String, Location>();// FullTeam Kills
	
	
	
	
	// Strings Must be dynamic
	public static String gamemode;
	public static String objectives;
	public static int GameTime = 0;
	public static int Gamesrun = 0;
	
	
	
	public static void BeginGame() throws SQLException {
		if (Main.World == null)
		Main.World = plugin.getServer().getWorld(Main.WarWorldname);
		Gamesrun++;
		Games.RefreshTags();
		setupTeams();
		
		Global.HorseNames.clear();
		Global.HorseNames.addAll(Files.config.getCustomConfig().getStringList("HorseNames"));
		
		
		
		
		
		
		
		
		
		ProtocolLib.updateTabAll();
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
			public void run(){
				Global.SpawnAllHorses();
			}
		}, 20 * 10);
		
		
		Main.GameStatus = Game.GAME;
		Main.Gamemode = GM.Mixed;
		Scoreboards.Boards.clear();
		Scoreboards.Objectives.clear();
		int previousTopRec = 0;
		for(Entry<GM, ArrayList<String>> entry : Vote.TotalVotes.entrySet()){
			if (entry.getValue().size() > previousTopRec){
				Main.Gamemode = entry.getKey();
				previousTopRec = entry.getValue().size();
			}
		}
		Vote.TotalVotes.clear();
		
		
		if (Main.Gamemode == GM.Elimination){
			for (Player pl : Bukkit.getOnlinePlayers()){
				if (!teamM.isInTeam(pl.getName()) || teamM.isInSpec(pl.getName())){
					teamM.assignTeam(pl.getName(), Team.Random);
					PlayerCommands.OpenClassMenu(pl);
				}
			}
		}
		
		
		if (Main.Gamemode == GM.CTF){
			gamemode = "Capture the flag (CTF)";
			objectives = CTFDescription;
			GameTime = CTFRoundLength * 60;
		}
		else if (Main.Gamemode == GM.Mixed){
			gamemode = "Mixed (CTF + TDM)";
			objectives = MixedDescription;
			GameTime = MixedRoundLength * 60;
		}
		else if (Main.Gamemode == GM.TDM){
			gamemode = "Team Deathmatch (TDM)";
			objectives = TDMDescription;
			GameTime = TDMRoundLength * 60;
		}
		else if (Main.Gamemode == GM.Elimination){
			gamemode = "Team Elimination (Last team standing)";
			objectives = EliminationDescription;
			GameTime = EliminationRoundLength * 60;
			}
		
		EndGameTimer.BeginTimer();
		
		
		Main.World.setTime(0);
		for (final Player p : plugin.getServer().getOnlinePlayers()){
		     if (teamM.isInTeam(p.getName())){
				teamM.Start(p, true);
		     }
		}
		
		if(Main.Gamemode == GM.Mixed || Main.Gamemode == GM.CTF){
		/* Flag set-up  */
		CTF.SpawnFlag(LocationsMW.redFlag, Team.Red);
		CTF.SpawnFlag(LocationsMW.blueFlag, Team.Blue);
		/*  END OF Flag Set-up  */
		}
		Message.G(MessageMW.PrepareTimer, true);
	}



	public static void endGame(Team team) {
		
		Bukkit.getScheduler().cancelTask(EndGameTimer.pgtimer);
		
		
		
		PreGameTimer.BeginTimer();
		final Location LobbyLoc = LocationsMW.lobbyMain;

		for (Player p : Bukkit.getOnlinePlayers()){
			p.setDisplayName(ChatColor.GRAY + p.getName());
			if (p.getVehicle() != null)
			p.getVehicle().eject();
			p.teleport(LobbyLoc);
			p.setHealth(20.0);
			p.setFoodLevel(20);
			p.setExp(0);
			p.setGameMode(GameMode.SURVIVAL);
			p.setAllowFlight(true);
			Global.clearInventory(p, true);
			Global.GiveTools(p, kz.PvP.MedievalWars.Enums.Team.Spectator);
		}
		
		
		
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				for (Player pl : Bukkit.getServer().getOnlinePlayers())
					Vote.ShowVoteMenu(pl);
				for (Entry<String, BukkitTask>  task: DeathListener.BT.entrySet())
					task.getValue().cancel();
				}
			}, 20L * 1);
		
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				ChangeMaps.LoadMapSave(Main.WarDisplayname);
				}
			}, 20L * 7);
		
		
		Main.Gamemode = GM.Voting;
		Main.GameStatus = Game.PREGAME;
		
		
		Scoreboards.Boards.clear();
		Scoreboards.Objectives.clear();
		Vote.TotalVotes.clear();
		
		for(Player p : plugin.getServer().getOnlinePlayers())
		teamM.adjustVisibility(p);
		
	}
	
}
