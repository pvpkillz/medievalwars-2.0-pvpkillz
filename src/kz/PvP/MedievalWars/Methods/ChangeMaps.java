package kz.PvP.MedievalWars.Methods;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

import kz.PvP.MedievalWars.Main;
import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;



public class ChangeMaps implements Listener {

	public static Main plugin;
	static Logger log = Bukkit.getLogger();

	public ChangeMaps (Main mainclass){
		plugin = mainclass;
	}
	
	
    public static void unloadMap(String mapname){
    	
        if(plugin.getServer().unloadWorld(mapname, false)){
            plugin.getLogger().info("Successfully unloaded " + mapname);
        }else{
            plugin.getLogger().severe("COULD NOT UNLOAD " + mapname);
        }
    }
    //Loading maps (MUST BE CALLED AFTER UNLOAD MAPS TO FINISH THE ROLLBACK PROCESS)
    
	private static void copyDirectory(File sourceLocation, File targetLocation)
			throws IOException {

		if (sourceLocation.isDirectory()) {
			if (!targetLocation.exists()) {
				targetLocation.mkdir();
			}

			String[] children = sourceLocation.list();
			for (int i = 0; i < children.length; i++) {
				copyDirectory(new File(sourceLocation, children[i]), new File(
						targetLocation, children[i]));
			}
		} else {

			InputStream in = new FileInputStream(sourceLocation);
			OutputStream out = new FileOutputStream(targetLocation);
			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			in.close();
			out.close();
		}
	}

	public static void copy(InputStream in, File file) {
		try {
			OutputStream out = new FileOutputStream(file);
			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			out.close();
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static boolean deleteDirectory(File path) {
        if( path.exists() ) {
            File files[] = path.listFiles();
            for(int i=0; i<files.length; i++) {
                if(files[i].isDirectory()) {
            deleteDirectory(files[i]);
            }
                else {
                    files[i].delete();
                } //end else
            }
            }
        return( path.delete() );
        }
	
	
	
	
    
	

	public static void BrandNewWorld(final String worldName) {
	     Runnable map = new Runnable() {
	         public void run() {
				World newWorld = Bukkit.createWorld(new WorldCreator(worldName).type(WorldType.NORMAL));
			    newWorld.setDifficulty(Difficulty.NORMAL);
			    newWorld.setAutoSave(false);
			   	newWorld.setFullTime(0);
			   	newWorld.setKeepSpawnInMemory(true);
			   	newWorld.setPVP(true);
			   	newWorld.setStorm(false);
			   	newWorld.setThundering(false);
			    Main.World = newWorld;
			    newWorld.loadChunk(newWorld.getChunkAt(newWorld.getSpawnLocation()));
			    System.out.println("Created new world");
	         }
	     };
	     ExecutorService executor = Executors.newCachedThreadPool();
	     executor.submit(map);
	}
	
	
	public static void LoadMapSave(final String mapname){
		System.out.println("Deleting old world.");
		
		
		
		final Location LobbyLoc = LocationsMW.lobbyMain;

    	
    	World world = plugin.getServer().getWorld(Main.WarWorldname);
    	
    	
    	if (world != null){
        	for (Player p: world.getPlayers())
        		p.teleport(LobbyLoc);
        	
    		for ( Entity e : world.getEntities()){
    			if (e.getType() != EntityType.PLAYER)
    			e.remove();
    		}
	        if(plugin.getServer().unloadWorld(Main.WarWorldname, false)){
	            plugin.getLogger().info(Main.WarWorldname + " Unloaded.");
	            deleteDirectory(world.getWorldFolder());
	            LoadMapSave(mapname);
			}
	        else
	        plugin.getLogger().severe("Map cannot be unloaded " + mapname);
    	}
    	else {
            plugin.getLogger().info("Copying saved world. (War)");
			try {
				copyDirectory(new File(plugin.getDataFolder(), mapname),
						new File(Main.WarWorldname));
			} catch (IOException e) {
				plugin.getLogger().warning("Error: " + e.toString());
			}
			BrandNewWorld(Main.WarWorldname);
			Main.WarDisplayname = mapname;
    	}
    	
    	
    	
	}
	
	
	
	public static Location getRandomLocation(){
		Location startFrom = Main.World.getSpawnLocation();
		Location loc;
		loc = startFrom.clone();
		int newY = Main.World.getHighestBlockYAt(loc);
		loc.setY(newY);
		return loc;
	}
	
	
}
