package kz.PvP.MedievalWars.Methods;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.block.BlockPlaceEvent;

import kz.PvP.MedievalWars.Main;
import kz.PvP.MedievalWars.Commands.GameCommands;
import kz.PvP.MedievalWars.Enums.Cannon;
import kz.PvP.MedievalWars.Gamemodes.Global;
import kz.PvP.MedievalWars.utilities.Files;
import kz.PvP.MedievalWars.utilities.MessageMW;
import kz.PvP.PkzAPI.utilities.Message;

public class Cannons{
	public static Main plugin;
	public static ArrayList<String> PlacingCannons = new ArrayList<String>();
	public static HashMap<Block, Cannon> Cannons = new HashMap<Block, Cannon>();

	public Cannons(Main mainclass) {
		plugin = mainclass;
		/*
		for (String world : Files.worlds.getCustomConfig().getStringList("Worlds")){
			if (Files.worldteleports.getCustomConfig().contains(world + ".Cannons"))
			for (String cannons : Files.worldteleports.getCustomConfig().getConfigurationSection(world + ".Cannons").getKeys(false)){
				Location loc = Global.GetLocOfString(Main.WarDisplayname + ".Cannons." + cannons, Files.worldteleports);
				Cannons.add(loc.getBlock());
			}
		}
		*/
	}
	
	
	
	@SuppressWarnings("deprecation")
	public static boolean IsACannon(Player p, Block clickedBlock) {
			int count = 0;
			Block middleBlock=null;
			Block tipBlock=null;
			if (Cannons.containsKey(clickedBlock))
				return true;
				else
			for (int x = -2; x <= 2; x++){
				for (int z = -2; z <= 2; z++){
					if (clickedBlock.getLocation().add(x, 0, z).getBlock().getType() == Material.WOOL){
						if (clickedBlock.getLocation().add(x, 0, z).getBlock().getData() == 15){
							count++;
							
							if (count == 1)
								middleBlock = clickedBlock.getLocation().add(x, 0, z).getBlock();
							else if (count == 2)
								tipBlock = clickedBlock.getLocation().add(x, 0, z).getBlock();
								
								
							if (Cannons.containsKey(clickedBlock) && count == 2)
								return true;
							else if (count == 2){
								registerNewCannon(p, tipBlock, clickedBlock,middleBlock);
								return true;
							}
						}
					}
				}
			}
		return false;
	}



	public static void LaunchProjectile(Player p, Block clickedBlock) {
		if (IsACannon(p, clickedBlock)){

			Projectile proj = clickedBlock.getWorld().spawn(Cannons.get(clickedBlock).getTip().getLocation(), Fireball.class);
			proj.setVelocity(Cannons.get(clickedBlock).getTip().getLocation().getDirection().multiply(-3));
			Message.G("Launched cannon", true);
		}
	}
	


	public static boolean isPlacingCannons(Player player) {
		return PlacingCannons.contains(player.getName());
	}



	public static void registerNewCannon(Player p, Block Tip, Block Button, Block Middle) {
			Message.P(p, "Created new cannon", true);
			
			GameCommands.saveLocations(p, Tip.getLocation(), "Map.Cannons.Tip", Files.worldteleports, true);
			GameCommands.saveLocations(p, Button.getLocation(), "Map.Cannons.LaunchButton", Files.worldteleports, true);
			GameCommands.saveLocations(p, Middle.getLocation(), "Map.Cannons.MiddleBlock", Files.worldteleports, true);
			Cannons.put(Button, new Cannon(Tip, Middle, Button));
	}



	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
