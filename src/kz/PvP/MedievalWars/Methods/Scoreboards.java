package kz.PvP.MedievalWars.Methods;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import kz.PvP.MedievalWars.Main;
import kz.PvP.MedievalWars.Vote;
import kz.PvP.MedievalWars.Enums.Game;
import kz.PvP.MedievalWars.Enums.GM;
import kz.PvP.MedievalWars.Enums.Team;
import kz.PvP.MedievalWars.utilities.MessageMW;
import kz.PvP.PkzAPI.utilities.Message;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Scoreboard;


public class Scoreboards{

	
	public static HashMap<String, Objective> Objectives = new HashMap<String, Objective>();
	public static HashMap<String, Scoreboard> Boards = new HashMap<String, Scoreboard>();
	
	
	static Scoreboard board;
	static ScoreboardManager manager;
	public static Objective objective;
	
	public static Main plugin;

	public Scoreboards(Main mainclass) {
		plugin = mainclass;
	}
	
	static public void ScoreBoard(Player player){
		if ((!Boards.containsKey(player.getName())) || (!Objectives.containsKey(player.getName()))){
		manager = Bukkit.getScoreboardManager();
		board = manager.getNewScoreboard();
		objective = board.registerNewObjective("test", "dummy");
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);

		if (!Objectives.containsKey(player.getName()))
		Objectives.put(player.getName(), objective);
		
		
		if (!Boards.containsKey(player.getName()))
		Boards.put(player.getName(), board);
		}
		
		objective = Objectives.get(player.getName());
		board = Boards.get(player.getName());
		
		if (Main.GameStatus == Game.PREGAME){
			// Vote info
			
			objective.setDisplayName(ChatColor.GREEN + "Gamemode Vote");
			for(Entry<GM, ArrayList<String>> entry : Vote.TotalVotes.entrySet()){
				GiveStat(player, objective, entry.getValue().size(), ChatColor.GRAY + entry.getKey().toString());
			}
		}
		else{
		
		
		GiveStat(player, objective, teamM.getTeamSize(Team.Red), "&cRed Team");
		GiveStat(player, objective, teamM.getTeamSize(Team.Blue), "&bBlue Team");
			
		/*
		if (red.contains(player) && (Games.RedKills.containsKey(player.getName())))
		GiveStat(player, objective, Games.RedKills.get(player.getName()), "Kills", ChatColor.RED);
		else if (blue.contains(player) && (Games.BlueKills.containsKey(player.getName())))
		GiveStat(player, objective, Games.BlueKills.get(player.getName()), "Kills", ChatColor.AQUA);
		*/
			if (Main.Gamemode == GM.TDM){
				GiveStat(player, objective, GameManagement.TDMKillsReq, "&6Goal");
				GiveStat(player, objective, teamC.getBlue().getTotalKills(), "&bBlue Kills");
				GiveStat(player, objective, teamC.getRed().getTotalKills(), "&cRed Kills");
			}	
			else if (Main.Gamemode == GM.CTF){
				GiveStat(player, objective, GameManagement.CTFCapturesNeeded, "&6Captures");
				GiveStat(player, objective, teamC.getBlue().getCaptures(), "&bBlue Caps");
				GiveStat(player, objective, teamC.getRed().getCaptures(), "&cRed Caps");
			}
			else if (Main.Gamemode == GM.Mixed){
				GiveStat(player, objective, GameManagement.MixedScoreReq, "&6Goal");
				GiveStat(player, objective, teamC.getBlue().getScore(), "&bBlue Score");
				GiveStat(player, objective, teamC.getRed().getScore(), "&cRed Score");
			}
		}
		player.setScoreboard(board);
	}
	public static void GiveStat(Player player, Objective objective, Integer integer, String string) {
		  Score score = objective.getScore(Bukkit.getOfflinePlayer(Message.Colorize(string))); //Get a fake offline player
		  score.setScore(integer); //Integer only!
	}
	
}
