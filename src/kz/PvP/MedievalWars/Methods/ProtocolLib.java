package kz.PvP.MedievalWars.Methods;

import java.util.logging.Logger;

import kz.PvP.MedievalWars.Main;
import kz.PvP.MedievalWars.Enums.Team;
import kz.PvP.MedievalWars.utilities.MessageMW;
import kz.PvP.PkzAPI.utilities.Message;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.kitteh.tag.PlayerReceiveNameTagEvent;
import org.mcsg.double0negative.tabapi.TabAPI;

import com.comphenix.protocol.Packets;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.ConnectionSide;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.utility.MinecraftReflection;
import com.comphenix.protocol.wrappers.nbt.NbtCompound;
import com.comphenix.protocol.wrappers.nbt.NbtFactory;

public class ProtocolLib implements Listener{
	public static Main plugin;
	static Logger log = Bukkit.getLogger();

	public ProtocolLib(Main mainclass) {
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
		setupProtocolLib();
	}

	private void setupProtocolLib() {
		EnableProtocolLibFunctions();
	}
	
	// We enable ProtocolLib's API for use with fake enchanted items
    private void EnableProtocolLibFunctions() {
		if (Main.ProtocolLibEnable){
			ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(
					plugin, ConnectionSide.SERVER_SIDE, ListenerPriority.HIGH, 
					Packets.Server.SET_SLOT, Packets.Server.WINDOW_ITEMS) {
				@Override
				public void onPacketSending(PacketEvent event) {
					if (event.getPacketID() == Packets.Server.SET_SLOT) {
						addGlow(new ItemStack[] { event.getPacket().getItemModifier().read(0) });
					} else {
						addGlow(event.getPacket().getItemArrayModifier().read(0));
					}
				}
			});
		}
	}
	public static void addGlow(ItemStack[] selector) {
		if (Main.ProtocolLibEnable){
			for (ItemStack stack : selector) {
				if (stack != null) {
					// Only update those stacks that have our flag enchantment
					if (stack.getEnchantmentLevel(Enchantment.SILK_TOUCH) == 32) {
						NbtCompound compound = (NbtCompound) NbtFactory.fromItemTag(stack);
						compound.put(NbtFactory.ofList("ench"));
					}
				}
			}
		}
	}

	public static ItemStack setGlowing(ItemStack is) {
		is = MinecraftReflection.getBukkitItemStack(is);
		is.addUnsafeEnchantment(Enchantment.SILK_TOUCH, 32);
		return is;
	}
	  public static void TabsetPriority(Player pl, int i) {
		    if (Main.TabAPIEnable)
		      TabAPI.setPriority(plugin, pl, 2);
		  }

		  public static void TabRefresh(Player p) {
		    if (Main.TabAPIEnable)
		      TabAPI.updatePlayer(p);
		  }

		  @EventHandler
		  public void onNameTag(PlayerReceiveNameTagEvent e)
		  {
		    if (Main.TabAPIEnable) {
		      Player p = e.getNamedPlayer();
		      if (teamM.isInRed(p.getName()))
		        e.setTag(ChatColor.RED + p.getName());
		      else if (teamM.isInBlue(p.getName()))
		        e.setTag(ChatColor.BLUE + p.getName());
		      else
		        e.setTag(ChatColor.GRAY + p.getName());
		    }
		  }

		  public static void clearAll(Player p) {
		    if (Main.TabAPIEnable)
		      for (int i = 0; i < TabAPI.getVertSize(); i++)
		        for (int j = 0; j < TabAPI.getHorizSize(); j++)
		          TabAPI.setTabString(plugin, p, i, j, "");
		  }
		  public static void updateTabAll(){
			  if (Main.TabAPIEnable) {
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
					public void run(){
						for (Player pl : plugin.getServer().getOnlinePlayers())
							ProtocolLib.updateTab(pl);
					}
				}, 6);
			  }
		  }
		  
		  
		  
		  public static void updateTab(Player p) {
		    if (Main.TabAPIEnable) {
		    	
		      TabAPI.setTabString(plugin, p, 0, 0, Message.Colorize("&a●&m-------------"));
		      TabAPI.setTabString(plugin, p, 0, 1, Message.Colorize("=MedievalWars="));
		      TabAPI.setTabString(plugin, p, 0, 2, Message.Colorize("&a&m-------------&a●"));
		      TabAPI.setTabString(plugin, p, 1, 0, Message.Colorize("&b    Blue Team"));
		      TabAPI.setTabString(plugin, p, 1, 1, Message.Colorize("&7    Spectators"));
		      TabAPI.setTabString(plugin, p, 1, 2, Message.Colorize("&c  Red Team"));
		      TabAPI.setTabString(plugin, p, 2, 0, Message.Colorize("&2●&m-------------"));
		      TabAPI.setTabString(plugin, p, 2, 1, Message.Colorize("&2&m--&aPvPKillz&2&m--"));
		      TabAPI.setTabString(plugin, p, 2, 2, Message.Colorize("&2&m-------------&2●"));
		      for (int x = 0; x <= 2; x++) {
		        String emptyText = "";
		        for (int y = 3; y <= TabAPI.getVertSize() - 1; y++) {
		          int num = y - 3;
		          if (x == 0) {
		            if (teamM.getTeam(Team.Blue).size() > num) {
		              TabAPI.setTabString(plugin, p, y, x, ChatColor.AQUA + (teamM.getTeam(Team.Blue).get(num)).getName());
		            } else {
		              TabAPI.setTabString(plugin, p, y, x, ChatColor.AQUA + emptyText + " ");
		              emptyText = emptyText + " ";
		            }
		          }
		          if (x == 1) {
		            if (teamM.getTeam(Team.Spectator).size() > num) {
		              TabAPI.setTabString(plugin, p, y, x, ChatColor.GRAY + (teamM.getTeam(Team.Spectator).get(num)).getName());
		            } else {
		              TabAPI.setTabString(plugin, p, y, x, ChatColor.GRAY + emptyText + " ");
		              emptyText = emptyText + " ";
		            }
		          }
		          if (x == 2) {
		            if (teamM.getTeam(Team.Red).size() > num) {
		              TabAPI.setTabString(plugin, p, y, x, ChatColor.RED + (teamM.getTeam(Team.Red).get(num)).getName());
		            } else {
		              TabAPI.setTabString(plugin, p, y, x, ChatColor.RED + emptyText + " ");
		              emptyText = emptyText + " ";
		            }

		          }

		        }

		      }

		      TabAPI.updatePlayer(p);
		    }
		  }
}
