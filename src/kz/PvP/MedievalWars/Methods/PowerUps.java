package kz.PvP.MedievalWars.Methods;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

import kz.PvP.MedievalWars.Main;
import kz.PvP.MedievalWars.utilities.Kits;

public class PowerUps {
	
	
	public static final int HorseHealthUpgrade1 = 10001;// Horse HP Powerups will be code 6000's
	public static final int HorseHealthUpgrade2 = 10002;
	public static final int HorseHealthUpgrade3 = 10003;
	public static final int PoisonUpgrade1 = 10011;// Arrow Poison Powerups will be code 6000's
	public static final int PoisonUpgrade2 = 10012;
	public static final int PoisonUpgrade3 = 10013;
	public static Main plugin;

	public PowerUps(Main mainclass) {
		plugin = mainclass;
	}
	
	
	
	
	
	public static List<Integer> getPowerUps(String p){
		if (!Kits.Abilities.containsKey(p))
			Kits.Abilities.put(p, new ArrayList<Integer>());
		return Kits.Abilities.get(p);
	}
	
	public static boolean hasPowerUp(Player p, int ID){
		return getPowerUps(p.getName()).contains(ID);
	}
	
	public static boolean addPowerUp(Player p, int ID){
		return getPowerUps(p.getName()).add(ID);
	}
	
	
	
	
	
}
